import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezTest {

    @Test
    void getBiggestNumberUntilValuesAreSmallerThanTrigger() {
        int[] list = {4,2,3,5,15,6,8};
        int max = 30;
        int expected = 15;
        int result = ExercicioDez.getBiggestNumberUntilValuesAreSmallerThanTrigger(list, max);
        assertEquals(expected, result);
    }
}