import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioVinteTest {

    @Test
    void isNumberPerfect() {
        int number = 6;
        boolean expected = true;
        boolean result = ExercicioVinte.isNumberPerfect(number);
        assertEquals(expected, result);
    }

    @Test
    void isNumberAbundant() {
        int number = 12;
        boolean expected = true;
        boolean result = ExercicioVinte.isNumberAbundant(number);
        assertEquals(expected, result);
    }

    @Test
    void isNumberReduced() {
        int number = 9;
        boolean expected = true;
        boolean result = ExercicioVinte.isNumberReduced(number);
        assertEquals(expected, result);
    }

    @Test
    void sumOfDivisors() {
        int number = 6;
        int expected = 6;
        int result = ExercicioVinte.sumOfDivisors(number);
        assertEquals(expected, result);
    }
}