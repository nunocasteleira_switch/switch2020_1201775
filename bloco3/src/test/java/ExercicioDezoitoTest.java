import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ExercicioDezoitoTest {

    @Test
    void multiplyByTenAndAddValidationDigit() {
        int number = 14206607;
        int verification = 9;
        int expected = 142066079;
        int result = ExercicioDezoito.multiplyByTenAndAddValidationDigit(number, verification);
        assertEquals(expected, result);
    }

    @Test
    void ponderatedSumWithoutArray() {
        int ccAll = 142066079;
        //1*9 + 4*8 + 2*7 + 0*6 + 6*5 + 6*4 + 0*3 + 7*2 + 9*1
        int expected = 132;
        int result = ExercicioDezoito.ponderatedSumWithoutArray(ccAll);
        assertEquals(expected, result);
    }

    @Test
    void isCCValid() {
        int number = 14206607;
        int verification = 9;
        boolean expected = true;
        boolean result = ExercicioDezoito.isCCValid(number, verification);
        assertEquals(expected, result);
    }

    @Test
    void isCCValid2() {
        int number = 13710152;
        int verification = 0;
        boolean expected = true;
        boolean result = ExercicioDezoito.isCCValid(number, verification);
        assertEquals(expected, result);
    }
}