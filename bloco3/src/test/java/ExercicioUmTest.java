import static org.junit.jupiter.api.Assertions.*;

class ExercicioUmTest {

    @org.junit.jupiter.api.Test
    void assureFactorizeOf5Gets120() {
        int num = 5;
        int expected = 120;
        int result = ExercicioUm.factorizeANumber(num);
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void assureFactorizeOf0Gets1() {
        int num = 0;
        int expected = 1;
        int result = ExercicioUm.factorizeANumber(num);
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void assureFactorizeOfANegativeNumberGets1() {
        int num = -3;
        int expected = 1;
        int result = ExercicioUm.factorizeANumber(num);
        assertEquals(expected, result);
    }
}