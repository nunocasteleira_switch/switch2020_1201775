import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezanoveTest {

    @Test
    void getEvenDigits() {
        int number = 24567;
        int expected = 642;
        int result = ExercicioDezanove.getInversedEvenDigits(number);
        assertEquals(expected, result);
    }

    @Test
    void getOddDigits() {
        int number = 24567;
        int expected = 75;
        int result = ExercicioDezanove.getInversedOddDigits(number);
        assertEquals(expected, result);
    }

    @Test
    void separateEvenFromOdds() {
        int number = 24567;
        int expected = 24657;
        int result = ExercicioDezanove.separateEvenFromOdds(number);
        assertEquals(expected, result);
    }
}