import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDoisTest {

    @Test
    void getPercentageOfPositiveGrades() {
        float[] grades = {15.0f, 16.0f, 14.0f, 8.0f, 5.0f, 6.0f, 7.0f};
        float expected = 0.42857f;
        float result = ExercicioDois.getPercentageOfPositiveGrades(grades);
        assertEquals(expected, result, 0.00001);
    }

    @Test
    void getMedianOfNegativeGrades() {
        float[] grades = {15.0f, 16.0f, 14.0f, 8.0f, 5.0f, 6.0f, 7.0f};
        float expected = 6.5f;
        float result = ExercicioDois.getMeanOfNegativeGrades(grades);
        assertEquals(expected, result, 0.01);
    }
}