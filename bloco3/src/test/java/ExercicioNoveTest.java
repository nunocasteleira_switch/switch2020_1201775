import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioNoveTest {

    @Test
    void getMonthlySalaryOfWorker() {
        int extraHours = 3;
        float baseSalary = 1207.65f;
        float expected = 1280.11f;//(1207.65 * 0.02) = 72459 // 1207.65 + 72.459 // 1280.109
        float result = ExercicioNove.getMonthlySalaryOfWorker(extraHours, baseSalary);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void meanMonthlySalaries() {
        float[] monthlySalaries = {1207.45f, 856.32f, 900.00f, 425.78f, -1}; // 847,3875
        float expected = 847.38f;
        float result = ExercicioNove.meanMonthlySalaries(monthlySalaries);
        assertEquals(expected, result, 0.01);
    }
}