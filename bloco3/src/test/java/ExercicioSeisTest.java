import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioSeisTest {

    @Test
    void numberOfDigits() {
        int number = 123456;
        int expected = 6;
        int result = ExercicioSeis.numberOfDigits(number);
        assertEquals(expected, result);
    }

    @Test
    void numberOfPairDigits() {
        int number = 123456;
        int expected = 3;
        int result = ExercicioSeis.numberOfEvenDigits(number);
        assertEquals(expected, result);
    }

    @Test
    void numberOfOddDigits() {
        int number = 1234567;
        int expected = 4;
        int result = ExercicioSeis.numberOfOddDigits(number);
        assertEquals(expected, result);
    }

    @Test
    void sumOfDigits() {
        int number = 123456;
        int expected = 21;
        int result = ExercicioSeis.sumOfDigits(number);
        assertEquals(expected, result);
    }

    @Test
    void sumOfEvenDigits() {
        int number = 123456;
        int expected = 12;
        int result = ExercicioSeis.sumOfEvenDigits(number);
        assertEquals(expected, result);
    }

    @Test
    void sumOfOddDigits() {
        int number = 123456;
        int expected = 9;
        int result = ExercicioSeis.sumOfOddDigits(number);
        assertEquals(expected, result);
    }

    @Test
    void meanOfDigits() {
        int number = 123456;
        double expected = 3.5;
        double result = ExercicioSeis.meanOfDigits(number);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void meanOfEvenDigits() {
        int number = 123456;
        double expected = 4;
        double result = ExercicioSeis.meanOfEvenDigits(number);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void meanOfOddDigits() {
        int number = 123456;
        double expected = 3;
        double result = ExercicioSeis.meanOfOddDigits(number);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void inverseOfNumber() {
        int number = 123456;
        int expected = 654321;
        int result = ExercicioSeis.inverseOfNumber(number);
        assertEquals(expected, result);
    }
}