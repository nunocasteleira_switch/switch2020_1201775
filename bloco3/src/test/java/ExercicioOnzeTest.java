import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioOnzeTest {

    @Test
    void getPossibleCombinationOfSumToObtainAValue() {
        int number = 5;
        int expected = 3;
        int result = ExercicioOnze.getPossibleCombinationOfSumToObtainAValue(number);
        assertEquals(expected, result);
    }
}