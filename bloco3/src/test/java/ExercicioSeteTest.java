import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ExercicioSeteTest {

    @Test
    void isNumberAPalindrome() {
        int number = 1234321;
        boolean expected = true;
        boolean result = ExercicioSete.isNumberAPalindrome(number);
        assertEquals(expected, result);
    }

    @Test
    void isNumberAnArmstrong() {
        int number = 548834;
        boolean expected = true;
        boolean result = ExercicioSete.isNumberAnArmstrong(number);
        assertEquals(expected, result);
    }

    @Test
    void firstPalindrome() {
        int first = 25;
        int last = 20;
        int expected = 22;
        int result = ExercicioSete.firstPalindrome(first, last);
        assertEquals(expected, result);
    }

    @Test
    void largestPalindrome() {
        int first = 35;
        int last = 20;
        int expected = 33;
        int result = ExercicioSete.largestPalindrome(first, last);
        assertEquals(expected, result);
    }

    @Test
    void countPalindromesInSequence() {
        int first = 44;
        int last = 20;
        int expected = 3;
        int result = ExercicioSete.countPalindromesInSequence(first, last);
        assertEquals(expected, result);
    }

    @Test
    void firstArmstrong() {
        int first = 400;
        int last = 100;
        int expected = 153;
        int result = ExercicioSete.firstArmstrong(first, last);
        assertEquals(expected, result);
    }

    @Test
    void countArmstrong() {
        int first = 400;
        int last = 100;
        int expected = 3;
        int result = ExercicioSete.countArmstrong(first, last);
        assertEquals(expected, result);
    }
}