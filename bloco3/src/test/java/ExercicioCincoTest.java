import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioCincoTest {

    @Test
    void sumOfEvenNumbersBetween2and10() {
        //2 + 4 + 6 + 8 + 10 = 30
        int first = 2;
        int last = 10;
        int expected = 30;
        int result = ExercicioCinco.sumOfEvenNumbersInSetInterval(first, last);
        assertEquals(expected, result);
    }

    @Test
    void countOfEvenNumbersBetween2and10() {
        //2, 4, 6, 8, 10 = 5
        int first = 2;
        int last = 10;
        int expected = 5;
        int result = ExercicioCinco.countOfEvenNumbersInSetInterval(first, last);
        assertEquals(expected, result);
    }

    @Test
    void sumOfOddNumbersInSetInterval() {
        //1 + 3 + 5 + 7 + 9 = 25
        int first = 1;
        int last = 9;
        int expected = 25;
        int result = ExercicioCinco.sumOfOddNumbersInSetInterval(first, last);
        assertEquals(expected, result);
    }

    @Test
    void countOfOddNumbersInSetInterval() {
        int first = 1;
        int last = 9;
        int expected = 5;
        int result = ExercicioCinco.countOfOddNumbersInSetInterval(first, last);
        assertEquals(expected, result);
    }

    @Test
    void sumOfAllMultiplesOfASetNumberInSetNotOrderedInterval() {
        //5 + 10 + 15 + 20 + 25 =
        int multiplex = 5;
        //Unordered
        int first = 25;
        int last = 1;
        int expected = 75;
        int result = ExercicioCinco.sumOfAllMultiplesOfASetNumberInSetNotOrderedInterval(multiplex, first, last);
        assertEquals(expected, result);
    }

    @Test
    void productOfAllMultiplesOfASetNumberInSetNotOrderedInterval() {
        //5 * 10 * 15 * 20 * 25 =
        int multiplex = 5;
        //Unordered
        int first = 25;
        int last = 1;
        int expected = 375000;
        int result = ExercicioCinco.productOfAllMultiplesOfASetNumberInSetNotOrderedInterval(multiplex, first, last);
        assertEquals(expected, result);
    }

    @Test
    void meanOfMultiplesOfASetNumberInSetInterval() {
        int multiplex = 5;
        int first = 25;
        int last = 1;
        int expected = 15;
        float result = ExercicioCinco.meanOfMultiplesOfASetNumberInSetInterval(multiplex, first, last);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void meanOfMultiplesOfXYInSetInterval() {
        //(4+8+12+16+20)/5 = 12
        int multiplex1 = 2;
        int multiplex2 = 4;
        int first = 20;
        int last = 1;
        int expected = 12;
        float result = ExercicioCinco.meanOfMultiplesOfXYInSetInterval(multiplex1, multiplex2, first, last);
        assertEquals(expected, result, 0.01);
    }
}