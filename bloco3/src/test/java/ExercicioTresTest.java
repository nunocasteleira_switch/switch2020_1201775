import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ExercicioTresTest {

    @Test
    void testPercentageOfEvenNumbers() {
        int[] sequence = {2, 4, 6, 7, 4, 3, 4, 5, 6, -1};
        float expected = 0.66f;
        float result = ExercicioTres.percentageOfEvenNumbers(sequence);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void testOddNumbersMean() {
        int[] sequence = {2, 4, 6, 7, 4, 3, 4, 5, 6, -1};
        float expected = 5;
        float result = ExercicioTres.oddNumbersMean(sequence);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void is2Even() {
        int number = 2;
        boolean expected = true;
        boolean result = ExercicioTres.isANumberEven(number);
        assertEquals(expected, result);
    }
}