import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ExercicioQuatroTest {

    @Test
    void multiplesOf3Between3and9() {
        int first = 3;
        int last = 9;
        int expected = 3;
        int result = ExercicioQuatro.multiplesOf3InSetInterval(first, last);
        assertEquals(expected, result);
    }

    @Test
    void multiplesOf3Between4and10() {
        int first = 4;
        int last = 10;
        int expected = 2;
        int result = ExercicioQuatro.multiplesOf3InSetInterval(first, last);
        assertEquals(expected, result);
    }

    @Test
    void multiplesOf5Between5and25() {
        int multiplex = 5;
        int first = 5;
        int last = 25;
        int expected = 5;
        int result = ExercicioQuatro.multiplesOfSetNumberInSetInterval(multiplex, first, last);
        assertEquals(expected, result);
    }

    @Test
    void multiplesOf3and5Between1and125() {
        int first = 1;
        int last = 125;
        int expected = 8;
        int result = ExercicioQuatro.multiplesOf3and5InSetInterval(first, last);
        assertEquals(expected, result);
    }

    @Test
    void multiplesOfTwoSetNumbersInSetInterval() {
        int multiplex1 = 2;
        int multiplex2 = 4;
        int first = 1;
        int last = 120;
        int expected = 30;
        int result = ExercicioQuatro.multiplesOfTwoSetNumbersInSetInterval(multiplex1, multiplex2, first, last);
        assertEquals(expected, result);
    }

    @Test
    void sumOfMultiplesOf2and4InSetInterval() {
        //4,8,12,16,20
        int multiplex1 = 2;
        int multiplex2 = 4;
        int first = 1;
        int last = 20;
        int expected = 60;
        int result = ExercicioQuatro.sumOfMultiplesOfTwoSetNumbersInSetInterval(multiplex1, multiplex2, first, last);
        assertEquals(expected, result);
    }
}