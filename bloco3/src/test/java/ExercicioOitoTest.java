import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioOitoTest {

    @Test
    void smallestValueInArrWhenInputIsLargerThanSum() {
        int[] list = {4,2,3,5,15,6,8};
        int expected = 2;
        int result = ExercicioOito.smallestValueInArrWhenInputIsLargerThanSum(list);
        assertEquals(expected, result);
    }
}