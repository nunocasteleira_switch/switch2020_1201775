import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezasseisTest {

    @Test
    void getTaxedSalary() {
        double totalSalary = 3500.0;
        double expected = 2875.0;
        double result = ExercicioDezasseis.getTaxedSalary(totalSalary);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void testGetTaxedSalaryUnderThousand() {
        double totalSalary = 900.0;
        double expected = 790.0;
        double result = ExercicioDezasseis.getTaxedSalary(totalSalary);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void testGetTaxedSalaryUnderFiveHundred() {
        double totalSalary = 400.0;
        double expected = 360;
        double result = ExercicioDezasseis.getTaxedSalary(totalSalary);
        assertEquals(expected, result, 0.01);
    }
}