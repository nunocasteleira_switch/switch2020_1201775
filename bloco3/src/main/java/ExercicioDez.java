public class ExercicioDez {
    public static void main(String[] args) {

    }

    public static int getBiggestNumberUntilValuesAreSmallerThanTrigger(int[] list, int max){
        int sum = 0;
        int i = 0;
        int biggest = 0;
        while(sum < max) {
            sum += list[i];
            if (biggest < list[i]) {
                biggest = list[i];
            }
            i++;
        }
        return biggest;
    }
}
