import java.util.Scanner;

public class ExercicioTreze {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Insira código do produto");
        int productCode = scan.nextInt();

        while (productCode != 0) {
            switch (productCode) {
                case 1:
                    System.out.println("Alimento não perecível");
                    break;
                case 2:
                case 3:
                case 4:
                    System.out.println("Alimento perecível");
                    break;
                case 5:
                case 6:
                    System.out.println("Vestuário");
                    break;
                case 7:
                    System.out.println("Higiene pessoal");
                    break;
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                    System.out.println("Limpeza e utensílios domésticos");
                    break;
                default:
                    System.out.println("Código inválido");
                    break;
            }
            System.out.println("Insira código do produto");
            productCode = scan.nextInt();
        }
    }
}
