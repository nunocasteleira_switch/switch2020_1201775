import java.util.Scanner;

public class ExercicioCatorze {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Insira valor em euros que pretende converter");
        double euros = scan.nextDouble();
        System.out.println("Insira o código da moeda para aqual pretende converter:");
        System.out.println("(D)ólar, (L)ibra, (I)ene, (C)oroa Sueca e (F)ranco Suiço");
        char currency = scan.next().charAt(0);

        while (euros != 0) {
            switch (currency) {
                case 'D':
                case 'd':
                    System.out.println(euros + " equivalem a " + (euros * 1.534) + " dólares.");
                    break;
                case 'L':
                case 'l':
                    System.out.println(euros + " equivalem a " + (euros * 0.774) + " libras.");
                    break;
                case 'I':
                case 'i':
                    System.out.println(euros + " equivalem a " + (euros * 161.48) + " libras.");
                    break;
                case 'C':
                case 'c':
                    System.out.println(euros + " equivalem a " + (euros * 9.593) + " coroas suecas.");
                    break;
                case 'F':
                case 'f':
                    System.out.println(euros + " equivalem a " + (euros * 1.601) + " francos suiços.");
                    break;
                default:
                    System.out.println("Moeda inválida");
                    break;
            }
            System.out.println("Insira valor em euros que pretende converter");
            euros = scan.nextDouble();
            System.out.println("(D)ólar, (L)ibra, (I)ene, (C)oroa Sueca e (F)ranco Suiço");
            currency = scan.next().charAt(0);
        }
    }
}
