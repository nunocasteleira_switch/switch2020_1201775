public class ExercicioSeis {
    public static void main(String[] args) {
    /*
        a) O número de algarismos de um número inteiro longo.
        b) O número de algarismos pares de um número inteiro longo.
        c) O número de algarismos ímpares de um número inteiro longo.
        d) A soma dos algarismos de um número inteiro longo.
        e) A soma dos algarismos pares de um número inteiro longo.
        f) A soma dos algarismos ímpares de um número inteiro longo.
        g) A média dos algarismos de um número inteiro longo.
        h) A média dos algarismos pares de um número inteiro longo.
        i) A média dos algarismos ímpares de um número inteiro longo.
        j) Um número inteiro longo cujos dígitos estão pela ordem inversa (e.g. dado 987 retorna 789).
     */
    }

    public static int numberOfDigits(int number) {
        int count = 0;
        while (number != 0) {
            number /= 10;
            count++;
        }
        return count;
    }

    public static int numberOfEvenDigits(int number) {
        int count = 0;
        int countEvens = 0;
        while (number != 0) {
            if (number % 2 == 0) {
                countEvens++;
            }
            number /= 10;
            count++;
        }
        return countEvens;
    }

    public static int numberOfOddDigits(int number) {
        int count = 0;
        int countOdds = 0;
        while (number != 0) {
            if (number % 2 == 1) {
                countOdds++;
            }
            number /= 10;
            count++;
        }
        return countOdds;
    }

    public static int sumOfDigits(int number) {
        //d) A soma dos algarismos de um número inteiro longo.
        int sum = 0;
        while (number != 0) {
            sum += number % 10;
            number /= 10;
        }
        return sum;
    }

    public static int sumOfEvenDigits(int number) {
        int sum = 0;
        while (number != 0) {
            if (number % 2 == 0) {
                sum += number % 10;
            }
            number /= 10;
        }
        return sum;
    }

    public static int sumOfOddDigits(int number) {
        int sum = 0;
        while (number != 0) {
            if (number % 2 == 1) {
                sum += number % 10;
            }
            number /= 10;
        }
        return sum;
    }

    public static double meanOfDigits(int number) {
        int count = numberOfDigits(number);
        int sum = sumOfDigits(number);
        return (double) sum / count;
    }

    public static double meanOfEvenDigits(int number) {
        int count = numberOfEvenDigits(number);
        int sum = sumOfEvenDigits(number);
        return (double) sum / count;
    }

    public static double meanOfOddDigits(int number) {
        int count = numberOfOddDigits(number);
        int sum = sumOfOddDigits(number);
        return (double) sum / count;
    }

    public static int inverseOfNumber(int number) {
        int inverse = 0;
        while (number != 0) {
            inverse *= 10;
            inverse += number % 10;
            number /= 10;
        }
        return inverse;
    }
}
