public class ExercicioTres {
    public static void main(String[] args) {
        //Percentagem dos números pares
        //Média dos números ímpares
        //É par ou ímpar
    }

    public static float percentageOfEvenNumbers(int[] sequence) {
        int evenNumbersCounter = 0;
        int i = 0;
        while (sequence[i] >= 0) {
            if (isANumberEven(sequence[i])) {
                evenNumbersCounter++;
            }
            i++;
        }
        return (float) evenNumbersCounter / i;
    }

    public static float oddNumbersMean(int[] sequence) {
        int i = 0;
        int oddNumbersSum = 0;
        int oddNumbersCounter = 0;
        while (sequence[i] >= 0) {
            if(!isANumberEven(sequence[i])) {
                oddNumbersSum+=sequence[i];
                oddNumbersCounter++;
            }
            i++;
        }
        return (float) oddNumbersSum / oddNumbersCounter;
    }

    public static boolean isANumberEven(int number) {
        return number % 2 == 0;
    }
}
