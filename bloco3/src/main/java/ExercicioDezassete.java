import java.util.Scanner;

public class ExercicioDezassete {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Insira o peso do cão:");
        float peso = scan.nextFloat();
        float racao = 0;
        String racaCategory;

        while (peso >= 0 && racao >= 0) {
            if (peso < 10) {
                getAdequateRatio(100);
            } else if (peso < 25) {
                racao = scan.nextFloat();
                getAdequateRatio(250);
            } else if (peso < 45) {
                racao = scan.nextFloat();
                getAdequateRatio(300);
            } else {
                racao = scan.nextFloat();
                getAdequateRatio(500);
            }
            System.out.println("Insira o peso do cão:");
            peso = scan.nextFloat();
        }
    }

    private static void getAdequateRatio(int expected) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Insira a quantidade de comida:");
        float racao = scan.nextFloat();
        if (racao >= 0) {
            if (racao == expected) {
                System.out.println("A ração é adequada");
            } else {
                System.out.println("A ração é NÃO É adequada");
            }
        }
    }
}
