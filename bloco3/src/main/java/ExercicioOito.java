public class ExercicioOito {
    public static void main(String[] args) {

    }

    public static int smallestValueInArrWhenInputIsLargerThanSum(int[] list) {
        //{4,2,3,5,15,6,8}
        int sum = 0;
        int smallest = 0;
        for (int i = 0; i < list.length; i++) {
            //Com ajuda do prof.
            //no primeiro ciclo tenho de atribuir os valores iniciais às variáveis
            if (i == 0) {
                sum = list[i];
                smallest = list[i];
            } else {
                if (sum < list[i]) {
                    break;
                }
                if (list[i] < smallest) {
                    smallest = list[i];
                }
                sum += list[i];
            }
        }
        return smallest;
    }
}
