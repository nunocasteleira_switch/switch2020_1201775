public class ExercicioDois {

    public static void main(String[] args) {
        /*
        Preciso de ter duas funções:
            Retornar a percentagem de notas positivas (eval>=10 / eval.length)
            Retornar média das notas negativas (eval<10 / eval<10.length)
         */
        float[] grades = {15.0f, 16.0f, 14.0f, 8.0f, 5.0f, 6.0f};
        System.out.println(getPercentageOfPositiveGrades(grades));
        System.out.println(getMeanOfNegativeGrades(grades));

    }

    public static float getPercentageOfPositiveGrades(float[] grades) {
        float counter = 0;
        //noinspection ForLoopReplaceableByForEach
        for (int i = 0; i < grades.length; i++) {
            if (grades[i] >= 10) {
                counter++;
            }
        }
        return counter / grades.length;
    }

    public static float getMeanOfNegativeGrades(float[] grades) {
        float negativeGradesSum = 0;
        float negativeGradesCounter = 0;
        //noinspection ForLoopReplaceableByForEach
        for (int i = 0; i < grades.length; i++) {
            if (grades[i] < 10) {
                negativeGradesSum += grades[i];
                negativeGradesCounter++;
            }
        }
        return negativeGradesSum / negativeGradesCounter;
    }
}
