public class ExercicioDezoito {

    // multiply by 10 and sum validation number
    // get digits
    // soma ponderada = a5 x5+a4 x4+a3 x3+a2 x2+a1
    // multiple of 11

    // 14206607 9

    public static void main(String[] args) {

    }

    public static boolean isCCValid(int CCNumber, int validation) {
        int allDigits = multiplyByTenAndAddValidationDigit(CCNumber, validation);
        int sum = ponderatedSumWithoutArray(allDigits);
        return sum % 11 == 0;
    }

    public static int multiplyByTenAndAddValidationDigit(int CCNumber, int validation) {
        return CCNumber * 10 + validation;
    }

    public static int ponderatedSumWithoutArray(int CCNumberWithVerification) {
        int length = ExercicioSeis.numberOfDigits(CCNumberWithVerification);
        int partialCC = CCNumberWithVerification;
        int sum = 0;
        for (int i = 1; i <= length; i++) {
            int digit = partialCC % 10;
            sum += digit * i;
            partialCC /= 10;
        }
        return sum;
    }

}
