public class ExercicioQuatro {
    public static void main(String[] args) {
        /*
            a) O número de múltiplos de 3 num intervalo dado.
            b) O número de múltiplos de um dado número inteiro num intervalo dado.
            c) O número de múltiplos de 3 e 5 num intervalo dado.
            d) O número de múltiplos de dois números inteiros num intervalo dado.
            e) A soma dos múltiplos de dois números inteiros num intervalo dado.
         */
    }

    public static int multiplesOf3InSetInterval(int first, int last) {
        int counter = 0;
        for (int i = first; i <= last; i++) {
            if (i % 3 == 0) {
                counter++;
            }
        }
        return counter;
    }

    public static int multiplesOfSetNumberInSetInterval(int multiplex, int first, int last) {
        int counter = 0;
        for (int i = first; i <= last; i++) {
            if (i % multiplex == 0) {
                counter++;
            }
        }
        return counter;
    }

    public static int multiplesOf3and5InSetInterval(int first, int last) {
        int counter = 0;
        for (int i = first; i <= last; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                counter++;
            }
        }
        return counter;
    }

    public static int multiplesOfTwoSetNumbersInSetInterval(int multiplex1, int multiplex2, int first, int last) {
        int counter = 0;
        for (int i = first; i <= last; i++) {
            if (i % multiplex1 == 0 && i % multiplex2 == 0) {
                counter++;
            }
        }
        return counter;
    }

    public static int sumOfMultiplesOfTwoSetNumbersInSetInterval(int multiplex1, int multiplex2, int first, int last) {
        int counter = 0;
        int sum = 0;
        for (int i = first; i <= last; i++) {
            if (i % multiplex1 == 0 && i % multiplex2 == 0) {
                counter++;
                sum += i;
            }
        }
        return sum;
    }
}
