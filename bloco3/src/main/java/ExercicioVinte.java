public class ExercicioVinte {
    public static void main(String[] args) {

    }

    public static boolean isNumberPerfect(int number) {
        int sum = sumOfDivisors(number);
        return sum == number;
    }

    public static boolean isNumberAbundant(int number) {
        int sum = sumOfDivisors(number);
        return sum > number;
    }

    public static boolean isNumberReduced(int number) {
        int sum = sumOfDivisors(number);
        return sum < number;
    }

    public static int sumOfDivisors(int number) {
        int sumOfDivisors = 0;
        for (int i = 1; i < (number / 2) + 1 ; i++) {
            if (number % i == 0) {
                sumOfDivisors += i;
            }
        }
        return sumOfDivisors;
    }
}
