public class ExercicioUm {
    public static void main(String[] args) {
        /*
        a) Este exercício factoriza um número inteiro
        e) Na minha opinião, qualquer valor introduzido, desde que inteiro
             permitirá que o ciclo execute, pois independentemente do valor
             será permitido ciclar até retornar o valor 1.
             Caso o valor não possa ser calculado, na linha 16 está declarado
             o valor inicial do resultado (res = 1), permitindo sempre a resposta
             de um número inteiro.
        */
        System.out.println("O resultado é " + factorizeANumber(0));
    }

    public static int factorizeANumber(int num) {
        int res = 1;
        for (int i = num; i > 1; i--) {
            res = res * i;
        }
        return res;
    }
}
