public class ExercicioNove {
    public static void main(String[] args) {

    }

    public static float getMonthlySalaryOfWorker(int extraHours, float baseSalary) {
        return (float) (baseSalary + (extraHours * (baseSalary * 0.02)));
    }

    public static float meanMonthlySalaries(float[] monthlySalaries) {
        float sum = 0;
        int i = 0;
        while (i < monthlySalaries.length && monthlySalaries[i] > 0) {
            sum += monthlySalaries[i];
            i++;
        }
        return sum / i;
    }
}
