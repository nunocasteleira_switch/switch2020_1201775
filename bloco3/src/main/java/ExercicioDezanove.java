public class ExercicioDezanove {
    public static void main(String[] args) {

    }

    //dividir pares e dividir impares
    //contar pares para saber o for
    //somar cada dígito

    //contar pares
    //multiplicar por dez as vezes que os pares existirem
    //somar ímpar

    public static int separateEvenFromOdds(int number) {
        int evenDigits = getInversedEvenDigits(number);
        int oddDigits = getInversedOddDigits(number);
        evenDigits = ExercicioSeis.inverseOfNumber(evenDigits);
        oddDigits = ExercicioSeis.inverseOfNumber(oddDigits);
        int countOfEvenDigits = ExercicioSeis.numberOfDigits(evenDigits);
        for (int i = 0; i < countOfEvenDigits - 1; i++) {
            evenDigits *= 10;
        }
        return evenDigits + oddDigits;
    }

    public static int getInversedEvenDigits(int number) {
        int digits = ExercicioSeis.numberOfDigits(number);
        int evenDigits = 0;
        int digit;
        int partialNumber = number;
        for (int i = 0; i < digits; i++) {
            digit = partialNumber % 10;
            partialNumber /= 10;
            if (digit % 2 == 0) {
                evenDigits *= 10;
                evenDigits += digit;
            }
        }
        return evenDigits;
    }

    public static int getInversedOddDigits(int number) {
        int digits = ExercicioSeis.numberOfDigits(number);
        int oddDigits = 0;
        int digit;
        int partialNumber = number;
        for (int i = 0; i < digits; i++) {
            digit = partialNumber % 10;
            partialNumber /= 10;
            if (digit % 2 == 1) {
                oddDigits *= 10;
                oddDigits += digit;
            }
        }
        return oddDigits;
    }
}
