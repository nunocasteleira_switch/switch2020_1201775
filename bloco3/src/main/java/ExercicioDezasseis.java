public class ExercicioDezasseis {
    public static void main(String[] args) {

    }

    public static double getTaxedSalary(double totalSalary) {
        double taxTwenty = 0;
        double taxFifteen = 0;
        double taxTen;
        //int partialSalary;
        if (totalSalary >= 1000) {
            taxTwenty = (totalSalary - 1000) * 0.20;
            taxFifteen = 500 * 0.15;
            taxTen = 500 * 0.1;
        } else if (totalSalary >= 500) {
            taxFifteen = (totalSalary - 500) * 0.15;
            taxTen = 500 * 0.1;
        } else {
            taxTen = totalSalary * 0.1;
        }
        return totalSalary - taxTwenty - taxFifteen - taxTen;
    }
}
