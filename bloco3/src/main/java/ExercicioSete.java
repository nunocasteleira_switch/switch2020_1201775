public class ExercicioSete {
    public static void main(String[] args) {
        /*
            a) Verifique se um número inteiro longo é capicua.
            b) Verifique se um dado número é um número de Amstrong, i.e. se for igual à soma dos cubos dos seus algarismos.
            c) Retorne a primeira capicua num intervalo dado.
            d) Retorne a maior capicua num intervalo dado.
            e) Retorne o número de capicuas num intervalo dado.
            f) RetorneoprimeironúmerodeAmstrongnumdadointervalo.
            g) Retorne a quantidade de números de Amstrong num dado intervalo.
         */
    }

    public static boolean isNumberAPalindrome(int number) {
        int inverse = ExercicioSeis.inverseOfNumber(number);
        return number == inverse;
    }

    public static boolean isNumberAnArmstrong(int number) {
        //153 = 1^3 + 5^3+ 3^3;
        int size = ExercicioSeis.numberOfDigits(number);
        int saveNumber = number;
        int sum = 0;
        int temp;
        while (number != 0) {
            temp = number % 10;
            sum += Math.pow(temp, size);
            number /= 10;
        }
        return sum == saveNumber;
    }

    public static int firstPalindrome(int first, int last) {
        if (first > last) {
            int temp = first;
            first = last;
            last = temp;
        }
        int firstPalindrome = first;
        while (!isNumberAPalindrome(firstPalindrome) && firstPalindrome <= last) {
            if (!isNumberAPalindrome(firstPalindrome)) {
                firstPalindrome++;
            }
        }
        return firstPalindrome;
    }

    public static int largestPalindrome(int first, int last) {
        if (first > last) {
            int temp = first;
            first = last;
            last = temp;
        }
        int i = first;
        int temp = 0;
        while (i <= last) {
            if (isNumberAPalindrome(i)) {
                temp = i;
            }
            i++;
        }
        return temp;
    }

    public static int countPalindromesInSequence(int first, int last) {
        if (first > last) {
            int temp = first;
            first = last;
            last = temp;
        }
        int i = first;
        int count = 0;
        while (i <= last) {
            if (isNumberAPalindrome(i)) {
                count++;
            }
            i++;
        }
        return count;
    }

    public static int firstArmstrong(int first, int last) {
        if (first > last) {
            int temp = first;
            first = last;
            last = temp;
        }
        int firstArmstrong = first;
        while (!isNumberAnArmstrong(firstArmstrong) && firstArmstrong <= last) {
            if (!isNumberAnArmstrong(firstArmstrong)) {
                firstArmstrong++;
            }
        }
        return firstArmstrong;
    }

    public static int countArmstrong(int first, int last) {
        if (first > last) {
            int temp = first;
            first = last;
            last = temp;
        }
        int i = first;
        int count = 0;
        while (i <= last) {
            if (isNumberAnArmstrong(i)) {
                count++;
            }
            i++;
        }
        return count;
    }
}
