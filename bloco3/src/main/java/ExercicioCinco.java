public class ExercicioCinco {
    public static void main(String[] args) {
        /*
        a) A soma de todos os números pares num dado intervalo.
        b) A quantidade de todos os números pares num dado intervalo.
        c) A soma de todos os números ímpares num dado intervalo.
        d) A quantidade de todos os números ímpares num dado intervalo.
        e) A soma de todos os números múltiplos de um dado número num dado intervalo. Os dois
            números, que definem os limites do intervalo, não estão necessariamente por ordem crescente.
        f) O produto de todos os números múltiplos de um dado número num dado intervalo.
        g) A média dos múltiplos de um dado número num intervalo definido por dois números.
        h) A média dos múltiplos de X ou Y num intervalo definido por dois números. X e Y são dados.
         */
    }

    public static int sumOfEvenNumbersInSetInterval(int first, int last) {
        int sum = 0;
        for (int i = first; i <= last; i++) {
            if (ExercicioTres.isANumberEven(i)) {
                sum += i;
            }
        }
        return sum;
    }

    public static int countOfEvenNumbersInSetInterval(int first, int last) {
        int count = 0;
        for (int i = first; i <= last; i++) {
            if (ExercicioTres.isANumberEven(i)) {
                count++;
            }
        }
        return count;
    }

    public static int sumOfOddNumbersInSetInterval(int first, int last) {
        int sum = 0;
        for (int i = first; i <= last; i++) {
            if (!ExercicioTres.isANumberEven(i)) {
                sum += i;
            }
        }
        return sum;
    }

    public static int countOfOddNumbersInSetInterval(int first, int last) {
        int count = 0;
        for (int i = first; i <= last; i++) {
            if (!ExercicioTres.isANumberEven(i)) {
                count++;
            }
        }
        return count;
    }

    public static int sumOfAllMultiplesOfASetNumberInSetNotOrderedInterval(int multiplex, int first, int last) {
        //e) A soma de todos os números múltiplos de um dado número num dado intervalo. Os dois
        //números, que definem os limites do intervalo, não estão necessariamente por ordem crescente.
        int sum = 0;
        //change the order if unordered
        if (first > last) {
            int temp = first;
            first = last;
            last = temp;
        }
        for (int i = first; i <= last; i++) {
            if (i % multiplex == 0) {
                sum += i;
            }
        }
        return sum;
    }

    public static int productOfAllMultiplesOfASetNumberInSetNotOrderedInterval(int multiplex, int first, int last) {
        int product = 1;
        //change the order if unordered
        if (first > last) {
            int temp = first;
            first = last;
            last = temp;
        }
        for (int i = first; i <= last; i++) {
            if (i % multiplex == 0) {
                product *= i;
            }
        }
        return product;
    }

    //g) A média dos múltiplos de um dado número num intervalo definido por dois números.
    public static float meanOfMultiplesOfASetNumberInSetInterval(int multiplex, int first, int last) {
        //change the order if unordered
        int sum;
        int count;
        if (first > last) {
            int temp = first;
            first = last;
            last = temp;
        }
        //multiples
        sum = ExercicioQuatro.sumOfMultiplesOfTwoSetNumbersInSetInterval(multiplex, multiplex, first, last);
        //sum
        count = ExercicioQuatro.multiplesOfSetNumberInSetInterval(multiplex, first, last);
        return (float) sum / count;
    }

    //h) A média dos múltiplos de X ou Y num intervalo definido por dois números. X e Y são dados.
    public static float meanOfMultiplesOfXYInSetInterval(int multiplex1, int multiplex2, int first, int last) {
        //change the order if unordered
        int sum;
        int count;
        if (first > last) {
            int temp = first;
            first = last;
            last = temp;
        }
        //multiples
        sum = ExercicioQuatro.sumOfMultiplesOfTwoSetNumbersInSetInterval(multiplex1, multiplex2, first, last);
        //sum
        count = ExercicioQuatro.multiplesOfTwoSetNumbersInSetInterval(multiplex1, multiplex2, first, last);
        return (float) sum / count;
    }
}
