import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class NunoQuadradoTest {

    @Test
    void toArray() {
        int[][] matriz = {
                {1},
                {2},
                {3}
        };
        NunoQuadrado matrix = new NunoQuadrado(matriz);
        int[][] expected = {
                {1},
                {2},
                {3}
        };
        NunoQuadrado expectedQuadrado = new NunoQuadrado(expected);

        assertEquals(expectedQuadrado, matrix);
    }

    @Test
    void highest() {
        int[][] matriz = {
                {1, 234, 345678},
                {2, 432, 5435},
                {3, 765432, 653}
        };
        NunoQuadrado matrix = new NunoQuadrado(matriz);
        int expected = 765432;
        int result;

        result = matrix.highest();

        assertEquals(expected, result);
    }
}