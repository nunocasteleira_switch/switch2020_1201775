import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class NunoTest {

    @org.junit.jupiter.api.Test
    void toArray() {
        int[] arrayInicial = {1, 2, 3};
        Nuno array = new Nuno(arrayInicial);
        int[] expectedArray = {1,2,3};
        Nuno expected = new Nuno(expectedArray);

        assertEquals(expected, array);
    }

    @Test
    void append() {
        int[] arrayInicial = {1, 2, 3};
        Nuno result = new Nuno(arrayInicial);
        int valor = 4;
        int[] expected = {1, 2, 3, 4};

        result.append(valor);

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void highest() {
        int[] arrayInicial = {1, 2, 3};
        Nuno array = new Nuno(arrayInicial);
        int expected = 3;
        int result;

        result = array.highest();

        assertEquals(expected, result);
    }
}