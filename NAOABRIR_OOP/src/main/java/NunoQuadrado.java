public class NunoQuadrado {
    private Nuno[] nunoQuadrado;

    public NunoQuadrado() {
        Nuno[] novoNunoQuadrado = new Nuno[0];
        this.nunoQuadrado = novoNunoQuadrado;
    }

    public NunoQuadrado(Nuno[] nuno) {
        Nuno[] novoNunoQuadrado = new Nuno[nuno.length];
        for (int i = 0; i < nuno.length; i++) {
            novoNunoQuadrado[i] = nuno[i].copia();
        }
        this.nunoQuadrado = novoNunoQuadrado;
    }

    public NunoQuadrado(int[][] array) {
        Nuno[] novoNunoQuadrado = new Nuno[array.length];
        for (int i = 0; i < array.length; i++) {
            Nuno arr = new Nuno(array[i]);
            novoNunoQuadrado[i] = arr;
        }
        this.nunoQuadrado = novoNunoQuadrado;
    }

    public int[][] toArray() {
        int[][] copiaNunoQuadrado = new int[this.nunoQuadrado.length][];
        for (int i = 0; i < this.nunoQuadrado.length; i++) {
            Nuno arr = new Nuno(this.nunoQuadrado[i].toArray());
            copiaNunoQuadrado[i] = arr.toArray();
        }
        return copiaNunoQuadrado;
    }

    public int highest() {
        int highest;
        Nuno highests = new Nuno();
        for (Nuno nuno : this.nunoQuadrado) {
            highests.append(nuno.highest());
        }
        highest = highests.highest();
        return highest;
    }

    @Override
    public boolean equals(Object other) {
        if (this.nunoQuadrado == other) return true;
        if (other == null || this.getClass() != other.getClass()) return false;

        NunoQuadrado otherNuno = (NunoQuadrado) other;
        if (this.nunoQuadrado.length != otherNuno.nunoQuadrado.length) return false;
        boolean isEquals = true;
        for (int i = 0; i < this.nunoQuadrado.length && isEquals; i++) {
            isEquals = this.nunoQuadrado[i].equals(otherNuno.nunoQuadrado[i]);
        }
        return isEquals;
    }

}
