public class Nuno {
    private int[] nuno;

    public Nuno() {
        int[] copia = new int[0];
        this.nuno = copia;
    }

    public Nuno(int[] inteiros) {
        int[] copia = new int[inteiros.length];
        for (int i = 0; i < inteiros.length; i++) {
            copia[i] = inteiros[i];
        }
        this.nuno = copia;
    }

    public Nuno copia() {
        int[] copia = new int[this.nuno.length];
        for (int i = 0; i < this.nuno.length; i++) {
            copia[i] = this.nuno[i];
        }
        Nuno copiaArray = new Nuno(copia);
        return copiaArray;
    }

    public int[] toArray() {
        int[] copia = new int[this.nuno.length];
        for (int i = 0; i < this.nuno.length; i++) {
            copia[i] = this.nuno[i];
        }
        return copia;
    }

    public void append(int valor) {
        int[] copiaMaior = new int[this.nuno.length + 1];
        for (int i = 0; i < this.nuno.length; i++) {
            copiaMaior[i] = this.nuno[i];
        }
        copiaMaior[this.nuno.length] = valor;
        this.nuno = copiaMaior;
    }

    public int highest() {
        int highest = this.nuno[0];
        for (int valor : this.nuno) {
            if (valor > highest) {
                highest = valor;
            }
        }
        return highest;
    }

    @Override
    public boolean equals(Object other) {
        if (this.nuno == other) return true;
        if (other == null || this.getClass() != other.getClass()) return false;

        Nuno otherNuno = (Nuno) other;
        if (this.nuno.length != otherNuno.nuno.length) return false;
        boolean isEquals = true;
        for (int i = 0; i < this.nuno.length && isEquals; i++) {
            isEquals = this.nuno[i] == otherNuno.nuno[i];
        }
        return isEquals;
    }

}
