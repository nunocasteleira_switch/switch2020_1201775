import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioQuinzeTest {

    @Test
    void testOneGetTriangleClass() {
        double ab = 1;
        double ac = 1;
        double bc = 1;
        String expected = "Equilátero.";
        String result = ExercicioQuinze.getTriangleClass(ab, ac, bc);
        assertEquals(expected, result);
    }

    @Test
    void testTwoGetTriangleClass() {
        double ab = 1;
        double ac = 1;
        double bc = 2;
        String expected = "Isósceles.";
        String result = ExercicioQuinze.getTriangleClass(ab, ac, bc);
        assertEquals(expected, result);
    }

    @Test
    void testThreeGetTriangleClass() {
        double ab = 5;
        double ac = 7;
        double bc = 8;
        String expected = "Escaleno.";
        String result = ExercicioQuinze.getTriangleClass(ab, ac, bc);
        assertEquals(expected, result);
    }
}