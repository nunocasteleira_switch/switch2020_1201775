import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ExercicioVinteTest {

    @Test
    void getGardeningTookit1CostOnWeekDay() {
        int weekday = 2;
        int kitType = 1;
        int expected = 30;
        int result = ExercicioVinte.getGardeningTookitCost(weekday,kitType);
        assertEquals(expected, result);
    }


    @Test
    void getGardeningTookit2CostOnWeekDay() {
        int weekday = 2;
        int kitType = 2;
        int expected = 50;
        int result = ExercicioVinte.getGardeningTookitCost(weekday,kitType);
        assertEquals(expected, result);
    }

    @Test
    void getGardeningTookit3CostOnWeekDay() {
        int weekday = 2;
        int kitType = 3;
        int expected = 100;
        int result = ExercicioVinte.getGardeningTookitCost(weekday,kitType);
        assertEquals(expected, result);
    }

    @Test
    void getGardeningTookit1CostOnWeekends() {
        int weekday = 1;
        int kitType = 1;
        int expected = 40;
        int result = ExercicioVinte.getGardeningTookitCost(weekday,kitType);
        assertEquals(expected, result);
    }

    @Test
    void getGardeningTookit2CostOnWeekends() {
        int weekday = 1;
        int kitType = 2;
        int expected = 70;
        int result = ExercicioVinte.getGardeningTookitCost(weekday,kitType);
        assertEquals(expected, result);
    }

    @Test
    void getGardeningTookit3CostOnWeekends() {
        int weekday = 1;
        int kitType = 3;
        int expected = 140;
        int result = ExercicioVinte.getGardeningTookitCost(weekday,kitType);
        assertEquals(expected, result);
    }

    @Test
    void getDeliveryCost() {
        int km = 6;
        int expected = 12;
        int result = ExercicioVinte.getDeliveryCost(km);
        assertEquals(expected, result);
    }
}