import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDoisTest {

    @Test
    void getDigito1() {
        int numero = 264;
        int expected = 2;
        int result = ExercicioDois.getDigito1(numero);
        assertEquals(expected,result);
    }

    @Test
    void getDigito2() {
        int numero = 264;
        int expected = 6;
        int result = ExercicioDois.getDigito2(numero);
        assertEquals(expected,result);
    }

    @Test
    void getDigito3() {
        int numero = 264;
        int expected = 4;
        int result = ExercicioDois.getDigito3(numero);
        assertEquals(expected,result);
    }
}