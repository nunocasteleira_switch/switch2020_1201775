import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ExercicioDezanoveTest {

    @Test
    void getSalaryUnder36Hours() {
        int hoursWorked = 25;
        double expected = 187.5;
        double result = ExercicioDezanove.getSalaryForExploitedWorker(hoursWorked);
        assertEquals(expected, result, 0.0001);
    }

    @Test
    void getSalaryOf36Hours() {
        int hoursWorked = 36;
        double expected = 270;
        double result = ExercicioDezanove.getSalaryForExploitedWorker(hoursWorked);
        assertEquals(expected, result, 0.0001);
    }

    @Test
    void getSalaryOver36HoursAndUnder10EurosGratification() {
        int hoursWorked = 38;
        double expected = 290;
        double result = ExercicioDezanove.getSalaryForExploitedWorker(hoursWorked);
        assertEquals(expected, result, 0.0001);
    }

    @Test
    void getSalaryOver36HoursAndOver10EurosGratification() {
        int hoursWorked = 42;
        double expected = 360;
        double result = ExercicioDezanove.getSalaryForExploitedWorker(hoursWorked);
        assertEquals(expected, result, 0.0001);
    }
}