import static org.junit.jupiter.api.Assertions.*;

class ExercicioUmTest {

    @org.junit.jupiter.api.Test
    void testGetMedia() {
        int nota1 = 15;
        int nota2 = 16;
        int nota3 = 17;
        int peso1 = 1;
        int peso2 = 2;
        int peso3 = 3;
        double expected = 16.33;
        double result = ExercicioUm.getMedia(nota1, nota2, nota3, peso1, peso2, peso3);
        assertEquals(expected, result, 0.01);
    }
}