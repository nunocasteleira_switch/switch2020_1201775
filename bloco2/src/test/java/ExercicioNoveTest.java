import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioNoveTest {

    @Test
    void TestOneIsSequenceCrescent() {
        int digitOne = 1;
        int digitTwo = 2;
        int digitThree = 3;
        int expected = 1;
        int result = ExercicioNove.isSequenceCrescent(digitOne, digitTwo, digitThree);
        assertEquals(expected,result);
    }

    @Test
    void TestTwoIsSequenceCrescent() {
        int digitOne = 3;
        int digitTwo = 2;
        int digitThree = 1;
        int expected = 0;
        int result = ExercicioNove.isSequenceCrescent(digitOne, digitTwo, digitThree);
        assertEquals(expected,result);
    }


    @Test
    void TestThreeIsSequenceCrescent() {
        int digitOne = 2;
        int digitTwo = 2;
        int digitThree = 3;
        int expected = 0;
        int result = ExercicioNove.isSequenceCrescent(digitOne, digitTwo, digitThree);
        assertEquals(expected,result);
    }
}