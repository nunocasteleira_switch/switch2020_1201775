import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ExercicioSeisTest {

    @Test
    void getHoursMinutesSeconds() {
        int seconds = 50000;
        String expected = "13:53:20";
        String result = ExercicioSeis.getHoursMinutesSeconds(seconds);
        assertEquals(expected, result);
    }

    @Test
    void getSalutation() {
        int seconds = 21599;
        String expected = "Boa noite";
        String result = ExercicioSeis.getSalutation(seconds);
        assertEquals(expected, result);
    }
}