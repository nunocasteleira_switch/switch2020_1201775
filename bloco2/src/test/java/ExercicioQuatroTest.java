import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioQuatroTest {

    @Test
    void testOneGetSolution() {
        int x = 0;
        int expected = 0;
        double result = ExercicioQuatro.getSolution(x);
        assertEquals(expected, result, 0.0001);
    }

    @Test
    void testTwoGetSolution() {
        int x = 2;
        int expected = 0;
        double result = ExercicioQuatro.getSolution(x);
        assertEquals(expected, result, 0.0001);
    }

    @Test
    void testThreeGetSolution() {
        int x = 4;
        int expected = 8;
        double result = ExercicioQuatro.getSolution(x);
        assertEquals(expected, result, 0.0001);
    }
}