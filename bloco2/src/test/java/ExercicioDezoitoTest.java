import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ExercicioDezoitoTest {

    @Test
    void getProcessingTime() {
        int start = 48630; // 13:30:30
        int duration = 300;
        String expected = "13:35:30";
        String result = ExercicioDezoito.getProcessingTime(start, duration);
        assertEquals(expected, result);
    }

    @Test
    void getSecondsinHHMMSS() {
        int input = 300;
        String expected = "0:5:0";
        String result = ExercicioDezoito.getSecondsinHHMMSS(input);
        assertEquals(expected, result);
    }

    @Test
    void getHHMMSSinSeconds() {
        int hours = 13;
        int minutes = 30;
        int seconds = 30;
        int expected = 48630;
        int result = ExercicioDezoito.getHHMMSSinSeconds(hours, minutes, seconds);
        assertEquals(expected, result);
    }
}