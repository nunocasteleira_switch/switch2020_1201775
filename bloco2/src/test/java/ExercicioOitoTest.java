import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ExercicioOitoTest {

    @Test
    void testOneIsMultiplier() {
        int x = 4;
        int y = 2;
        int expected = 1;
        int result = ExercicioOito.isMultiplier(x, y);
        assertEquals(expected, result);
    }

    @Test
    void testTwoIsMultiplier() {
        int x = 3;
        int y = 5;
        int expected = 0;
        int result = ExercicioOito.isMultiplier(x, y);
        assertEquals(expected, result);
    }
}