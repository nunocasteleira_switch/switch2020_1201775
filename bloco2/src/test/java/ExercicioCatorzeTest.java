import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioCatorzeTest {

    @Test
    void getMedianDistance() {
        double miles1 = 1.65;
        double miles2 = 1.532;
        double miles3 = 2;
        double miles4 = 3.21;
        double miles5 = 4.2;
        double expected = 2.5184;
        double result = ExercicioCatorze.getMedianDistance(miles1, miles2, miles3, miles4, miles5);
        assertEquals(expected, result, 0.0001);
    }

    @Test
    void convertMilesIntoKm() {
        double miles = 3;
        double expected = 4.827;
        double result = ExercicioCatorze.convertMilesIntoKm(miles);
        assertEquals(expected, result, 0.0001);
    }
}