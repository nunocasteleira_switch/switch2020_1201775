import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioTresTest {

    @Test
    void getDistance() {
        Point first = new Point(1,2);
        Point second = new Point(2,4);
        double expected = 2.23606797749979;
        double result = ExercicioTres.getDistance(first, second);
        assertEquals(expected, result, 0.00001);
    }
}