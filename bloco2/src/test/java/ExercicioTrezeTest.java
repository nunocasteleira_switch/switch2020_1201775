import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioTrezeTest {

    @Test
    void getTimeCost() {
        float area = 30;
        int quantityOfTrees = 6;
        int quantityOfBushes = 4;
        double expected = 14200;
        double result = ExercicioTreze.getTimeCost(area, quantityOfTrees, quantityOfBushes);
        assertEquals(expected, result, 0.0001);
    }

    @Test
    void getMonetaryCost() {
        float area = 30;
        int quantityOfTrees = 6;
        int quantityOfBushes = 4;
        double expected = 520;
        double result = ExercicioTreze.getMonetaryCost(area, quantityOfTrees, quantityOfBushes, 14200);
        assertEquals(expected, result, 0.0001);
    }
}