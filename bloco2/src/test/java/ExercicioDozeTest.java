import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDozeTest {

    @Test
    void testOneGetPollutionAlert() {
        float index = 0.34f;
        String expected = "As indústrias do 1º grupo devem suspender as suas atividades.";
        String result = ExercicioDoze.getPollutionAlert(index);
        assertEquals(expected, result);
    }

    @Test
    void testTwoGetPollutionAlert() {
        float index = 0.43f;
        String expected = "As indústrias do 1º e 2º grupos devem suspender as suas atividades.";
        String result = ExercicioDoze.getPollutionAlert(index);
        assertEquals(expected, result);
    }

    @Test
    void testThreeGetPollutionAlert() {
        float index = 0.67f;
        String expected = "As indústrias do 1º, 2º e 3º grupos devem suspender as suas atividades.";
        String result = ExercicioDoze.getPollutionAlert(index);
        assertEquals(expected, result);
    }
}