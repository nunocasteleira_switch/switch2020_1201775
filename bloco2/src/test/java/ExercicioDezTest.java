import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezTest {

    @Test
    void testOneGetFinalPrice() {
        float price = 250;
        float expected = 100;
        double result = ExercicioDez.getFinalPrice(price);
        assertEquals(expected, result, 0.0001);
    }

    @Test
    void testTwoGetFinalPrice() {
        float price = 196;
        float expected = 117.6f;
        double result = ExercicioDez.getFinalPrice(price);
        assertEquals(expected, result, 0.0001);
    }
}