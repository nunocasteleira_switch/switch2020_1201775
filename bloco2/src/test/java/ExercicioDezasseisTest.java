import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezasseisTest {

    @Test
    void testOneGetTriangleAngleClass() {
        double a = 45;
        double b = 45;
        double c = 90;
        String expected = "Reto.";
        String result = ExercicioDezasseis.getTriangleAngleClass(a, b, c);
        assertEquals(expected, result);
    }

    @Test
    void testTwoGetTriangleAngleClass() {
        double a = 60;
        double b = 60;
        double c = 60;
        String expected = "Acutângulo.";
        String result = ExercicioDezasseis.getTriangleAngleClass(a, b, c);
        assertEquals(expected, result);
    }

    @Test
    void testThreeGetTriangleAngleClass() {
        double a = 100;
        double b = 40;
        double c = 40;
        String expected = "Obtusângulo.";
        String result = ExercicioDezasseis.getTriangleAngleClass(a, b, c);
        assertEquals(expected, result);
    }


    @Test
    void testFourGetTriangleAngleClass() {
        double a = 100;
        double b = 40;
        double c = 50;
        String expected = "Triângulo impossível.";
        String result = ExercicioDezasseis.getTriangleAngleClass(a, b, c);
        assertEquals(expected, result);
    }
}