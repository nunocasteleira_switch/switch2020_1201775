import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezasseteTest {

    @Test
    void getMinutesInHHMM() {
        int input = 222;
        String expected = "3:42";
        String result = ExercicioDezassete.getMinutesInHHMM(input);
        assertEquals(expected, result);
    }

    @Test
    void getHHMMinMinutes() {
        int hours = 3;
        int minutes = 42;
        int expected = 222;
        int result = ExercicioDezassete.getHHMMinMinutes(hours, minutes);
        assertEquals(expected,result);
    }

    @Test
    void getArrivalTimeOnDate() {
        int startMinutes = 222;
        int durationMinutes = 80;
        String expected = "O comboio chegará às 5:02 do próprio dia.";
        String result = ExercicioDezassete.getArrivalTime(startMinutes, durationMinutes);
    }

    @Test
    void getArrivalTimeOnDayAfter() {
        int startMinutes = 1430;
        int durationMinutes = 20;
        String expected = "O comboio chegará às 0:10 do dia seguinte.";
        String result = ExercicioDezassete.getArrivalTime(startMinutes, durationMinutes);
    }
}