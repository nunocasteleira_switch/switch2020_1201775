import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioOnzeTest {

    @Test
    void getClassClassification() {
        float aprovados = 0.45f;
        String expected = "Turma fraca";
        String result = ExercicioOnze.getClassClassification(aprovados);
        assertEquals(expected, result);
    }

    @Test
    void getClassClassificationWithValues() {
        float aprovados = 0.45f;
        float turmaFraca = 0.6f;
        String expected = "Turma fraca";
        String result = ExercicioOnze.getClassClassificationWithValues(aprovados, 0.2f, turmaFraca, 0.7f, 0.9f);
        assertEquals(expected, result);
    }
}