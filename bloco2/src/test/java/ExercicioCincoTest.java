import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioCincoTest {

    @Test
    void testUmGetVolume() {
        double area = 8;
        double expected = 1.53960;
        double result = ExercicioCinco.getVolume(area);
        assertEquals(expected,result,0.0001);
    }

    @Test
    void testDoisGetVolume() {
        double area = 15;
        double expected = 3.95284;
        double result = ExercicioCinco.getVolume(area);
        assertEquals(expected,result,0.0001);
    }

    @Test
    void testTresGetVolume() {
        double area = 36;
        double expected = 14.69693;
        double result = ExercicioCinco.getVolume(area);
        assertEquals(expected,result,0.0001);
    }
}