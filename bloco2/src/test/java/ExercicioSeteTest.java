import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioSeteTest {

    @Test
    void getWorkCost() {
        float buildingArea = 20;
        float painterSalaryPerDay = 64;
        int painterRendPerDay = 2 * 8;
        double expected = 128;
        double result = ExercicioSete.getWorkCost(buildingArea, painterSalaryPerDay, painterRendPerDay);
        assertEquals(expected, result, 0.0001);
    }

    @Test
    void getPaintCost() {
        float buildingArea = 20;
        float literCost = 6;
        float literRend = 3;
        double expected = 42;
        double result = ExercicioSete.getPaintCost(buildingArea, literCost, literRend);
        assertEquals(expected, result, 0.0001);
    }
}