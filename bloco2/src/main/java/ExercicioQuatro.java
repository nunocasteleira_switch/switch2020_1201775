import java.util.Scanner;

public class ExercicioQuatro {
    public static void main(String[] args) {
        exercicio4();
    }

    public static void exercicio4() {
        //      | x     se x < 0
        //F(x) =| 0     se x=0
        //      | x2–2x se x>0
        Scanner ler = new Scanner(System.in);
        System.out.println("Este programa resolve a seguinte função:");
        System.out.println("      | x     se x < 0\nF(x) =| 0     se x=0\n      | x2–2x se x>0");
        System.out.println("Introduza o valor de x");
        double x = ler.nextDouble();
        double solution = getSolution(x);
        System.out.println("O resultado é " + getSolution(x));
    }
    public static double getSolution(double x) {
        if (x <= 0) {
            return x;
        } else {
            return (Math.pow(x, 2) - 2 * x);
        }
    }
}
