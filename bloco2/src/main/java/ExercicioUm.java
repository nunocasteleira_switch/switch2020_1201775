import java.util.Scanner;

public class ExercicioUm {

    public static void main(String[] args) {
        exercicio1();
    }

    public static void exercicio1() {
        //Este exercício retorna a média ponderada de três notas e dos respetivos pesos.
        //Por exemplo 15, 16 e 17, com ponderações de 1, 2 e 3
        //( 15/1 + 16/2 + 17/3 ) / (1+2+3)
        int nota1, nota2, nota3, peso1, peso2, peso3;
        double mediaPesada;

        Scanner ler = new Scanner(System.in);
        nota1 = ler.nextInt();
        nota2 = ler.nextInt();
        nota3 = ler.nextInt();
        peso1 = ler.nextInt();
        peso2 = ler.nextInt();
        peso3 = ler.nextInt();

        mediaPesada = getMedia(nota1, nota2, nota3, peso1, peso2, peso3);
        if ( mediaPesada >= 8){
            System.out.println("O aluno cumpre a nota mínima exigida.");
        }
        System.out.println("O aluno tem média de " + mediaPesada);
    }

    public static double getMedia(int nota1, int nota2, int nota3, int peso1, int peso2, int peso3) {
        //Dar cast a double devido à divisão
        return (double) (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 + peso3);
    }
}
