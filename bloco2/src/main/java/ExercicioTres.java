import java.awt.*;
import java.util.Scanner;

public class ExercicioTres {
    public static void main(String[] args) {
        exercicio3();
    }

    public static void exercicio3() {
        //Este exercício divide um número com três dígitos nos seus dígitos,
        //para números superiores a 100 e inferiores a 999 (exclusive).
        int numero, digito1, digito2, digito3;
        Scanner ler = new Scanner(System.in);
        Point Um = new Point();
        Point Dois = new Point();
        System.out.println("Introduza as coordenadas do primeiro ponto.");
        Um.setLocation(ler.nextInt(),ler.nextInt());
        //System.out.println(Um.getLocation());
        System.out.println("Introduza as coordenadas do segundo ponto.");
        Dois.setLocation(ler.nextInt(),ler.nextInt());
        //System.out.println(Dois.getLocation());
        System.out.println("Os dois pontos estão à distância de " + getDistance(Um, Dois));
    }
    public static double getDistance(Point first, Point second) {
        return Math.sqrt(Math.pow((second.x - first.x), 2) + Math.pow((second.y - first.y), 2));
    }
}
