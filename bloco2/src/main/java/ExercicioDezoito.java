public class ExercicioDezoito {
    public static void main(String[] args) {
        System.out.println(getProcessingTime(getHHMMSSinSeconds(13,24,23), 13467));
    }

    public static String getProcessingTime(int startSeconds, int durationSeconds) {
        int endInSeconds = startSeconds + durationSeconds;
        //Hora de fim de processamento = Hora de início + tempo de processamento
        //tempo de início em HH:MM:SS e tempo em segundos
        return getSecondsinHHMMSS(endInSeconds);
    }

    public static String getSecondsinHHMMSS(int input) {
        int numberOfHours;
        int numberOfMinutes;
        int numberOfSeconds;

        numberOfHours = input / 3600;
        numberOfMinutes = (input % 3600) / 60;
        numberOfSeconds = (input % 3600) % 60;
        return numberOfHours + ":" + numberOfMinutes + ":" + numberOfSeconds;
    }

    public static int getHHMMSSinSeconds(int hours, int minutes, int seconds) {
        return (hours * 3600) + (minutes * 60) + seconds;
    }
}
