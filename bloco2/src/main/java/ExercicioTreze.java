public class ExercicioTreze {
    public static void main(String[] args) {
        exercicio13();
    }

    public static void exercicio13() {



    }

    public static double getTimeCost(float area, int quantityOfTrees, int quantityOfBushes) {
        int areaTime = 300; //s/m^2
        int treeTime = 600; //s each
        int bushTime = 400; //s each

        double seconds = ((area * areaTime) + (quantityOfTrees * treeTime) + (quantityOfBushes * bushTime));
        //Convert seconds in hours
        //round up (Math.ceil(secs / 3600)
        return seconds;
    }

    public static double getMonetaryCost(float area, int quantityOfTrees, int quantityOfBushes, int seconds) {
        int areaCost = 10; //€/m^2
        int treeCost = 20; //€ each
        int bushCost = 15; //€ each
        int workHourCost = 10; //€/h
        double hours = Math.ceil( seconds /  3600.0f); //conversion

        return ((area * areaCost) + (quantityOfTrees * treeCost) + (quantityOfBushes * bushCost) + (hours * workHourCost));
    }
}
