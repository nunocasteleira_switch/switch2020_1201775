import java.util.Scanner;

public class ExercicioDez {
    public static void main(String[] args) {
        exercicio10();
    }

    public static void exercicio10() {
        float price;
        System.out.println("Introduza o preço do artigo.");
        Scanner ler = new Scanner(System.in);

        price = ler.nextFloat();

        double finalPrice = getFinalPrice(price);
        System.out.println("O produto custará " + String.format("%.2f", finalPrice));
    }

    public static double getFinalPrice(float price) {
        float discount;

        if (price < 50) {
            discount = 20;
        } else if (price <= 100) {
            discount = 30;
        } else if (price <= 200) {
            discount = 40;
        } else {
            discount = 60;
        }

        return (price - (price * (discount / 100)));
    }
}
