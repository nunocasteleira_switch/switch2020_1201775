public class ExercicioVinte {
    public static void main(String[] args) {
    }

    public static int getGardeningTookitCost(int weekDay, int kitType) {
        //kitType = 1: Elementar;
        //          2: Semi-complete;
        //          3: Complete;
        int toolkitCost;
        switch (weekDay) {
            case 1:
            case 7:
            case 8:
                switch (kitType) {
                    case 1:
                        toolkitCost = 40;
                        break;
                    case 2:
                        toolkitCost = 70;
                        break;
                    case 3:
                    default:
                        toolkitCost = 140;
                        break;
                }
                break;
            default:
                switch (kitType) {
                    case 1:
                        toolkitCost = 30;
                        break;
                    case 2:
                        toolkitCost = 50;
                        break;
                    case 3:
                    default:
                        toolkitCost = 100;
                        break;
                }
                break;
        }
        return toolkitCost;
    }

    public static int getDeliveryCost(int km) {
        int costPerKm = 2;
        return km * costPerKm;
    }
}
