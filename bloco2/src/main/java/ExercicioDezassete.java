public class ExercicioDezassete {
    public static void main(String[] args) {
        exercicio17();
    }

    public static void exercicio17() {
        System.out.println(getArrivalTime(getHHMMinMinutes(23,42), getHHMMinMinutes(1,17)));
    }

    public static String getArrivalTime(int startMinutes, int durationMinutes) {
        int arrivalInMinutes = startMinutes + durationMinutes;
        if (arrivalInMinutes < getHHMMinMinutes(24, 0)) {
            return "O comboio chegará às " + getMinutesInHHMM(arrivalInMinutes) + " do próprio dia.";
        } else {
            return "O comboio chegará às " + getMinutesInHHMM(arrivalInMinutes - getHHMMinMinutes(24, 0)) + " do dia seguinte.";
        }
    }

    public static String getMinutesInHHMM(int input) {
        int numberOfHours;
        int numberOfMinutes;
        numberOfHours = input / 60;
        numberOfMinutes = input % 60;
        return numberOfHours + ":" + numberOfMinutes;
    }

    public static int getHHMMinMinutes(int hours, int minutes) {
        return (hours * 60) + minutes;
    }
}
