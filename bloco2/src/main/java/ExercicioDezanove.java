import javafx.scene.control.ChoiceDialog;

public class ExercicioDezanove {
    public static void main(String[] args) {
        //exercicio19();
        //if horas < 36 = rendimento 7.5€/h
        //if horas > 36+5 = rendimento 10€/h das horas extra
        //if horas > 36+5+ = rendimento 15€/h das horas extra
    }
    public static double getSalaryForExploitedWorker(int hoursWorked){
        int regularHours;
        int extraHours = 0;
        double gratification;

        if (hoursWorked > 36 ) {
            extraHours = hoursWorked - 36;
            regularHours = hoursWorked - extraHours;//has to be 36.
        } else regularHours = hoursWorked;

        if (extraHours > 5 ) {
            gratification = 15;
        } else if (extraHours != 0){
            gratification = 10;
        } else {
            gratification = 0;
        }

        return regularHours * 7.5 + extraHours * gratification;
    }
}
