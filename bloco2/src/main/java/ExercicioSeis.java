import java.util.Scanner;

public class ExercicioSeis {
    public static void main(String[] args) {
        exercicio6();
    }

    public static void exercicio6() {
        int input;
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o número de segundos:");
        input = ler.nextInt();

        System.out.println(getHoursMinutesSeconds(input));
        System.out.println(getSalutation(input));
    }
    public static String getHoursMinutesSeconds(int input) {
        int numberOfHours;
        int numberOfMinutes;
        int numberOfSeconds;

        numberOfHours = input / 3600;
        numberOfMinutes = (input % 3600) / 60;
        numberOfSeconds = (input % 3600) % 60;
        return numberOfHours + ":" + numberOfMinutes + ":" + numberOfSeconds;
    }
    public static String getSalutation(int input) {
        float numberOfHours;
        numberOfHours = (float) input / 3600;
        if (numberOfHours >= 6 && numberOfHours <= 12)
            return "Bom dia";
        else if (numberOfHours >= 12 && numberOfHours <= 20)
            return "Boa tarde";
        else return "Boa noite";
    }
}
