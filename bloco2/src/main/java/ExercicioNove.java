import java.util.Scanner;

public class ExercicioNove {
    public static void main(String[] args) {
        exercicio9();
    }

    public static void exercicio9() {
        int input;
        int digitOne = 0, digitTwo = 0, digitThree = 0;
        Scanner ler = new Scanner(System.in);
        input = ler.nextInt();

        if (input >= 100 && input <= 999) {
            digitThree = input % 10;
            digitTwo = (input / 10) % 10;
            digitOne = (input / 100) % 10;
        }
        switch (isSequenceCrescent(digitOne, digitTwo, digitThree)) {
            case 0:
            default:
                System.out.println("A sequência não é crescente.");
                break;
            case 1:
                System.out.println("A sequência é crescente.");
        }
    }

    public static int isSequenceCrescent(int digitOne, int digitTwo, int digitThree) {
        if (digitOne >= digitTwo) {
            return 0;
        } else if (digitTwo >= digitThree) {
            return 0;
        } else {
            return 1;
        }
    }
}
