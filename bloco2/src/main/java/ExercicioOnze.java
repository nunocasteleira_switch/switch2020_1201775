import java.util.Scanner;

public class ExercicioOnze {

    public static void main(String[] args) {
        exercicio11();
    }

    public static void exercicio11() {
        int menuOption;
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza a percentagem de aprovados (entre 0 e 1).");
        float aprovados = ler.nextFloat();
        System.out.println("Pretende os valores predefinidos ou definir limites?\n0: Predefinidos\n1: Definidos por mim");
        menuOption = ler.nextInt();
        switch (menuOption){
            case 0:
            default:
                String classClassification = getClassClassification(aprovados);
                System.out.println(classClassification);
                break;
            case 1:
                float turmaMa, turmaFraca, turmaRazoavel, turmaBoa;
                System.out.println("Introduza o limite para turma má:");
                turmaMa = ler.nextFloat();
                System.out.println("Introduza o limite para turma fraca:");
                turmaFraca = ler.nextFloat();
                System.out.println("Introduza o limite para turma razoável:");
                turmaRazoavel = ler.nextFloat();
                System.out.println("Introduza o limite para turma boa:");
                turmaBoa = ler.nextFloat();
                String classClassificationWithValues = getClassClassificationWithValues(aprovados, turmaMa, turmaFraca, turmaRazoavel, turmaBoa);
                System.out.println(classClassificationWithValues);
                break;
        }
    }

    public static String getClassClassification(float aprovados) {
        if (aprovados < 0 || aprovados > 1) {
            return "Valor inválido";
        } else if (aprovados < 0.2) {
            return "Turma má";
        } else if (aprovados < 0.5) {
            return "Turma fraca";
        } else if (aprovados < 0.7) {
            return "Turma razoável";
        } else if (aprovados < 0.9) {
            return "Turma boa";
        } else {
            return "Turma excelente";
        }
    }

    public static String getClassClassificationWithValues(float aprovados, float turmaMa, float turmaFraca, float turmaRazoavel, float turmaBoa) {
        Scanner ler = new Scanner(System.in);

        if (aprovados < 0 || aprovados > 1) {
            return "Valor inválido";
        } else if (aprovados < turmaMa) {
            return "Turma má";
        } else if (aprovados < turmaFraca) {
            return "Turma fraca";
        } else if (aprovados < turmaRazoavel) {
            return "Turma razoável";
        } else if (aprovados < turmaBoa) {
            return "Turma boa";
        } else {
            return "Turma excelente";
        }
    }
}
