import java.util.Scanner;

public class ExercicioDois {
    public static void main(String[] args) {
        exercicio2();
    }

    public static void exercicio2() {
        //Este exercício divide um número com três dígitos nos seus dígitos,
        //para números superiores a 100 e inferiores a 999 (exclusive).
        int numero, digito1, digito2, digito3;
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza um número com três dígitos.");
        numero = ler.nextInt();

        //TODO: modularizar
        if ( numero < 100 || numero > 999 ){
            System.out.println("O número não tem 3 dígitos.");
        } else {
            digito3 = getDigito3(numero);
            digito2 = getDigito2(numero);
            digito1 = getDigito1(numero);
            System.out.println(digito1 + " " + digito2 + " " + digito3);
        }
    }

    public static int getDigito1(int numero) {
        int digito1;
        digito1 = (numero/100) % 10;
        return digito1;
    }

    public static int getDigito2(int numero) {
        int digito2;
        digito2 = (numero/10) % 10;
        return digito2;
    }

    public static int getDigito3(int numero) {
        int digito3;
        digito3 = numero % 10;
        return digito3;
    }
}
