import java.util.Scanner;

public class ExercicioCatorze {
    public static void main(String[] args) {
        exercicio14();
    }
    public static void exercicio14() {
        double miles1, miles2, miles3, miles4, miles5, milesMedian;
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza cinco valores em milhas, para o cálculo da média.");
        miles1 = ler.nextFloat();
        miles2 = ler.nextFloat();
        miles3 = ler.nextFloat();
        miles4 = ler.nextFloat();
        miles5 = ler.nextFloat();
        milesMedian = getMedianDistance(miles1, miles2, miles3, miles4, miles5);
        double medianInKm = convertMilesIntoKm(milesMedian);
        System.out.println("O estafeta fez " + medianInKm + "km em média.");
    }

    public static double getMedianDistance(double miles1, double miles2, double miles3, double miles4, double miles5) {
        return (miles1 + miles2 + miles3 + miles4 + miles5) / 5.0;
    }

    public static double convertMilesIntoKm(double miles) {
        return miles * 1.609;
    }
}
