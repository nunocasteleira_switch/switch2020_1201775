import java.util.Scanner;

public class ExercicioDoze {
    public static void main(String[] args) {
        exercicio12();
    }
    public static void exercicio12() {
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o índice de poluição.");
        float index = ler.nextFloat();
        System.out.println(getPollutionAlert(index));
    }

    public static String getPollutionAlert(float index) {
        if (index < 0.3) {
            return "";
        } else if (index < 0.4) {
            return "As indústrias do 1º grupo devem suspender as suas atividades.";
        } else if (index < 0.5) {
            return "As indústrias do 1º e 2º grupos devem suspender as suas atividades.";
        } else {
            return "As indústrias do 1º, 2º e 3º grupos devem suspender as suas atividades.";
        }
    }
}
