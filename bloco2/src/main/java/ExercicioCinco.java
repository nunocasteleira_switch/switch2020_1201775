import java.util.Scanner;

public class ExercicioCinco {
    public static void main(String[] args) {
        exercicio5();
    }

    public static void exercicio5() {
        double area;
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza a área da base do cubo.");
        area = ler.nextDouble();
        if (area > 0) {
            double volume = getVolume(area);
            getCubeCategory(volume);
        } else System.out.println("Valor da área inválido.");
    }

    public static void getCubeCategory(double volume) {
        System.out.println("Volume do cubo = " + volume);
        if (volume <= 1) {
            System.out.println("O cubo é pequeno.");
        } else if (volume <= 2) {
            System.out.println("O cubo é médio.");
        } else System.out.println("O cubo é grande.");
    }

    public static double getVolume(double area) {
        double aresta;
        double volume;
        aresta = Math.sqrt(area / 6);
        volume = Math.pow(aresta, 3);
        return volume;
    }
}
