public class ExercicioDezasseis {
    public static void main(String[] args) {
        exercicio16();
    }

    public static void exercicio16() {
        double a = 100;
        double b = 40;
        double c = 50;
        System.out.println(getTriangleAngleClass(a, b, c));
    }

    public static String getTriangleAngleClass(double a, double b, double c) {
        if (a > 0 && b > 0 && c > 0 && a < 180 && b < 180 && c < 180 && a + b + c == 180) {
            if (a > 90 || b > 90 || c > 90) {
                return "Obtusângulo.";
            } else if (a == 90 || b == 90 || c == 90) {
                return "Reto.";
            } else if (a < 90 && b < 90 && c < 90) {
                return "Acutângulo.";
            } else {
                return "Triângulo impossível.";
            }
        } else {
            return "Triângulo impossível.";
        }
    }
}
