import java.util.Scanner;

public class ExercicioOito {
    public static void main(String[] args) {
        exercicio8();
    }

    public static void exercicio8() {
        int x, y;
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza x");
        x = ler.nextInt();
        System.out.println("Introduza y");
        y = ler.nextInt();

        int xy = isMultiplier(x, y);
        //noinspection SuspiciousNameCombination
        int yx = isMultiplier(y, x);

        if (xy == 1) {
            System.out.println(x + " é múltiplo de " + y);
        } else if (yx == 1) {
            System.out.println(y + " é múltiplo de " + x);
        } else {
            System.out.println(x + " não é múltiplo nem divisor de " + y);
        }

    }

    public static int isMultiplier(int x, int y) {
        if (x % y == 0) {
            return 1;
        } else
            return 0;
    }
}
