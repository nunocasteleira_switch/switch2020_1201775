public class ExercicioQuinze {
    public static void main(String[] args) {
        exercicio15();
    }
    public static void exercicio15() {
        String classification = getTriangleClass(5,7,8);
        System.out.println(classification);
    }

    public static String getTriangleClass(double ab, double ac, double bc) {
        if (ab > 0 && ac > 0 && bc >0) {
            if (ab <= ac + bc && ac <= ab + bc && bc <= ab + ac) {
                if ( ab == ac && ac == bc) {
                    return "Equilátero.";
                } else if (ab == ac || ab == bc || bc == ac) {
                    return "Isósceles.";
                } else {
                    return "Escaleno.";
                }
            } else {
                return "Comprimentos errados.";
            }
        } else  {
            return "Comprimentos errados.";
        }
    }
}
