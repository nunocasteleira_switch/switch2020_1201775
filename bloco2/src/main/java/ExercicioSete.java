import java.util.Scanner;

public class ExercicioSete {
    public static void main(String[] args) {
        exercicio7();
    }
    //Custo tinta

    public static void exercicio7() {
        int menuOption;
        float buildingArea, literCost, literRend, painterSalaryPerDay;
        int hoursInDay = 8;
        int painterRendPerHour = 2;
        int painterRendPerDay = hoursInDay * painterRendPerHour;
        Scanner ler = new Scanner(System.in);
        //System.out.println( (int) Math.ceil((double) 350 / 8 ) );

        System.out.println("Introduza a área do edifício");
        buildingArea = ler.nextFloat();
        System.out.println("Introduza o custo por litro de tinta.");
        literCost = ler.nextFloat();
        System.out.println("Introduza o rendimento por litro de tinta.");
        literRend = ler.nextFloat();
        System.out.println("Introduza o salário do pintor por dia");
        painterSalaryPerDay = ler.nextFloat();
        System.out.println("[Menu] O que pretende calcular?\n1: O custo da tinta.\n2: O custo da mão de obra.\n3: O custo total");
        menuOption = ler.nextInt();

        switch (menuOption) {
            case 1:
                double paintCost = getPaintCost(buildingArea, literCost, literRend);
                System.out.println("O custo da tinta é " + paintCost);
                break;
            case 2:
                double workCost = getWorkCost(buildingArea, painterSalaryPerDay, painterRendPerDay);
                System.out.println("O custo da mão de obra é " + workCost);
                break;
            case 3: default:
                paintCost = getPaintCost(buildingArea, literCost, literRend);
                workCost = getWorkCost(buildingArea, painterSalaryPerDay, painterRendPerDay);
                System.out.println("O custo da tinta é " + paintCost);
                System.out.println("O custo da mão de obra é " + workCost);
                System.out.println("O custo total é " + (paintCost + workCost));
                break;
        }
    }


    public static double getWorkCost(float buildingArea, float painterSalaryPerDay, int painterRendPerDay) {
        int workersNeeded;

        if (buildingArea < 100) {
            workersNeeded = 1;
        } else if (buildingArea < 300) {
            workersNeeded = 2;
        } else if (buildingArea < 1000) {
            workersNeeded = 3;
        } else {
            workersNeeded = 4;
        }

        double workCost = (Math.ceil(buildingArea / (workersNeeded * painterRendPerDay))) * painterSalaryPerDay;
        if (workersNeeded == 1) {
            System.out.println("É necessário " + workersNeeded + " pintor.");
        } else {
            System.out.println("São necessários " + workersNeeded + " pintores.");
        }
        return workCost;
    }

    public static double getPaintCost(float buildingArea, float literCost, float literRend) {
        double paintCost = (Math.ceil(buildingArea / literRend)) * literCost;
        return paintCost;
    }
}
