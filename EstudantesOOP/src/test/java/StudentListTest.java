import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class StudentListTest {

    @Test
    void toArray() {
        Student student1 = new Student(1200145, "Sampaio");
        Student student2 = new Student(1200054, "Moreira");
        Student student3 = new Student(1200086, "Silva");
        Student[] students = {student1, student2, student3};
        StudentList studentList = new StudentList(students);
        Student[] expected = {student1, student2, student3};
        Student[] result = studentList.toArray();
        assertArrayEquals(expected, result);
    }

    @Test
    void sortByNumberAsc() {
        Student student1 = new Student(1200145, "Sampaio");
        Student student2 = new Student(1200054, "Moreira");
        Student student3 = new Student(1200086, "Silva");

        Student[] students = {student1, student2, student3};
        StudentList studentList = new StudentList(students);
        Student[] expected = {student2, student3, student1};
        studentList.sortByNumberAsc();
        assertArrayEquals(expected, studentList.toArray());
    }

    @Test
    void sortByNumberDesc() {
        Student student1 = new Student(1200145, "Sampaio");
        Student student2 = new Student(1200054, "Moreira");
        Student student3 = new Student(1200086, "Silva");

        Student[] students = {student1, student2, student3};
        StudentList studentList = new StudentList(students);
        Student[] expected = {student1, student3, student2};
        studentList.sortByNumberDesc();
        assertArrayEquals(expected, studentList.toArray());
    }

    @Test
    void sortByGradeAsc() {
        Student student1 = new Student(1200145, "Sampaio");
        student1.doEvaluation(12);
        Student student2 = new Student(1200054, "Moreira");
        student2.doEvaluation(11);
        Student student3 = new Student(1200086, "Silva");
        student3.doEvaluation(13);

        Student[] students = {student1, student2, student3};
        StudentList studentList = new StudentList(students);
        Student[] expected = {student2, student1, student3};
        studentList.sortByGradeAsc();
        assertArrayEquals(expected, studentList.toArray());
    }

    @Test
    void sortByGradeDesc() {
        Student student1 = new Student(1200145, "Sampaio");
        student1.doEvaluation(12);
        Student student2 = new Student(1200054, "Moreira");
        student2.doEvaluation(11);
        Student student3 = new Student(1200086, "Silva");
        student3.doEvaluation(13);

        Student[] students = {student1, student2, student3};
        StudentList studentList = new StudentList(students);
        Student[] expected = {student3, student1, student2};
        studentList.sortByGradeDesc();
        assertArrayEquals(expected, studentList.toArray());
    }
}
