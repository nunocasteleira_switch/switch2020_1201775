import java.util.ArrayList;

public class StudentList {
    //Attributes
    private ArrayList<Student> students = new ArrayList();

    //Constructors
    public StudentList() {

    }

    public StudentList(Student[] sts) {
        if (sts != null) {
            for (Student st : sts) {
                students.add(st);
            }
        }
    }

    public Student[] toArray() {
        Student[] copy = new Student[this.students.size()];
        for (int i = 0; i < this.students.size(); i++) {
            copy[i] = students.get(i);
        }
        return copy;
    }

    public void sortByNumberAsc() {
        Student tempStudent = null;
        for (int index1 = 0; index1 < this.students.size(); index1++) {
            for (int index2 = index1 + 1; index2 < this.students.size(); index2++) {
                if (students.get(index1).compareByNumber(students.get(index2)) > 0) {
                    tempStudent = students.get(index1);
                    students.set(index1, students.get(index2));
                    students.set(index2, tempStudent);
                }
            }
        }
    }

    public void sortByNumberDesc() {
        Student tempStudent = null;
        for (int index1 = 0; index1 < this.students.size(); index1++) {
            for (int index2 = index1 + 1; index2 < students.size(); index2++) {
                if (students.get(index1).compareByNumber(students.get(index2)) < 0) {
                    tempStudent = students.get(index1);
                    students.set(index1, students.get(index2));
                    students.set(index2, tempStudent);
                }
            }
        }
    }
    public void sortByGradeAsc() {
        Student tempStudent = null;
        for (int index1 = 0; index1 < this.students.size(); index1++) {
            for (int index2 = index1 + 1; index2 < this.students.size(); index2++) {
                if (students.get(index1).compareByGrade(students.get(index2)) > 0) {
                    tempStudent = students.get(index1);
                    students.set(index1, students.get(index2));
                    students.set(index2, tempStudent);
                }
            }
        }
    }

    public void sortByGradeDesc() {
        Student tempStudent = null;
        for (int index1 = 0; index1 < this.students.size(); index1++) {
            for (int index2 = index1 + 1; index2 < students.size(); index2++) {
                if (students.get(index1).compareByGrade(students.get(index2)) < 0) {
                    tempStudent = students.get(index1);
                    students.set(index1, students.get(index2));
                    students.set(index2, tempStudent);
                }
            }
        }
    }
}
