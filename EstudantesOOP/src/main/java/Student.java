public class Student {
    //Attributes
    private int number;
    private String name;
    private int grade;

    //Constructors
    public Student(int number, String name) {
        if (!this.isValidNumber(number)) {
            throw new IllegalArgumentException("Student number needs to be 7 digits number (e.g. 1190001).");
        }
        if (!this.isValidName(name)) {
            throw new IllegalArgumentException("Student name cannot be shorter than 5 chars.");
        }
        this.number = number;
        this.name = name.trim();
        this.grade = -1;
    }

    public static boolean sortStudentsByNumberAsc(Student[] students) {
        if (students == null) {
            return false;
        }
        Student tempStudent = null;

        for (int index1 = 0; index1 < students.length; index1++) {
            for (int index2 = index1 + 1; index2 < students.length; index2++) {
                if (students[index1].compareByNumber(students[index2]) > 0) {
                    tempStudent = students[index1];
                    students[index1] = students[index2];
                    students[index2] = tempStudent;
                }
            }
        }
        return true;
    }

    public int compareByNumber(Student other) {
        if (this.number < other.number) {
            return -1;
        }
        if (this.number > other.number) {
            return 1;
        }
        return 0;
    }

    public int compareByGrade(Student other) {
        if (this.grade < other.grade) {
            return -1;
        }
        if (this.grade > other.grade) {
            return 1;
        }
        return 0;
    }

    //Getters and Setters
    public boolean doEvaluation(int grade) {
        if (!this.isEvaluated() && this.isValidGrade(grade)) {
            this.grade = grade;
            return true;
        }
        return false;
    }

    public int getGrade() {
        return this.grade;
    }

    //Business Methods
    private boolean isValidNumber(int number) {
        if (number != Math.abs(number)) {
            return false;
        }
        String strNumber = Integer.toString(number);
        return (strNumber.length() == 7);
    }

    private boolean isValidName(String name) {
        if (name == null) {
            return false;
        }
        name = name.trim();
        return !(name.length() < 5);
    }

    private boolean isValidGrade(int grade) {
        return grade >= 0 && grade <= 20;
    }

    private boolean isEvaluated() {
        return this.grade >= 0;
    }
}
