public class ExercicioDezanove {
    public static void main(String[] args) {

    }

    /**
     * This method returns a mask matrix in which identifies the given numbers to the User in the beginning of the game.
     *
     * @param baseSudokuMatrix the starting matrix in the beginning of the game.
     * @return a boolean matrix with 1 in the given numbers to the User in the beginning of the game, else 0.
     */
    public static int[][] getMaskSudokuMatrix(int[][] baseSudokuMatrix) {
        if (baseSudokuMatrix == null || baseSudokuMatrix.length == 0) {
            return null;
        }
        int[][] maskSudokuMatrix = new int[9][9];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                //Ternary Operator
                maskSudokuMatrix[i][j] = (baseSudokuMatrix[i][j] > 0) ? 1 : 0;
            }
        }
        return maskSudokuMatrix;
    }

    /**
     * This method adds a number to the Sudoku Matrix on a specified cell by the user.
     *
     * @param baseSudokuMatrix the starting matrix before the move.
     * @param sudokuNumber     the number to input.
     * @param coordX           the X coordinate.
     * @param coordY           the Y coordinate.
     * @return the updated matrix after the move.
     */
    public static int[][] insertNumberOnBaseSudokuMatrix(int[][] baseSudokuMatrix, int sudokuNumber, int coordX, int coordY) {
        if (areEmptyCellsOnSudokuMatrix(baseSudokuMatrix)) {
            baseSudokuMatrix[coordX][coordY] = sudokuNumber;
        }
        return baseSudokuMatrix;
    }

    /**
     * This method evaluates the state of the Matrix. When there's no more Empty Cells, the game should end.
     *
     * @param baseSudokuMatrix the Sudoku matrix.
     * @return true while there's still empty cells.
     */
    public static boolean areEmptyCellsOnSudokuMatrix(int[][] baseSudokuMatrix) {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                //If cell is empty, it has the value of 0.
                //isEmptyCells = baseSudokuMatrix[i][j] == 1;
                if (baseSudokuMatrix[i][j] == 0) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * This method returns a mask matrix with 1 signaling coordinates with the same number.
     *
     * @param baseSudokuMatrix the Sudoku matrix
     * @param sudokuNumber     the searching number.
     * @return a boolean matrix with 1 in every coordinate with the same sudokuNumber.
     */
    public static int[][] getMatrixWithAllCoordsWithSameNumber(int[][] baseSudokuMatrix, int sudokuNumber) {
        int[][] sameCoordsMatrix = new int[9][9];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (baseSudokuMatrix[i][j] == sudokuNumber) {
                    sameCoordsMatrix[i][j] = 1;
                } else {
                    sameCoordsMatrix[i][j] = 0;
                }
            }
        }
        return sameCoordsMatrix;
    }

    /**
     * This method asesses if we can put a specified number on a given row.
     *
     * @param baseSudokuMatrix the current state of the Sudoku matrix.
     * @param sudokuNumber     the evaluating number.
     * @param coordX           the row index.
     * @return true if possible (the number isn't still on that row).
     */
    public static boolean isNumberLegalInRow(int[][] baseSudokuMatrix, int sudokuNumber, int coordX) {
        boolean isNumberLegalInRow = true;
        for (int i = 0; i < 9 && isNumberLegalInRow; i++) {
            isNumberLegalInRow = baseSudokuMatrix[coordX][i] != sudokuNumber;
        }
        return isNumberLegalInRow;
    }

    /**
     * This method asesses if we can put a specified number on a given column.
     *
     * @param baseSudokuMatrix the current state of the Sudoku matrix.
     * @param sudokuNumber     the evaluating number.
     * @param coordY           the column index.
     * @return true if possible (the number isn't still on that column).
     */
    public static boolean isNumberLegalInColumn(int[][] baseSudokuMatrix, int sudokuNumber, int coordY) {
        boolean isNumberLegalInColumn = true;
        for (int i = 0; i < 9 && isNumberLegalInColumn; i++) {
            isNumberLegalInColumn = baseSudokuMatrix[i][coordY] != sudokuNumber;
        }
        return isNumberLegalInColumn;
    }

    /**
     * This method asesses if we can put a specified number on a given 3x3 small matrix.
     *
     * @param baseSudokuMatrix the current state of the Sudoku matrix.
     * @param sudokuNumber     the evaluating number.
     * @param coordX           the row index.
     * @param coordY           the column index.
     * @return true if possible (the number isn't still on that 3x3 small matrix).
     */
    public static boolean isNumberLegalInSquare(int[][] baseSudokuMatrix, int sudokuNumber, int coordX, int coordY) {
        boolean isNumberLegalInSquare = true;
        /*
         |0,0|   |   |0,3|   |   |0,6|   |   |
         |   |   |   |   |   |   |   |   |   |
         |   |   |   |   |   |   |   |   |   |
         |3,0|   |   |3,3|   |   |3,6|   |   |
         |   |   |   |   |   |   |   |   |   |
         |   |   |   |   |   |   |   |   |   |
         |6,0|   |   |6,3|   |   |6,6|   |   |
         |   |   |   |   |   |   |   |   |   |
         |   |   |   |   |   |   |   |   |   |
        My strategy is to obtain the coordinates of the 3x3 small matrix by iterating [0,3[
        And incrementing the first index of the top leftmost cell.
        I achieve this by integer dividing the coordinate by 3: It changes its integer value three by three rows/columns.
        For example, the middle 3x3 matrix (the 1,1 3x3 matrix) starts at 3,3.
        In essence: startCell (S) + iterator (i): S + i, S + i+1, S + i+2.
        For it to iterate over those cells, I have to get 3 + 0, 3 + 1, 3 + 2 ( i < 3, j < 3).
        To iterate over the cells starting on cell 6,*, I have to get 6 + 0, 6 + 1, 6 + 2.
        But I'm dividing it by 3, getting 1 or 2. So multiplying it by 3, I always get the firstmost index.
        */

        //By Sudoku rules, we can't have the same number repeated times in a 3x3 small matrix.

        int subSquareX = (coordX / 3) * 3;
        int subSquareY = (coordY / 3) * 3;

        for (int i = 0; i < 3 && isNumberLegalInSquare; i++) {
            for (int j = 0; j < 3 && isNumberLegalInSquare; j++) {
                isNumberLegalInSquare = baseSudokuMatrix[subSquareX + i][subSquareY + j] != sudokuNumber;
            }
        }
        return isNumberLegalInSquare;
    }

    /**
     * THis method aggregate all conditions to assess the legality of a number on a given cell.
     *
     * @param baseSudokuMatrix the current state of the Sudoku matrix.
     * @param sudokuNumber     the evaluating number.
     * @param coordX           the row index.
     * @param coordY           the column index.
     * @return true if possible.
     */
    public static boolean isNumberLegal(int[][] baseSudokuMatrix, int sudokuNumber, int coordX, int coordY) {
        boolean isNumberLegalInRow = isNumberLegalInRow(baseSudokuMatrix, sudokuNumber, coordX);
        boolean isNumberLegalInColumn = isNumberLegalInColumn(baseSudokuMatrix, sudokuNumber, coordY);
        boolean isNumberLegalInSquare = isNumberLegalInSquare(baseSudokuMatrix, sudokuNumber, coordX, coordY);
        return isNumberLegalInRow && isNumberLegalInColumn && isNumberLegalInSquare;
    }

    /**
     * This method assesses if any given coordinate is "printed" or if it's assigned by the user.
     * If it's assigned or empty, the method returns true.
     * It relies on the mask matrix created at the beginning of the game.
     *
     * @param maskSudokuMatrix the mask Sudoku matrix.
     * @param coordX           the row index.
     * @param coordY           the column index.
     * @return true if possible.
     */
    public static boolean canAddOrChangeNumber(int[][] maskSudokuMatrix, int coordX, int coordY) {
        //If there's a 0 in mask matrix, then the number can be added or changed.
        return maskSudokuMatrix[coordX][coordY] == 0;
    }

    /**
     * This method assesses the state of the Sudoku Matrix at the end of the game.
     * It only works if the matrix is finished (e.g. has no empty cells).
     *
     * @param baseSudokuMatrix the finished state of the Sudoku matrix.
     * @return true if matrix is valid.
     */
    public static boolean isSudokuMatrixVerified(int[][] baseSudokuMatrix) {
        //By Sudoku rules, a valid matrix has the product of each row or column equal to (9!).
        final int FACTORIAL_NINE = 362880;
        boolean isSudokuVerified = true;
        for (int i = 0; i < baseSudokuMatrix.length && isSudokuVerified; i++) {
            int[] line = new int[baseSudokuMatrix.length];
            for (int j = 0; j < baseSudokuMatrix[i].length; j++) {
                line[j] = baseSudokuMatrix[i][j];
            }
            isSudokuVerified = ExercicioDez.productOfValuesInArray(line) == FACTORIAL_NINE;
        }
        return isSudokuVerified;
    }

}
