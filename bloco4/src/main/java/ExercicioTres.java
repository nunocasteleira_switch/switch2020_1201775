public class ExercicioTres {
    public static void main(String[] args) {

    }

    public static int sumElementsInArray(int[] numbers) {
        int sum = 0;
        if (numbers != null) {
            if (numbers.length > 0) {
                for (int digit : numbers) {
                    sum += digit;
                }
            }
        }
        return sum;
    }
}
