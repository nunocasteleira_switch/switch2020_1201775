public class ExercicioCatorze {
    public static void main(String[] args) {

    }

    public static boolean isMatrixRect(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return false;
        }
        boolean isRect = false;
        int rectLength = ExercicioDoze.getCommonLengthNumber(matrix);
        if (rectLength != -1) {
            for (int[] array : matrix) {
                isRect = array.length != matrix.length;
            }
        }
        return isRect;
    }
}
