public class ExercicioSeis {
    public static void main(String[] args) {
        //O exercício seis solicita que se faça o que já resolvi no exercício quatro.
        //O Método truncateArrayWithGivenLength retorna um  vetor com os primeiros N
        // elementos de um vetor passado como parâmetro.
        int[] array = {1,2,3,4};
        int count = 2;
        //noinspection ResultOfMethodCallIgnored
        ExercicioQuatro.truncateArrayWithGivenLength(array, count);
    }
}
