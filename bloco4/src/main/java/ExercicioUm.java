public class ExercicioUm {
    public static void main(String[] args) {    }

    public static int countDigits (int number){
        int count = 0;
        while (number > 0 ) {
            count++;
            number /= 10;
        }
        return count;
    }
}
