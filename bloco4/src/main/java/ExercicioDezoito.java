import java.util.Scanner;

public class ExercicioDezoito {
    public static void main(String[] args) {
        //Define puzzle and solutions
        char[][] wordSearchPuzzle = {
                {'X', 'S', 'I', 'M', 'A', 'N', 'U', 'S', 'T', 'B'},
                {'Z', 'T', 'E', 'C', 'T', 'O', 'N', 'I', 'C', 'A'},
                {'J', 'J', 'S', 'O', 'M', 'S', 'I', 'S', 'O', 'G'},
                {'O', 'T', 'O', 'M', 'E', 'R', 'A', 'M', 'R', 'E'},
                {'R', 'V', 'I', 'I', 'U', 'C', 'R', 'O', 'T', 'D'},
                {'T', 'A', 'H', 'L', 'A', 'F', 'E', 'G', 'N', 'U'},
                {'N', 'C', 'B', 'L', 'R', 'L', 'T', 'R', 'E', 'T'},
                {'E', 'I', 'P', 'A', 'G', 'J', 'H', 'A', 'C', 'I'},
                {'C', 'L', 'S', 'C', 'L', 'J', 'C', 'F', 'O', 'N'},
                {'I', 'P', 'O', 'R', 'X', 'O', 'I', 'O', 'P', 'G'},
                {'P', 'E', 'L', 'E', 'N', 'E', 'R', 'G', 'I', 'A'},
                {'E', 'R', 'O', 'M', 'E', 'R', 'T', 'H', 'H', 'M'}
        };
        String[] solutions = {"ABALO", "ENERGIA", "EPICENTRO", "FALHA", "GRAU", "HIPOCENTRO", "MAGNITUDE", "MAREMOTO",
                "MERCALLI", "PLACAS", "REPLICA", "RICHTER", "SISMOGRAFO", "SISMOS", "SOLO", "TECTONICA",
                "TREMOR", "TSUNAMI"};
        int solutionsCount = solutions.length;

        Scanner scan = new Scanner(System.in);
        int LINE_INDEX = 0;
        final int COLUMN_INDEX = 1;

        /*
        "ABALO" 5 1, 9 5
        "ENERGIA" 10 3, 10 9
        "EPICENTRO" 11 0, 3 0
        "FALHA" 5 5, 5 1
        "GRAU" 7 4, 4 4
        "HIPOCENTRO" 11 8, 2 8
        "MAGNITUDE" 11 9, 3 9
        "MAREMOTO" 3 7, 3 0
        "MERCALLI" 11 3, 4 3
        "PLACAS" 7 2, 2 7
        "REPLICA" 11 1, 5 1
        "RICHTER" 10 6, 4 6
        "SISMOGRAFO" 0 7, 9 7
        "SISMOS" 2 7, 2 2
        "SOLO" 8 2, 11 2
        "TECTONICA" 1 1, 1 9
        "TREMOR" 11 6, 11 1
        "TSUNAMI" 0 8, 0 2
         */

        //Define loop while there's still missing words
        while (solutionsCount > 0) {
            int[] startCoord = new int[2];
            int[] endCoord = new int[2];
            printWordSearchPuzzle(wordSearchPuzzle);
            printRemainingWords(solutions, solutionsCount);

            System.out.println("Insira a primeira coordenada:");
            startCoord[LINE_INDEX] = scan.nextInt();
            startCoord[COLUMN_INDEX] = scan.nextInt();
            System.out.println("Insira a segunda coordenada:");
            endCoord[LINE_INDEX] = scan.nextInt();
            endCoord[COLUMN_INDEX] = scan.nextInt();

            String candidateWord = getWordFromWordSearch(startCoord, endCoord, wordSearchPuzzle);
            System.out.println(candidateWord);

            //if word is valid, then it gets removed from the solutions array.
            //solutionsCount is updated.
            if (validateWord(candidateWord, solutions)) {
                solutions = removeWord(candidateWord, solutions);
                solutionsCount = solutions.length;
                System.out.println("\nPalavra validada!");
            } else {
                System.out.println("\n     !!!!!!!!!!!!\nPalavra errada!\n     ======== \n");
            }
            System.out.print("\n     ======== \n");
        }
    }

    public static void printRemainingWords(String[] solutions, int solutionsCount) {
        System.out.println("Palavras em falta: (" + solutionsCount + ")");
        for (int i = 0; i < solutionsCount; i++) {
            System.out.print(solutions[i] + " ");
        }
        System.out.print("\n");
    }

    public static int[][] getMaskMatrix(char[][] wordSearchMatrix, char searchLetter) {
        if (wordSearchMatrix == null || wordSearchMatrix.length == 0) {
            return null;
        }
        int[][] maskMatrix = new int[wordSearchMatrix.length][wordSearchMatrix[0].length];
        if (Character.isLowerCase(searchLetter)) {
            searchLetter = Character.toUpperCase(searchLetter);
        }
        for (int i = 0; i < wordSearchMatrix.length; i++) {
            for (int j = 0; j < wordSearchMatrix[0].length; j++) {
                if (searchLetter == wordSearchMatrix[i][j]) {
                    maskMatrix[i][j] = 1;
                }

            }
        }
        return maskMatrix;
    }

    public static Integer getDirection(int[] coord1, int[] coord2) {
        if (isArrayNotNullOrEmpty(coord1) || isArrayNotNullOrEmpty(coord2)) {
            return null;
        }
        /*0. esquerda > direita;
          1. direita > esquerda;
          2. cima > baixo;
          3. baixo > cima;
          4. diagonal > descendente esq-dir;
          5. diagonal > ascendente dir-esq;
          6. diagonal > descendente dir-esq;
          7. diagonal > ascendente esq-dir;*/
        int direction = 0;
        if (coord1[1] < coord2[1] && coord1[0] == coord2[0]) {
            //noinspection ConstantConditions
            direction = 0;
        }
        if (coord1[1] > coord2[1] && coord1[0] == coord2[0]) {
            direction = 1;
        }
        if (coord1[0] < coord2[0] && coord1[1] == coord2[1]) {
            direction = 2;
        }
        if (coord1[0] > coord2[0] && coord1[1] == coord2[1]) {
            direction = 3;
        }
        if (coord1[0] < coord2[0] && coord1[1] < coord2[1]) {
            direction = 4;
        }
        if (coord1[0] > coord2[0] && coord1[1] > coord2[1]) {
            direction = 5;
        }
        if (coord1[0] < coord2[0] && coord1[1] > coord2[1]) {
            direction = 6;
        }
        if (coord1[0] > coord2[0] && coord1[1] < coord2[1]) {
            direction = 7;
        }
        return direction;
    }

    public static Integer getSize(int[] coord1, int[] coord2) {
        if (isArrayNotNullOrEmpty(coord1) || isArrayNotNullOrEmpty(coord2)) {
            return null;
        }
        /*0. esquerda > direita;
          1. direita > esquerda;
          2. cima > baixo;
          3. baixo > cima;
          4. diagonal > descendente esq-dir;
          5. diagonal > ascendente dir-esq;
          6. diagonal > descendente dir-esq;
          7. diagonal > ascendente esq-dir;*/
        //Account for at least one letter for single coordinate
        int size = 1;
        int direction = getDirection(coord1, coord2);

        switch (direction) {
            case 0:
                //diagonal desc, esq-dir: mesmas coordenadas em ambos.
            case 4:
                //diagonal asc, esq-dir: primeira parte coordenada interessa: segunda - primeira
            case 7:
            default:
                size += coord2[1] - coord1[1];
                break;
            case 1:
                //diagonal asc, dir-esq: mesmas primeiras coordenadas menos mesmas segundas coordenadas.
            case 5:
                //diagonal desc, dir-esq: primeira parte coordenada interessa: segunda - primeira.
            case 6:
                size += coord1[1] - coord2[1];
                break;
            case 2:
                size += coord2[0] - coord1[0];
                break;
            case 3:
                size += coord1[0] - coord2[0];
                break;
        }
        return size;
    }

    public static String getWordFromWordSearch(int[] coord1, int[] coord2, char[][] wordSearchMatrix) {
        if (isArrayNotNullOrEmpty(coord1) || isArrayNotNullOrEmpty(coord2) || wordSearchMatrix == null || wordSearchMatrix.length == 0) {
            return null;
        }
        int direction = getDirection(coord1, coord2);

        StringBuilder foundWord = new StringBuilder();
        switch (direction) {
            case 0:
            default:
                for (int i = coord1[1]; i <= coord2[1]; i++) {
                    foundWord.append(wordSearchMatrix[coord1[0]][i]);
                }
                break;
            case 1:
                for (int i = coord1[1]; i >= coord2[1]; i--) {
                    foundWord.append(wordSearchMatrix[coord1[0]][i]);
                }
                break;
            case 2:
                for (int i = coord1[0]; i <= coord2[0]; i++) {
                    foundWord.append(wordSearchMatrix[i][coord1[1]]);
                }
                break;
            case 3:
                for (int i = coord1[0]; i >= coord2[0]; i--) {
                    foundWord.append(wordSearchMatrix[i][coord1[1]]);
                }
                break;
            case 4:
                for (int i = coord1[0], j = coord1[1]; i <= coord2[0]; i++, j++) {
                    foundWord.append(wordSearchMatrix[i][j]);
                }
                break;
            case 5:
                for (int i = coord1[0], j = coord1[1]; i >= coord2[0]; i--, j--) {
                    foundWord.append(wordSearchMatrix[i][j]);
                }
                break;
            case 6:
                for (int i = coord1[0], j = coord1[1]; i <= coord2[0]; i++, j--) {
                    foundWord.append(wordSearchMatrix[i][j]);
                }
                break;
            case 7:
                for (int i = coord1[0], j = coord1[1]; i >= coord2[0]; i--, j++) {
                    foundWord.append(wordSearchMatrix[i][j]);
                }
                break;
        }
        return foundWord.toString();
    }

    public static boolean validateWord(String candidateWord, String[] solutions) {
        if (solutions == null || solutions.length == 0) {
            return false;
        }
        boolean isWord = false;
        for (int i = 0; i < solutions.length && !isWord; i++) {
            isWord = candidateWord.equals(solutions[i]);
        }
        return isWord;
    }

    public static String[] removeWord(String foundWord, String[] solutions) {
        if (isArrayNotNullOrEmpty(solutions)) {
            return null;
        }
        int lengthOfTrimmedSolutions = solutions.length - 1;
        String[] trimmedSolutions = new String[lengthOfTrimmedSolutions];
        int counter = 0;
        if (validateWord(foundWord, solutions)) {
            for (String solution : solutions) {
                if (!solution.equals(foundWord)) {
                    trimmedSolutions[counter] = solution;
                    counter++;
                }
            }
        }
        return trimmedSolutions;
    }

    public static void printWordSearchPuzzle(char[][] wordSearchMatrix) {
        if (wordSearchMatrix != null && wordSearchMatrix.length != 0) {
            //noinspection ForLoopReplaceableByForEach
            for (int i = 0; i < wordSearchMatrix.length; i++) {
                for (int j = 0; j < wordSearchMatrix[0].length; j++) {
                    System.out.print(" " + wordSearchMatrix[i][j]);
                    if (j != wordSearchMatrix[0].length - 1) {
                        System.out.print(" |");
                    }
                }
                System.out.print("\n");
            }
        }
    }

    public static int[][] getIntersectionCoordinateFromTwoWords(int[] coord1Word1, int[] coord2Word1, int[] coord1Word2, int[] coord2Word2) {
        if (isArrayNotNullOrEmpty(coord1Word1) || isArrayNotNullOrEmpty(coord2Word1) ||
                isArrayNotNullOrEmpty(coord1Word2) || isArrayNotNullOrEmpty(coord2Word2)) {
            return null;
        }
        int[][] coordsWord1 = getCoordinatesOfWord(coord1Word1, coord2Word1);
        int[][] coordsWord2 = getCoordinatesOfWord(coord1Word2, coord2Word2);

        int sizeWord1 = getSize(coord1Word1, coord2Word1);
        int sizeWord2 = getSize(coord1Word2, coord2Word2);

        int commonSize;
        commonSize = Math.min(sizeWord1, sizeWord2);


        int[][] commonCoordinates = new int[commonSize][2];
        int commonCount = 0;

        boolean isCommon;
        for (int[] ints : coordsWord1) {
            for (int[] coordinates : coordsWord2) {
                isCommon = ints[0] == coordinates[0] && ints[1] == coordinates[1];
                if (isCommon) {
                    commonCoordinates[commonCount][0] = coordinates[0];
                    commonCoordinates[commonCount][1] = coordinates[1];
                    commonCount++;
                }
            }
        }
        return truncateMatrixWithGivenArrayLength(commonCoordinates, commonCount);
    }

    public static int[][] truncateMatrixWithGivenArrayLength(int[][] matrix, int arrayLength) {
        if (matrix == null || matrix.length == 0) {
            return null;
        }
        int[][] matrixTruncated = new int[arrayLength][matrix[0].length];
        for (int i = 0; i < arrayLength; i++) {
            //noinspection ManualArrayCopy
            for (int j = 0; j < matrix[0].length; j++) {
                matrixTruncated[i][j] = matrix[i][j];
            }
        }
        return matrixTruncated;
    }

    public static int[][] getCoordinatesOfWord(int[] coordStart, int[] coordEnd) {
        if (isArrayNotNullOrEmpty(coordStart) || isArrayNotNullOrEmpty(coordEnd)) {
            return null;
        }
        int direction = getDirection(coordStart, coordEnd);
        //sizes
        int size = getSize(coordStart, coordEnd);

        //create an array with the right length
        int[][] coordinates = new int[size][2];

        //creates a disposable variable to iterate.
        int j = size - 1;
        switch (direction) {
            default:
            case 0:
                for (int i = coordEnd[1]; j >= 0; i--, j--) {
                    coordinates[j][1] = i;
                    coordinates[j][0] = coordEnd[0];
                }
                break;
            case 1:
                for (int i = coordStart[1]; j >= 0; i--, j--) {
                    coordinates[j][1] = i;
                    coordinates[j][0] = coordEnd[0];
                }
                break;
            case 2:
                for (int i = coordEnd[0]; j >= 0; i--, j--) {
                    coordinates[j][0] = i;
                    coordinates[j][1] = coordEnd[1];
                }
                break;
            case 3:
                for (int i = coordStart[0]; j >= 0; i--, j--) {
                    coordinates[j][0] = i;
                    coordinates[j][1] = coordEnd[1];
                }
                break;
            case 4:
                for (int i = 0; j >= 0; i++, j--) {
                    coordinates[j][0] = coordEnd[0] - i;
                    coordinates[j][1] = coordEnd[1] - i;
                }
                break;
            case 5:
                for (int i = 0; j >= 0; i++, j--) {
                    coordinates[j][0] = coordStart[0] - i;
                    coordinates[j][1] = coordStart[1] - i;
                }
                break;
            case 6:
                for (int i = 0; j >= 0; i++, j--) {
                    coordinates[j][0] = coordEnd[0] - i;
                    coordinates[j][1] = coordEnd[1] + i;
                }
                break;
            case 7:
                for (int i = 0; j >= 0; i++, j--) {
                    coordinates[j][0] = coordStart[0] - i;
                    coordinates[j][1] = coordStart[1] + i;
                }
                break;
        }
        return coordinates;
    }

    public static boolean isArrayNotNullOrEmpty(int[] array) {
        return array == null || array.length == 0;
    }

    public static boolean isArrayNotNullOrEmpty(String[] array) {
        return array == null || array.length == 0;
    }
}
