public class ExercicioTreze {
    public static void main(String[] args) {

    }

    public static boolean isMatrixSquare(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return false;
        }
        boolean isSquare = true;
        for (int i = 0; i < matrix.length && isSquare; i++) {
            isSquare = matrix[i].length == matrix.length;
        }
        return isSquare;
    }
}
