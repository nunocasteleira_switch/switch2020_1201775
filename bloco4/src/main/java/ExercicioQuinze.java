public class ExercicioQuinze {
    public static void main(String[] args) {

    }

    public static Integer smallestValueInMatrix(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return null;
        }
        int smallest = matrix[0][0];
        for (int[] array : matrix) {
            for (int value : array) {
                if (value < smallest) {
                    smallest = value;
                }
            }
        }
        return smallest;
    }

    public static Integer largestValueInMatrix(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return null;
        }
        int largest = matrix[0][0];
        for (int[] array : matrix) {
            for (int value : array) {
                if (value > largest) {
                    largest = value;
                }
            }
        }
        return largest;
    }

    public static Integer meanValueInMatrix(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return null;
        }
        int count = 0;
        int sum = 0;
        for (int[] array : matrix) {
            for (int value : array) {
                sum += value;
                count++;
            }
        }
        return sum / count;
    }

    public static Long productOfValuesInMatrix(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return null;
        }
        long product = 1;
        for (int[] array : matrix) {
            for (int value : array) {
                product *= value;
            }
        }
        return product;
    }

    public static Integer getMatrixSize(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return null;
        }
        int size = 0;
        for (int[] array : matrix) {
            //noinspection unused
            for (int value : array) {
                size++;
            }
        }
        return size;
    }

    public static int[] uniqueValuesInMatrix(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return null;
        }
        int size = getMatrixSize(matrix);
        int[] uniqueArray = new int[size];
        //o primeiro é de borla
        uniqueArray[0] = matrix[0][0];

        //iniciar o counter fora para poder aceder ao seu valor
        int counter = 1; //com o número um porque o primeiro é de borla
        //inicio um booleano que vai alterar o seu valor quando encontrar um número igual
        boolean isUnique = true;
        for (int[] array : matrix) {
            for (int value : array) {
                //loop interior até ao valor de counter do que pretendo colocar na array
                //Tem de ser counter para evitar encontrar zeros de números até então não verificados
                for (int j = 0; j < counter && isUnique; j++) {
                    //se o valor do segundo loop for igual ao valor do primeiro, o número não é único
                    isUnique = uniqueArray[j] != value;
                }
                //se no final da verificação, o número se mantiver único, então adiciono-o à nova array
                //preciso do counter para isso
                if (isUnique) {
                    uniqueArray[counter] = value;
                    counter++;
                }
                isUnique = true;
            }
        }
        //truncar os zeros
        return ExercicioQuatro.truncateArrayWithGivenLength(uniqueArray, counter);
    }

    public static int[] primeValuesInMatrix(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return null;
        }
        int size = getMatrixSize(matrix);
        int[] newArray = new int[size];
        int counter = 0;
        for (int[] array : matrix) {
            boolean isPrime;
            for (int value : array) {
                isPrime = ExercicioDez.isNumberPrime(value);
                if (isPrime) {
                    newArray[counter] = value;
                    counter++;
                }
            }
        }
        return ExercicioQuatro.truncateArrayWithGivenLength(newArray, counter);
    }

    public static int[] getMainDiagonalInMatrix(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return null;
        }
        int[] diagonal;
        if (ExercicioTreze.isMatrixSquare(matrix) || ExercicioCatorze.isMatrixRect(matrix)) {
            //Se for rectângulo, a diagonal vai ser sempre o menor valor, entre length e o comum
            int size = Math.min(ExercicioDoze.getCommonLengthNumber(matrix), matrix.length);
            diagonal = new int[size];
            for (int i = 0; i < size; i++) {
                diagonal[i] = matrix[i][i];
            }
        } else {
            diagonal = new int[1];
            diagonal[0] = -1;
        }
        return diagonal;
    }

    public static int[] getAntidiagonalInMatrix(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return null;
        }
        int[] diagonal;
        if (ExercicioTreze.isMatrixSquare(matrix) || ExercicioCatorze.isMatrixRect(matrix)) {
            int size = Math.min(ExercicioDoze.getCommonLengthNumber(matrix), matrix.length);
            //ir para o último número. Como de certeza é quadrada ou rect, pode-se verificar na primeira linha
            int edge = matrix[0].length - 1;
            diagonal = new int[size];
            for (int i = 0; i < size; i++, edge--) {
                diagonal[i] = matrix[i][edge];
            }
        } else {
            diagonal = new int[1];
            diagonal[0] = -1;
        }
        return diagonal;
    }

    public static boolean isIdentityMatrix(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return false;
        }
        boolean isIdentity = true;
        if (ExercicioTreze.isMatrixSquare(matrix)) {
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[i].length && isIdentity; j++) {
                    if (i != j) {
                        isIdentity = matrix[i][j] == 0;
                    } else {
                        isIdentity = matrix[i][j] == 1;
                    }
                }
            }
        } else {
            isIdentity = false;
        }
        return isIdentity;
    }

    public static int[][] trimMatrixByLineAndColumnIndex(int[][] matrix, int line, int column) {
        if (matrix == null || matrix.length == 0) {
            return null;
        }
        int length = matrix.length - 1;//one smaller than entered matrix
        int[][] trimmed = new int[length][length];
        for (int i = 0, k = 0; i < 3; i++) {
            if (i != line) {
                for (int j = 0, l = 0; j < 3; j++) {
                    if (j != column) {
                        trimmed[k][l] = matrix[i][j];
                        l++;
                    }
                }
                k++;
            }
        }
        return trimmed;
    }

    public static long multiplyDiagonalsOfMatrix(int[][] matrix) {
        int[] diagonalArray = getMainDiagonalInMatrix(matrix);
        return ExercicioDez.productOfValuesInArray(diagonalArray);
    }

    public static long multiplyAntidiagonalsOfMatrix(int[][] matrix) {
        int[] antidiagonalArray = getAntidiagonalInMatrix(matrix);
        return ExercicioDez.productOfValuesInArray(antidiagonalArray);
    }

    public static int[][] getMatrixOfMinors(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return null;
        }
        int length = matrix.length;
        int[][] matrixOfMinors = new int[length][length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                matrixOfMinors[i][j] =
                        (int) (multiplyDiagonalsOfMatrix(trimMatrixByLineAndColumnIndex(matrix, i, j)) -
                                                        multiplyAntidiagonalsOfMatrix(trimMatrixByLineAndColumnIndex(matrix, i, j)));
            }
        }
        return matrixOfMinors;
    }

    public static int[][] getMatrixOfCofactors(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return null;
        }
        int length = matrix.length;
        int[][] matrixOfCofactors = new int[length][length];
        boolean isAlternate = false;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (isAlternate) {
                    matrixOfCofactors[i][j] = matrix[i][j] * -1;
                    isAlternate = false;
                } else {
                    matrixOfCofactors[i][j] = matrix[i][j];
                    isAlternate = true;
                }
            }
        }
        return matrixOfCofactors;
    }

/*    public static int[][] getAdjugateMatrix(int[][] matrix) {
        int length = matrix.length;
        int[][] adjugateMatrix = new int[length][length];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                adjugateMatrix[i][j] = matrix[j][i];
            }
        }
        return adjugateMatrix;
    }
    ESTE MÉTODO FOI SUBSTITUÍDO PELO getTransposedMatrix POIS FAZ O MESMO,
    PARA MATRIZES QUADRADAS E RECTANGULARES.
    */

    public static int getDeterminantOfMatrix(int[][] matrix) {
        int determinant = 0;
        int[][] matrixOfCofactor = getMatrixOfMinors(getMatrixOfCofactors(matrix));

        for (int i = 0; i < matrix[0].length; i++) {
            if (i % 2 == 0) {
                determinant += matrix[0][i] * matrixOfCofactor[0][i];
            } else {
                determinant -= matrix[0][i] * matrixOfCofactor[0][i];
            }
        }
        return determinant;
    }

    public static float[][] multiplyAllValuesInMatrixByConstant(int[][] matrix, float constant) {
        //este método não corre com double
        if (matrix == null || matrix.length == 0) {
            return null;
        }
        int length = matrix.length;
        float[][] multipliedMatrix = new float[length][length];
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                multipliedMatrix[i][j] = matrix[i][j] * constant;
            }
        }
        return multipliedMatrix;
    }

    public static float[][] getInverseOfMatrix(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return null;
        }
        float[][] inverseOfMatrix;
        if (ExercicioTreze.isMatrixSquare(matrix)) {
            int[][] matrixOfMinors = getMatrixOfMinors(matrix);
            int[][] matrixOfCofactors = getMatrixOfCofactors(matrixOfMinors);
            int[][] adjugateMatrix = getTransposedMatrix(matrixOfCofactors);
            int determinant = getDeterminantOfMatrix(matrix);
            float multiplyFactor = (float) 1 / determinant;
            assert adjugateMatrix != null;
            inverseOfMatrix = multiplyAllValuesInMatrixByConstant(adjugateMatrix, multiplyFactor);
        } else {
            return null;
        }
        return inverseOfMatrix;
    }

    public static int[][] getTransposedMatrix(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return null;
        }
        int[][] transposedMatrix;
        if (ExercicioTreze.isMatrixSquare(matrix) || ExercicioCatorze.isMatrixRect(matrix)) {
            int height = matrix.length;
            int width = matrix[0].length;
            if (height > width) {
                transposedMatrix = new int[width][height];
                for (int i = 0; i < width; i++) {
                    for (int j = 0; j < height; j++) {
                        transposedMatrix[j][i] = matrix[i][j];
                    }
                }
            } else {
                transposedMatrix = new int[width][height];
                for (int i = 0; i < height; i++) {
                    for (int j = 0; j < width; j++) {
                        transposedMatrix[j][i] = matrix[i][j];
                    }
                }
            }
        } else {
            return null;
        }
        return transposedMatrix;
    }
}

