public class ExercicioNove {
    public static void main(String[] args) {
        //Tomando por premissa que pretendemos testar com arrays
    }

    public static boolean isPalindrome (int number) {
        int[] numberInArray = ExercicioDois.separateDigitsIntoArray(number);
        boolean isPalindrome = false;
        for (int i = 0, j = numberInArray.length - 1; i < j; i++, j--) {
            //O IDE sugere a alteração disto para a linha abaixo
            //if (numberInArray[i] == numberInArray[j]) {
            //    isPalindrome = true;
            //} else {
            //    isPalindrome = false;
            //}
            isPalindrome = numberInArray[i] == numberInArray[j];
        }
        return isPalindrome;
    }
}
