public class ExercicioDois {
    public static void main(String[] args) {

    }

    public static int[] separateDigitsIntoArray(int number) {
        int length = ExercicioUm.countDigits(number);
        int[] array = new int[length];
        for (int i = length - 1;  number > 0; i--) {
            array[i] = number % 10;
            number /= 10;
        }
        return array;
    }
}
