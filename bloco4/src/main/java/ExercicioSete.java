public class ExercicioSete {
    public static void main(String[] args) {

    }

    public static int[] getMultiplesOnGivenInterval(int first, int last, int multiplex) {
        /*if (first > last) {
            int temp = first;
            first = last;
            last = temp;
        }
        int size = last - first;*/
        int[] limits = getCorrectLimitOrderAndReturnSize(first, last);
        int size = limits[1] - limits[0];
        if (limits[1] == limits[0]) {
            size = 1;
        }
        int[] multiples = new int[size];
        int count = 0;
        for (int i = limits[0]; i <= limits[1]; i++) {
            if (i % multiplex == 0) {
                multiples[count] = i;
                count++;
            }
        }
        return ExercicioQuatro.truncateArrayWithGivenLength(multiples, count);
    }

    public static int[] getCorrectLimitOrderAndReturnSize(int first, int last) {
        int[] limitArray = new int[2];
        limitArray[0] = first;
        limitArray[1] = last;
        if (first > last) {
            limitArray[0] = last;
            limitArray[1] = first;
        }
        return limitArray;
    }
}
