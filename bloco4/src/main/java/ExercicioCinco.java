public class ExercicioCinco {
    public static void main(String[] args) {

    }

    public static int sumEvenDigitsOnNumber (int number) {
        int[] evenDigits = ExercicioDois.separateDigitsIntoArray(number);
        int[] evenDigitsArray = ExercicioQuatro.evenDigitsInArray(evenDigits);
        return ExercicioTres.sumElementsInArray(evenDigitsArray);
    }


    public static int sumOddDigitsOnNumber (int number) {
        int[] oddDigits = ExercicioDois.separateDigitsIntoArray(number);
        int[] oddDigitsArray = ExercicioQuatro.oddDigitsInArray(oddDigits);
        return ExercicioTres.sumElementsInArray(oddDigitsArray);
    }
}
