public class ExercicioOito {
    public static void main(String[] args) {
        //Alterado para poder aceitar mais que dois multiplicadores
    }

    public static int[] getMultiplesOfMultipliersArray(int first, int last, int[] multiplex) {
        /*//poderei tornar esta verificação num método?
        //p.ex. um void que altere os valores no meu método?
        if (first > last) {
            int temp = first;
            first = last;
            last = temp;
        }*/
        int[] limits = ExercicioSete.getCorrectLimitOrderAndReturnSize(first, last);
        int size = limits[1] - limits[0];
        int[] multiples = new int[size];
        int count = 0;
        for (int i = limits[0]; i <= limits[1]; i++) {
        boolean isMultiple = true;
            for (int j = 0; j < multiplex.length && isMultiple; j++) {
                isMultiple = i % multiplex[j] == 0;
            }
            if (isMultiple) {
                multiples[count] = i;
                count++;
            }
        }
        return ExercicioQuatro.truncateArrayWithGivenLength(multiples, count);
    }
}
