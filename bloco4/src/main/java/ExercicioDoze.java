public class ExercicioDoze {
    public static void main(String[] args) {

    }

    public static Integer getCommonLengthNumber(int[][] matrix) {
        if (matrix == null) {
            return null;
        }
        if (matrix.length == 0) {
            return null;
        }
        int rowLength = 0;
        for (int i = 0; i < matrix.length; i++) {
            int rowLengthNew;
            if (i == 0) {
                rowLength = matrix[i].length;
            } else {
                rowLengthNew = matrix[i].length;
                if (rowLength != rowLengthNew) {
                    rowLength = -1;
                }
            }
        }
        return rowLength;
    }
}
