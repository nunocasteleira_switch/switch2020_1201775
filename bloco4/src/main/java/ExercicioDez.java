public class ExercicioDez {
    public static void main(String[] args) {

    }

    public static Integer smallerValueInArray(int[] array) {
        if (array == null || array.length == 0) {
            return null;
        }
        int smaller = array[0];
        for (int value : array) {
            if (value < smaller) {
                smaller = value;
            }
        }
        return smaller;
    }

    public static Integer largestValueInArray(int[] array) {
        if (array == null || array.length == 0) {
            return null;
        }
        int largest = array[0];
        for (int value : array) {
            if (value > largest) {
                largest = value;
            }
        }
        return largest;
    }

    public static Double meanValueInArray(int[] array) {
        if (array == null || array.length == 0) {
            return null;
        }
        int sum = 0;
        for (int value : array) {
            sum += value;
        }
        return (double) sum / array.length;
    }

    public static Long productOfValuesInArray(int[] array) {
        if (array == null || array.length == 0) {
            return null;
        }
        long product = 1L;
        for (int value : array) {
            product *= value;
        }
        return product;
    }

    /*
    1 2 3 4 3 5
    1
    1 2
    1 2 3
    1 2 3 4
    1 2 3
    1 2 3 4 5
     */

    public static int[] uniqueValuesInArray(int[] array) {
        //primeiro passo é ordenar a array
        //isto é legítimo usar?
        //Arrays.sort(array);
        //Colocando a condição no loop, não preciso de ordenar!
        if (array == null || array.length == 0) {
            return null;
        }
        int[] uniqueArray = new int[array.length];
        //o primeiro é de borla
        uniqueArray[0] = array[0];
        //iniciar o counter fora para poder aceder ao seu valor
        int counter = 0;
        //inicio um booleano que vai alterar o seu valor quando encontrar um número igual
        boolean isUnique = true;
        //loop de fora para os valores da array, que vai percorrer todos
        for (int i = 0; i < array.length; i++) {
            //loop interior até ao valor anterior do que pretendo colocar na array
            for (int j = 0; j < i && isUnique; j++) {
                //se o valor do segundo loop for igual ao valor do primeiro, o número não é único
                isUnique = uniqueArray[j] != array[i];
                /*
                if (array[j] == array[i]){
                    isUnique = false;
                } else {
                    isUnique = true;
                }
                 */
            }
            //se no final da verificação, o número se mantiver único, então adiciono-o à nova array
            //preciso do counter para isso
            if (isUnique) {
                uniqueArray[counter] = array[i];
                counter++;
            }
            isUnique = true;
        }
        //truncar os zeros
        return ExercicioQuatro.truncateArrayWithGivenLength(uniqueArray, counter);
    }

    public static int[] invertArray(int[] array) {
        if (array == null || array.length == 0) {
            return null;
        }
        int[] newArray = new int[array.length];
        for (int i = array.length - 1, j = 0; i >= 0; i--, j++) {
            newArray[j] = array[i];
        }
        return newArray;
    }

    public static boolean isNumberPrime(int number) {
        boolean isPrime;
        int count = 0;
        for (int i = 2; i <= (int) Math.sqrt(number); i++) {
            if (number % i == 0) {
                count++;
            }
        }
        isPrime = count <= 0;
        return isPrime;
    }

    public static int[] primeNumbersInArray(int[] array) {
        if (array == null || array.length == 0) {
            return null;
        }
        int[] newArray = new int[array.length];
        int counter = 0;
        boolean isPrime;
        for (int value : array) {
            isPrime = isNumberPrime(value);
            if (isPrime) {
                newArray[counter] = value;
                counter++;
            }
        }
        return ExercicioQuatro.truncateArrayWithGivenLength(newArray, counter);
    }
}
