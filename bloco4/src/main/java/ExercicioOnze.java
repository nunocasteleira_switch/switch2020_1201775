public class ExercicioOnze {
    public static void main(String[] args) {

    }

    public static Long getDotProductOfTwoArrays(int[] first, int[] second) {
        if (first == null || second == null) {
            return null;
        }
        long product = 0;
        if (first.length == second.length && first.length > 0) {
            for (int i = 0; i < first.length; i++) {
                product += first[i] * second[i];
            }
        } else {
            return null;
        }
        return product;
    }
}
