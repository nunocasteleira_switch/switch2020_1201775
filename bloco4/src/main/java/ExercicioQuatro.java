public class ExercicioQuatro {
    public static void main(String[] args) {
        /*
        Tem de haver alguma alternativa para poder truncar a array
        ou para poder reduzir os loops que se realizam.

        Há duas alternativas: contar os pares e depois criar uma array
        com essas dimensão. Dois loops, contar e atribuir valores.

        Ou

        Como eu fiz, truncar com o count de dígitos, que para nossa conveniência
        pode ser mantido para fora do loop.
         */

    }

    public static int[] evenDigitsInArray(int[] numbers) {
        int[] evenArray = new int[numbers.length];
        int count = 0;
        for (int number : numbers) {
            if (number % 2 == 0) {
                evenArray[count] = number;
                count++;
            }
        }
        return truncateArrayWithGivenLength(evenArray, count);
    }

    public static int[] oddDigitsInArray(int[] numbers) {
        int[] oddArray = new int[numbers.length];
        int count = 0;
        for (int number : numbers) {
            if (number % 2 != 0) {
                oddArray[count] = number;
                count++;
            }
        }
        return truncateArrayWithGivenLength(oddArray, count);
    }

    public static int[] truncateArrayWithGivenLength(int[] initialArray, int count) {
        int[] arrayTruncated = new int[count];
        //noinspection ManualArrayCopy
        for (int i = 0; i < count; i++) {
            arrayTruncated[i] = initialArray[i];
        }
        // sugestão do IDE: (substitui as três (quatro) linhas acima.
        // if (count >= 0) System.arraycopy(initialArray, 0, arrayTruncated, 0, count);
        return arrayTruncated;
    }
}
