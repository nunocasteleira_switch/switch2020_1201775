public class ExercicioDezasseis {
    public static void main(String[] args) {
        //O exercício dezasseis solicita que se faça o que já resolvi num método do exercício quinze.
        //O Método getDeterminantOfMatrix retorna o determinante de uma matriz
        // passada como parâmetro.
        int[][] matrix = {{3, 0, 2}, {2, 0, -2}, {0, 1, 1}};
        ExercicioQuinze.getDeterminantOfMatrix(matrix);
    }
}
