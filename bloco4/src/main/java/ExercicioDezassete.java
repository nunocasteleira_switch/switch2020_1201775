public class ExercicioDezassete {
    public static void main(String[] args) {
        //a alínea a) já está resolvida no exercício quinze.
        //O método multiplyAllValuesInMatrixByConstant multiplica todos os valores de uma matriz por uma constante.
        //Esta função retorna valores float porque devido à divisão por uma constante para obter a inversa é necessário
        // essa conversão de tipo de dados.
    }

    public static int[][] sumMatrixes(int[][] matrix1, int[][] matrix2) {
        if (matrix1 == null || matrix1.length == 0 || matrix2 == null || matrix2.length == 0) {
            return null;
        }
        int[][] summedMatrix;
        if (matrix1.length == matrix2.length && matrix1[0].length == matrix2[0].length) {
            summedMatrix = new int[matrix1.length][matrix1[0].length];
            for (int i = 0; i < summedMatrix.length; i++) {
                for (int j = 0; j < summedMatrix[0].length; j++) {
                    summedMatrix[i][j] = matrix1[i][j] + matrix2[i][j];
                }
            }
        } else {
            return null;
        }
        return summedMatrix;
    }

    public static int[][] matrixMultiplication(int[][] matrix1, int[][] matrix2) {
        //In order for matrix multiplication to be defined,
        // the number of columns in the first matrix must be
        // equal to the number of rows in the second matrix.
        if (matrix1 == null || matrix1.length == 0 || matrix2 == null || matrix2.length == 0) {
            return null;
        }
        int[][] multipliedMatrix;
        if (matrix1[0].length == matrix2.length) {
            multipliedMatrix = new int[matrix1.length][matrix2[0].length];
            for (int i = 0; i < multipliedMatrix.length; i++) {
                for (int j = 0; j < multipliedMatrix[0].length; j++) {
                    int sum = 0;
                    for (int k = 0; k < matrix1[0].length; k++) {
                        sum += matrix1[i][k] * matrix2[k][j];
                    }
                    multipliedMatrix[i][j] = sum;
                }
            }
        } else {
            return null;
        }
        return multipliedMatrix;
    }
}

