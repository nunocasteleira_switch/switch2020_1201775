import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class ExercicioDezasseteTest {

    @Test
    void sumMatrixes() {
        int[][] matrix1 = {{1, 2}, {3, 4}};
        int[][] matrix2 = {{-1, 3}, {4, 2}};
        int[][] expected = {{0, 5}, {7, 6}};
        int[][] result = ExercicioDezassete.sumMatrixes(matrix1, matrix2);
        assertArrayEquals(expected, result);
    }

    @Test
    void matrixMultiplication() {
        int[][] matrix1 = {{1, 2}, {3, 4}};
        int[][] matrix2 = {{-1, 3}, {4, 2}};
        int[][] expected = {{7, 7}, {13, 17}};
        int[][] result = ExercicioDezassete.matrixMultiplication(matrix1, matrix2);
        assertArrayEquals(expected, result);
    }

    @Test
    void matrixThreeByThreeMultiplication() {
        int[][] matrix1 = {{2, 3}, {0, 1}, {-1, 4}};
        int[][] matrix2 = {{1, 2, 3}, {-2, 0, 4}};
        int[][] expected = {{-4, 4, 18}, {-2, 0, 4}, {-9, -2, 13}};
        int[][] result = ExercicioDezassete.matrixMultiplication(matrix1, matrix2);
        assertArrayEquals(expected, result);
    }

    @Test
    void matrixOtherThreeByThreeMultiplication() {
        int[][] matrix1 = {{1, 2, 3}, {-2, 0, 4}};
        int[][] matrix2 = {{2, 3}, {0, 1}, {-1, 4}};
        int[][] expected = {{-1, 17}, {-8, 10}};
        int[][] result = ExercicioDezassete.matrixMultiplication(matrix1, matrix2);
        assertArrayEquals(expected, result);
    }
}