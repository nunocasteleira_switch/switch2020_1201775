import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioTresTest {

    @Test
    void sumElementsInArray() {
        int[] numbers = {3, 6, 7, 8, 1};
        int expected = 25;
        int result = ExercicioTres.sumElementsInArray(numbers);
        assertEquals(expected, result);
    }

    @Test
    public void sumNegValues() {
        int[] array = {50, -1, -1, 0, -3, -2, -10};
        int expected = 33;
        int result = ExercicioTres.sumElementsInArray(array);
        assertEquals(expected, result);
    }

    @Test
    public void sumEmptyArray() {
        int[] array = {};
        int expected = 0;
        int result = ExercicioTres.sumElementsInArray(array);
        assertEquals(expected, result);
    }

    @Test
    public void sumNullArray() {
        int[] array = null;
        int expected = 0;
        int result = ExercicioTres.sumElementsInArray(array);
        assertEquals(expected, result);
    }
}