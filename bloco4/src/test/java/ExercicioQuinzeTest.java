import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioQuinzeTest {

    @Test
    void smallestValueInMatrix() {
        int[][] matrix = {{3, 5, 67}, {34, 5, 256}, {2, 4}};
        int expected = 2;
        int result = ExercicioQuinze.smallestValueInMatrix(matrix);
        assertEquals(expected, result);
    }

    @Test
    void largestValueInMatrix() {
        int[][] matrix = {{3, 5, 67}, {34, 5, 256}, {2, 4}};
        int expected = 256;
        int result = ExercicioQuinze.largestValueInMatrix(matrix);
        assertEquals(expected, result);
    }

    @Test
    void meanValueInMatrix() {
        int[][] matrix = {{3, 5, 67}, {34, 5, 256}, {2, 4}};
        int expected = 47;
        int result = ExercicioQuinze.meanValueInMatrix(matrix);
        assertEquals(expected, result);
    }

    @Test
    void productOfValuesInMatrix() {
        int[][] matrix = {{3, 5, 67}, {34, 5, 256}, {2, 4}};
        long expected = 349900800;
        long result = ExercicioQuinze.productOfValuesInMatrix(matrix);
        assertEquals(expected, result);
    }

    @Test
    void uniqueValuesInMatrix() {
        int[][] matrix = {{3, 5, 67}, {34, 5, 256}, {2, 4}};
        int[] expected = {3, 5, 67, 34, 256, 2, 4};
        int[] result = ExercicioQuinze.uniqueValuesInMatrix(matrix);
        assertArrayEquals(expected, result);
    }

    @Test
    void anotherUniqueValuesInMatrix() {
        int[][] matrix = {{1, 1, 2}, {1, 1, 1}, {2, 3}};
        int[] expected = {1, 2, 3};
        int[] result = ExercicioQuinze.uniqueValuesInMatrix(matrix);
        assertArrayEquals(expected, result);
    }

    @Test
    void anotherUniqueValuesInMatrixWithZero() {
        int[][] matrix = {{1, 0, 2}, {1, 1, 1}, {2, 3}};
        int[] expected = {1, 0, 2, 3};
        int[] result = ExercicioQuinze.uniqueValuesInMatrix(matrix);
        assertArrayEquals(expected, result);
    }

    @Test
    public void uniqueValuesInMatrixRaquel() {
        int[][] matriz ={{1,2,5},{9,0,1},{0,2,3}};
        int[] expected = {1, 2, 5, 9, 0, 3};
        int[] result = ExercicioQuinze.uniqueValuesInMatrix(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    void primeValuesInMatrix() {
        //2, 3, 5, 7, 11, 13, 17, 19, 23, 29
        int[][] matrix = {{2, 4, 3}, {6, 8, 9}, {10, 12}};
        int[] expected = {2, 3};
        int[] result = ExercicioQuinze.primeValuesInMatrix(matrix);
        assertArrayEquals(expected, result);
    }

    @Test
    void getMainDiagonalInSquareMatrix() {
        int[][] matrix = {{2, 4, 3}, {6, 8, 9}, {10, 12, 11}};
        int[] expected = {2, 8, 11};
        int[] result = ExercicioQuinze.getMainDiagonalInMatrix(matrix);
        assertArrayEquals(expected, result);
    }

    @Test
    void getMainDiagonalInVerticalRectMatrix() {
        int[][] matrix = {{2, 4}, {6, 8}, {10, 12}, {1, 3}};
        int[] expected = {2, 8};
        int[] result = ExercicioQuinze.getMainDiagonalInMatrix(matrix);
        assertArrayEquals(expected, result);
    }

    @Test
    void getMainDiagonalInHorizRectMatrix() {
        int[][] matrix = {{2, 4, 6, 8}, {10, 12, 1, 3}};
        int[] expected = {2, 12};
        int[] result = ExercicioQuinze.getMainDiagonalInMatrix(matrix);
        assertArrayEquals(expected, result);
    }

    @Test
    void getNotADiagonalInNotARectOrSquareMatrix() {
        int[][] matrix = {{2, 4, 6, 8}, {10}, {12, 1, 3}};
        int[] expected = {-1};
        int[] result = ExercicioQuinze.getMainDiagonalInMatrix(matrix);
        assertArrayEquals(expected, result);
    }

    @Test
    void getAntidiagonalInHorizRectMatrix() {
        int[][] matrix = {{2, 4, 6, 8}, {10, 12, 1, 3}};
        int[] expected = {8, 1};
        int[] result = ExercicioQuinze.getAntidiagonalInMatrix(matrix);
        assertArrayEquals(expected, result);
    }

    @Test
    void getAntidiagonalInSquareMatrix() {
        int[][] matrix = {{2, 4, 3}, {6, 8, 9}, {10, 12, 11}};
        int[] expected = {3, 8, 10};
        int[] result = ExercicioQuinze.getAntidiagonalInMatrix(matrix);
        assertArrayEquals(expected, result);
    }

    @Test
    void getAntidiagonalInVerticalRectMatrix() {
        int[][] matrix = {{2, 4}, {6, 8}, {10, 12}, {1, 3}};
        int[] expected = {4, 6};
        int[] result = ExercicioQuinze.getAntidiagonalInMatrix(matrix);
        assertArrayEquals(expected, result);
    }

    @Test
    void getNotAnAntidiagonalInNotARectOrSquareMatrix() {
        int[][] matrix = {{2, 4, 6, 8}, {10}, {12, 1, 3}};
        int[] expected = {-1};
        int[] result = ExercicioQuinze.getAntidiagonalInMatrix(matrix);
        assertArrayEquals(expected, result);
    }

    @Test
    void isIdentityMatrix() {
        int[][] matrix = {{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}};
        boolean result = ExercicioQuinze.isIdentityMatrix(matrix);
        assertTrue(result);
    }

    @Test
    void isNotIdentityMatrix() {
        int[][] matrix = {{1, 4, 6, 8}, {1, 4, 6, 8}, {6, 4, 1, 8}, {6, 4, 8, 1}};
        boolean result = ExercicioQuinze.isIdentityMatrix(matrix);
        assertFalse(result);
    }

    @Test
    void trimThreeByThreeMatrix() {
        int[][] matrix = {{3, 0, 2}, {2, 0, -2}, {0, 1, 1}};
        int line = 0;
        int column = 0;
        int[][] expected = {{0,-2}, {1,1}};
        int[][] result = ExercicioQuinze.trimMatrixByLineAndColumnIndex(matrix, line, column);
        assertArrayEquals(expected, result);
    }

    @Test
    void trimAnotherThreeByThreeMatrix() {
        int[][] matrix = {{3, 0, 2}, {2, 0, -2}, {0, 1, 1}};
        int line = 1;
        int column = 1;
        int[][] expected = {{3,2}, {0,1}};
        int[][] result = ExercicioQuinze.trimMatrixByLineAndColumnIndex(matrix, line, column);
        assertArrayEquals(expected, result);
    }

    @Test
    void multiplyDiagonalsOfMatrix() {
        int[][] matrix = {{3,2}, {0,1}};
        long expected = 3;
        long result = ExercicioQuinze.multiplyDiagonalsOfMatrix(matrix);
        assertEquals(expected, result);
    }

    @Test
    void multiplyAntidiagonalsOfMatrix() {
        int[][] matrix = {{3,2}, {0,1}};
        long expected = 0;
        long result = ExercicioQuinze.multiplyAntidiagonalsOfMatrix(matrix);
        assertEquals(expected, result);
    }

    @Test
    void getMatrixOfMinors() {
        int[][] matrix = {{3, 0, 2}, {2, 0, -2}, {0, 1, 1}};
        int[][] expected = {{2, 2, 2}, {-2, 3, 3}, {0, -10, 0}};
        int[][] result = ExercicioQuinze.getMatrixOfMinors(matrix);
        assertArrayEquals(expected, result);
    }

    @Test
    void getMatrixOfCofactors() {
        int[][] matrix = {{2, 2, 2}, {-2, 3, 3}, {0, -10, 0}};
        int[][] expected = {{2, -2, 2}, {2, 3, -3}, {0, 10, 0}};
        int[][] result = ExercicioQuinze.getMatrixOfCofactors(matrix);
        assertArrayEquals(expected, result);
    }

    @Test
    void getTransposedMatrix() {
        int[][] matrix = {{2, -2, 2}, {2, 3, -3}, {0, 10, 0}};
        int[][] expected = {{2, 2, 0}, {-2, 3, 10}, {2, -3, 0}};
        int[][] result = ExercicioQuinze.getTransposedMatrix(matrix);
        assertArrayEquals(expected, result);
    }

    @Test
    void getDeterminantOfMatrix() {
        int[][] matrix = {{3, 0, 2}, {2, 0, -2}, {0, 1, 1}};
        int expected = 10;
        int result = ExercicioQuinze.getDeterminantOfMatrix(matrix);
        assertEquals(expected, result);
    }

    @Test
    void multiplyAllValuesInMatrixByConstant() {
        //este teste não corre com double
        int[][] matrix = {{3, 0, 2}, {2, 0, -2}, {0, 1, 1}};
        float constant = 0.1f;
        float[][] expected = {{0.3f, 0.0f, 0.2f}, {0.2f, 0.0f, -0.2f}, {0, 0.1f, 0.1f}};
        float[][] result = ExercicioQuinze.multiplyAllValuesInMatrixByConstant(matrix, constant);
        assertArrayEquals(expected, result);
    }

    @Test
    void getInverseOfMatrix() {
        //este teste não corre com double
        int[][] matrix = {{3, 0, 2}, {2, 0, -2}, {0, 1, 1}};
        float[][] expected = {{0.2f, 0.2f, 0.0f}, {-0.2f, 0.3f, 1f}, {0.2f, -0.3f, 0.0f}};
        float[][] result = ExercicioQuinze.getInverseOfMatrix(matrix);
        assertArrayEquals(expected, result);
    }

    @Test
    void getTransposedSquareMatrix() {
        //para matrizes quadradas, o método do Adjugado corresponde à transposição
        int[][] matrix = {{0, 1, 2}, {10, 11, 12}, {20, 21, 22}};
        int[][] expected = {{0, 10, 20}, {1, 11, 21}, {2, 12, 22}};
        int[][] result = ExercicioQuinze.getTransposedMatrix(matrix);
        assertArrayEquals(expected, result);
    }

    @Test
    void getTransposedRectWideMatrix() {
        //para matrizes quadradas, o método do Adjugado corresponde à transposição
        int[][] matrix = {{0, 1, 2, 3}, {10, 11, 12, 13}, {20, 21, 22, 23}};
        int[][] expected = {{0, 10, 20}, {1, 11, 21}, {2, 12, 22}, {3, 13, 23}};
        int[][] result = ExercicioQuinze.getTransposedMatrix(matrix);
        assertArrayEquals(expected, result);
    }
}