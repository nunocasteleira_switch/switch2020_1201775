import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezoitoTest {

    @Test
    void getMaskMatrix() {
        char[][] baseMatrix = {
                {'X', 'S', 'I', 'M', 'A', 'N', 'U', 'S', 'T', 'B'},
                {'Z', 'T', 'E', 'C', 'T', 'O', 'N', 'I', 'C', 'A'},
                {'J', 'J', 'S', 'O', 'M', 'S', 'I', 'S', 'O', 'G'},
                {'O', 'T', 'O', 'M', 'E', 'R', 'A', 'M', 'R', 'E'},
                {'R', 'V', 'I', 'I', 'U', 'C', 'R', 'O', 'T', 'D'},
                {'T', 'A', 'H', 'L', 'A', 'F', 'E', 'G', 'N', 'U'},
                {'N', 'C', 'B', 'L', 'R', 'L', 'T', 'R', 'E', 'T'},
                {'E', 'I', 'P', 'A', 'G', 'J', 'H', 'A', 'C', 'I'},
                {'C', 'L', 'S', 'C', 'L', 'J', 'C', 'F', 'O', 'N'},
                {'I', 'P', 'O', 'R', 'X', 'O', 'I', 'O', 'P', 'G'},
                {'P', 'E', 'L', 'E', 'N', 'E', 'R', 'G', 'I', 'A'},
                {'E', 'R', 'O', 'M', 'E', 'R', 'T', 'H', 'H', 'M'}
        };
        char searchLetter = 'A';
        int[][] expected = {
                {0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 1, 0, 0, 1, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 1, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
        };
        int[][] result = ExercicioDezoito.getMaskMatrix(baseMatrix, searchLetter);
        assertArrayEquals(expected, result);
    }

    @Test
    void getWordFromWordSearch() {
        char[][] baseMatrix = {
                {'X', 'S', 'I', 'M', 'A', 'N', 'U', 'S', 'T', 'B'},
                {'Z', 'T', 'E', 'C', 'T', 'O', 'N', 'I', 'C', 'A'},
                {'J', 'J', 'S', 'O', 'M', 'S', 'I', 'S', 'O', 'G'},
                {'O', 'T', 'O', 'M', 'E', 'R', 'A', 'M', 'R', 'E'},
                {'R', 'V', 'I', 'I', 'U', 'C', 'R', 'O', 'T', 'D'},
                {'T', 'A', 'H', 'L', 'A', 'F', 'E', 'G', 'N', 'U'},
                {'N', 'C', 'B', 'L', 'R', 'L', 'T', 'R', 'E', 'T'},
                {'E', 'I', 'P', 'A', 'G', 'J', 'H', 'A', 'C', 'I'},
                {'C', 'L', 'S', 'C', 'L', 'J', 'C', 'F', 'O', 'N'},
                {'I', 'P', 'O', 'R', 'X', 'O', 'I', 'O', 'P', 'G'},
                {'P', 'E', 'L', 'E', 'N', 'E', 'R', 'G', 'I', 'A'},
                {'E', 'R', 'O', 'M', 'E', 'R', 'T', 'H', 'H', 'M'}
        };
        //Direction 0
        int[] coord1Zero = {1, 1};
        int[] coord2Zero = {1, 9};
        String expectedWordInDirectionZero = "TECTONICA";
        String resultWordInDirectionZero = ExercicioDezoito.getWordFromWordSearch(coord1Zero, coord2Zero, baseMatrix);

        //Direction 1
        int[] coord1One = {2, 7};
        int[] coord2One = {2, 2};
        String expectedWordInDirectionOne = "SISMOS";
        String resultWordInDirectionOne = ExercicioDezoito.getWordFromWordSearch(coord1One, coord2One, baseMatrix);

        //Direction 2
        int[] coord1Two = {0, 7};
        int[] coord2Two = {9, 7};
        String expectedWordInDirectionTwo = "SISMOGRAFO";
        String resultWordInDirectionTwo = ExercicioDezoito.getWordFromWordSearch(coord1Two, coord2Two, baseMatrix);

        //Direction 3
        int[] coord1Three = {11, 9};
        int[] coord2Three = {3, 9};
        String expectedWordInDirectionThree = "MAGNITUDE";
        String resultWordInDirectionThree = ExercicioDezoito.getWordFromWordSearch(coord1Three, coord2Three, baseMatrix);

        //Direction 4
        int[] coord1Four = {5, 1};
        int[] coord2Four = {9, 5};
        String expectedWordInDirectionFour = "ABALO";
        String resultWordInDirectionFour = ExercicioDezoito.getWordFromWordSearch(coord1Four, coord2Four, baseMatrix);

        //Direction 5
        int[] coord1Five = {2, 2};
        int[] coord2Five = {0, 0};
        String expectedWordInDirectionFive = "STX";
        String resultWordInDirectionFive = ExercicioDezoito.getWordFromWordSearch(coord1Five, coord2Five, baseMatrix);

        //Direction 6
        int[] coord1Six = {0, 9};
        int[] coord2Six = {2, 7};
        String expectedWordInDirectionSix = "BCS";
        String resultWordInDirectionSix = ExercicioDezoito.getWordFromWordSearch(coord1Six, coord2Six, baseMatrix);

        //Direction 7
        int[] coord1Seven = {7, 2};
        int[] coord2Seven = {2, 7};
        String expectedWordInDirectionSeven = "PLACAS";
        String resultWordInDirectionSeven = ExercicioDezoito.getWordFromWordSearch(coord1Seven, coord2Seven, baseMatrix);

        assertEquals(expectedWordInDirectionZero, resultWordInDirectionZero);
        assertEquals(expectedWordInDirectionOne, resultWordInDirectionOne);
        assertEquals(expectedWordInDirectionTwo, resultWordInDirectionTwo);
        assertEquals(expectedWordInDirectionThree, resultWordInDirectionThree);
        assertEquals(expectedWordInDirectionFour, resultWordInDirectionFour);
        assertEquals(expectedWordInDirectionFive, resultWordInDirectionFive);
        assertEquals(expectedWordInDirectionSix, resultWordInDirectionSix);
        assertEquals(expectedWordInDirectionSeven, resultWordInDirectionSeven);
    }

    @Test
    void validateWord() {
        String[] solutions = {"ABALO", "ENERGIA", "EPICENTRO", "FALHA", "GRAU", "HIPOCENTRO", "MAGNITUDE", "MAREMOTO",
                "MERCALLI", "PLACAS", "REPLICA", "RICHTER", "SISMOGRAFO", "SISMOS", "SOLO", "TECTONICA",
                "TREMOR", "TSUNAMI"};
        String candidateWord = "SISMOS";
        boolean result = ExercicioDezoito.validateWord(candidateWord, solutions);

        assertTrue(result);
    }

    @Test
    void removeFoundWord() {
        String[] solutions = {"ABALO", "ENERGIA", "EPICENTRO", "FALHA", "GRAU", "HIPOCENTRO", "MAGNITUDE", "MAREMOTO",
                "MERCALLI", "PLACAS", "REPLICA", "RICHTER", "SISMOGRAFO", "SISMOS", "SOLO", "TECTONICA",
                "TREMOR", "TSUNAMI"};
        String foundWord = "SISMOS";
        String[] expected = {"ABALO", "ENERGIA", "EPICENTRO", "FALHA", "GRAU", "HIPOCENTRO", "MAGNITUDE", "MAREMOTO",
                "MERCALLI", "PLACAS", "REPLICA", "RICHTER", "SISMOGRAFO", "SOLO", "TECTONICA",
                "TREMOR", "TSUNAMI"};
        String[] result = ExercicioDezoito.removeWord(foundWord, solutions);

        assertArrayEquals(expected, result);
    }

    //=====================\\
    //  Do Tomás Osswald   \\
    //=====================\\

    @Test
    void getMaskArraySizeTwoSquareFindA() {
        // arrange
        char[][] arrayLetters = {{'A', 'B'}, {'C', 'D'}};
        char letter = 'A';
        int[][] expected = {{1, 0}, {0, 0}};

        // act
        int[][] result = ExercicioDezoito.getMaskMatrix(arrayLetters, letter);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getMaskArraySizeTwoSquareFindLowerCaseA() {
        // arrange
        char[][] arrayLetters = {{'A', 'B'}, {'C', 'D'}};
        char letter = 'a';
        int[][] expected = {{1, 0}, {0, 0}};

        // act
        int[][] result = ExercicioDezoito.getMaskMatrix(arrayLetters, letter);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getMaskArraySizeSevenSquareFindA() {
        // arrange
        char[][] arrayLetters = {
                {'E', 'E', 'D', 'N', 'N', 'S', 'E'},
                {'L', 'A', 'A', 'R', 'R', 'T', 'V'},
                {'B', 'A', 'L', 'U', 'J', 'R', 'E'},
                {'U', 'R', 'N', 'T', 'B', 'I', 'E'},
                {'O', 'R', 'I', 'E', 'T', 'N', 'I'},
                {'D', 'A', 'G', 'R', 'T', 'G', 'L'},
                {'J', 'Y', 'J', 'A', 'V', 'A', 'T'}};
        char letter = 'A';
        int[][] expected = {
                {0, 0, 0, 0, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0},
                {0, 1, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 1, 0, 0, 0, 0, 0},
                {0, 0, 0, 1, 0, 1, 0}};

        // act
        int[][] result = ExercicioDezoito.getMaskMatrix(arrayLetters, letter);

        // assert
        assertArrayEquals(expected, result);
    }

    @Test
    void getCoordinatesOfWordDirection0() {
        int[] coord1 = {1, 0};
        int[] coord2 = {1, 2};
        int[][] expected = {{1, 0}, {1, 1}, {1, 2}};
        int[][] result = ExercicioDezoito.getCoordinatesOfWord(coord1, coord2);
        assertArrayEquals(expected, result);
    }

    @Test
    void getCoordinatesOfWordDirection1() {
        int[] coord1 = {1, 2};
        int[] coord2 = {1, 0};
        int[][] expected = {{1, 0}, {1, 1}, {1, 2}};
        int[][] result = ExercicioDezoito.getCoordinatesOfWord(coord1, coord2);
        assertArrayEquals(expected, result);
    }

    @Test
    void getCoordinatesOfWordDirection2() {
        int[] coord1 = {0, 0};
        int[] coord2 = {2, 0};
        int[][] expected = {{0, 0}, {1, 0}, {2, 0}};
        int[][] result = ExercicioDezoito.getCoordinatesOfWord(coord1, coord2);
        assertArrayEquals(expected, result);
    }

    @Test
    void getCoordinatesOfWordDirection3() {
        int[] coord1 = {2, 0};
        int[] coord2 = {0, 0};
        int[][] expected = {{0, 0}, {1, 0}, {2, 0}};
        int[][] result = ExercicioDezoito.getCoordinatesOfWord(coord1, coord2);
        assertArrayEquals(expected, result);
    }

    @Test
    void getCoordinatesOfWordDirection4() {
        int[] coord1 = {3, 2};
        int[] coord2 = {5, 4};
        int[][] expected = {{3, 2}, {4, 3}, {5, 4}};
        int[][] result = ExercicioDezoito.getCoordinatesOfWord(coord1, coord2);
        assertArrayEquals(expected, result);
    }

    @Test
    void getCoordinatesOfWordDirection5() {
        int[] coord1 = {5, 4};
        int[] coord2 = {3, 2};
        int[][] expected = {{3, 2}, {4, 3}, {5, 4}};
        int[][] result = ExercicioDezoito.getCoordinatesOfWord(coord1, coord2);
        assertArrayEquals(expected, result);
    }

    @Test
    void getCoordinatesOfWordDirection6() {
        int[] coord1 = {3, 4};
        int[] coord2 = {5, 2};
        int[][] expected = {{3, 4}, {4, 3}, {5, 2}};
        int[][] result = ExercicioDezoito.getCoordinatesOfWord(coord1, coord2);
        assertArrayEquals(expected, result);
    }

    @Test
    void getCoordinatesOfWordDirection7() {
        int[] coord1 = {5, 2};
        int[] coord2 = {3, 4};
        int[][] expected = {{3, 4}, {4, 3}, {5, 2}};
        int[][] result = ExercicioDezoito.getCoordinatesOfWord(coord1, coord2);
        assertArrayEquals(expected, result);
    }

    @Test
    void getIntersectionCoordinateFromTwoWords() {
        int[] coord1Word1 = {1, 0};
        int[] coord2Word1 = {1, 2};
        int[] coord1Word2 = {1, 1};
        int[] coord2Word2 = {4, 4};
        int[][] expected = {{1, 1}};
        int[][] result = ExercicioDezoito.getIntersectionCoordinateFromTwoWords(coord1Word1, coord2Word1, coord1Word2, coord2Word2);
        assertArrayEquals(expected, result);
    }

    @Test
    void getIntersectionCoordinateFromTwoWordsWithMultipleIntersections() {
        int[] coord1Word1 = {1, 0};
        int[] coord2Word1 = {1, 2};
        int[] coord1Word2 = {1, 1};
        int[] coord2Word2 = {1, 3};
        int[][] expected = {{1, 1}, {1, 2}};
        int[][] result = ExercicioDezoito.getIntersectionCoordinateFromTwoWords(coord1Word1, coord2Word1, coord1Word2, coord2Word2);
        assertArrayEquals(expected, result);
    }
}