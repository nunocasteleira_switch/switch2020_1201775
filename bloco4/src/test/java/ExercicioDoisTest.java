import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDoisTest {

    @Test
    void separateDigitsIntoArrayPositions() {
        int number = 36781;
        int[] expected = {3, 6, 7, 8, 1};
        int[] result = ExercicioDois.separateDigitsIntoArray(number);
        assertArrayEquals(expected, result);
    }

    @Test
    void separateSingleDigitIntoArrayPositions() {
        int number = 1;
        int[] expected = {1};
        int[] result = ExercicioDois.separateDigitsIntoArray(number);
        assertArrayEquals(expected, result);
    }
}