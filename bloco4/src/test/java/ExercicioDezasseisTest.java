import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezasseisTest {

    @Test
    void testExercicioDezaseisMasMetodoNoExercicioQuinze() {
        int[][] matrix = {{3, 0, 2}, {2, 0, -2}, {0, 1, 1}};
        int expected = 10;
        int result = ExercicioQuinze.getDeterminantOfMatrix(matrix);
        assertEquals(expected, result);
    }
}