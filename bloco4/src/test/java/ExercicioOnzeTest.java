import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioOnzeTest {

    @Test
    void getDotProductOfTwoArrays() {
        int[] first = {2, 4, 6, 8};
        int[] second = {3, 5, 7, 9};
        long expected = 140;
        long result = ExercicioOnze.getDotProductOfTwoArrays(first, second);
        assertEquals(expected, result);
    }

    @Test
    void getDotProductOfTwoArraysWithOneNullArray() {
        int[] first = null;
        int[] second = {3, 5, 7, 9};
        //noinspection ConstantConditions
        Long result = ExercicioOnze.getDotProductOfTwoArrays(first, second);
        assertNull(result);
    }

    @Test
    void getDotProductOfTwoArraysWithOneEmptyArray() {
        int[] first = {};
        int[] second = {3, 5, 7, 9};
        Long result = ExercicioOnze.getDotProductOfTwoArrays(first, second);
        assertNull(result);
    }

    @Test
    void getDotProductOfTwoArraysWithSecondArrayEmpty() {
        int[] first = {3, 5, 7, 9};
        int[] second = {};
        Long result = ExercicioOnze.getDotProductOfTwoArrays(first, second);
        assertNull(result);
    }
}