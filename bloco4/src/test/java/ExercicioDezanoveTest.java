import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezanoveTest {

    @Test
    void getMaskSudokuMatrix() {
        int[][] baseSudokuMatrix = {
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        int[][] expected = {
                {0, 0, 1, 0, 0, 1, 1, 0, 1},
                {1, 0, 0, 0, 0, 1, 0, 1, 1},
                {0, 1, 1, 1, 1, 1, 0, 0, 0},
                {0, 0, 0, 1, 0, 1, 1, 0, 0},
                {1, 0, 1, 0, 0, 0, 1, 0, 1},
                {0, 0, 0, 0, 0, 0, 1, 1, 1},
                {1, 1, 1, 1, 1, 0, 0, 1, 1},
                {1, 0, 1, 1, 1, 0, 0, 0, 0},
                {0, 1, 0, 0, 0, 1, 1, 1, 0}
        };
        int[][] result = ExercicioDezanove.getMaskSudokuMatrix(baseSudokuMatrix);
        assertArrayEquals(expected, result);
    }

    @Test
    void insertNumberOnBaseSudokuMatrix() {
        int[][] baseSudokuMatrix = {
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        int coordx = 0;
        int coordy = 0;
        int sudokuNumber = 7;
        int[][] expected = {
                {7, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        int[][] result = ExercicioDezanove.insertNumberOnBaseSudokuMatrix(baseSudokuMatrix, sudokuNumber, coordx, coordy);
        assertArrayEquals(expected, result);
    }

   /* @Test
    void insertNumberOnMaskSudokuMatrix() {
        int[][] maskSudokuMatrix = {
                {0, 0, 1, 0, 0, 1, 1, 0, 1},
                {1, 0, 0, 0, 0, 1, 0, 1, 1},
                {0, 1, 1, 1, 1, 1, 0, 0, 0},
                {0, 0, 0, 1, 0, 1, 1, 0, 0},
                {1, 0, 1, 0, 0, 0, 1, 0, 1},
                {0, 0, 0, 0, 0, 0, 1, 1, 1},
                {1, 1, 1, 1, 1, 0, 0, 1, 1},
                {1, 0, 1, 1, 1, 0, 0, 0, 0},
                {0, 1, 0, 0, 0, 1, 1, 1, 0}
        };
        int coordx = 0;
        int coordy = 0;
        int[][] expected = {
                {1, 0, 1, 0, 0, 1, 1, 0, 1},
                {1, 0, 0, 0, 0, 1, 0, 1, 1},
                {0, 1, 1, 1, 1, 1, 0, 0, 0},
                {0, 0, 0, 1, 0, 1, 1, 0, 0},
                {1, 0, 1, 0, 0, 0, 1, 0, 1},
                {0, 0, 0, 0, 0, 0, 1, 1, 1},
                {1, 1, 1, 1, 1, 0, 0, 1, 1},
                {1, 0, 1, 1, 1, 0, 0, 0, 0},
                {0, 1, 0, 0, 0, 1, 1, 1, 0}
        };
        int[][] result = ExercicioDezanove.insertNumberOnMaskSudokuMatrix(maskSudokuMatrix, coordx, coordy);
        assertArrayEquals(expected, result);
    }*/

    @Test
    void thereIsEmptyCellsOnSudokuMatrix() {
        int[][] baseSudokuMatrix = {
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        boolean result = ExercicioDezanove.areEmptyCellsOnSudokuMatrix(baseSudokuMatrix);
        assertTrue(result);
    }

    @Test
    void thereIsNotEmptyCellsOnSudokuMatrix() {
        int[][] baseSudokuMatrix = {
                {2, 2, 3, 4, 5, 6, 7, 8, 9},
                {3, 3, 3, 4, 5, 6, 7, 8, 9},
                {1, 2, 3, 4, 5, 6, 7, 8, 9},
                {4, 4, 4, 4, 5, 6, 7, 8, 9},
                {5, 5, 5, 5, 5, 6, 7, 8, 9},
                {6, 6, 6, 6, 6, 6, 7, 8, 9},
                {7, 7, 7, 7, 7, 7, 7, 8, 9},
                {8, 8, 8, 8, 8, 8, 8, 8, 9},
                {9, 9, 9, 9, 9, 9, 9, 9, 9}
        };
        boolean result = ExercicioDezanove.areEmptyCellsOnSudokuMatrix(baseSudokuMatrix);
        assertFalse(result);
    }

    @Test
    void getMatrixWithAllCoordsWithTheSameNumber() {
        int[][] baseSudokuMatrix = {
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        int sudokuNumber = 6;
        int[][] expected = {
                {0, 0, 0, 0, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}
        };
        int[][] result = ExercicioDezanove.getMatrixWithAllCoordsWithSameNumber(baseSudokuMatrix, sudokuNumber);
        assertArrayEquals(expected, result);
    }

    @Test
    void numberIsLegalInLine() {
        int[][] baseSudokuMatrix = {
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        int sudokuNumber = 6;
        int coordx = 3;
        boolean result = ExercicioDezanove.isNumberLegalInRow(baseSudokuMatrix, sudokuNumber, coordx);
        assertTrue(result);
    }

    @Test
    void numberIsNotLegalInLine() {
        int[][] baseSudokuMatrix = {
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        int sudokuNumber = 6;
        int coordx = 0;
        boolean result = ExercicioDezanove.isNumberLegalInRow(baseSudokuMatrix, sudokuNumber, coordx);
        assertFalse(result);
    }

    @Test
    void numberIsLegalInColumn() {
        int[][] baseSudokuMatrix = {
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        int sudokuNumber = 6;
        int coordy = 2;
        boolean result = ExercicioDezanove.isNumberLegalInColumn(baseSudokuMatrix, sudokuNumber, coordy);
        assertTrue(result);
    }

    @Test
    void numberIsNotLegalInColumn() {
        int[][] baseSudokuMatrix = {
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        int sudokuNumber = 6;
        int coordy = 8;
        boolean result = ExercicioDezanove.isNumberLegalInColumn(baseSudokuMatrix, sudokuNumber, coordy);
        assertFalse(result);
    }

    @Test
    void numberIsLegalInSquare() {
        int[][] baseSudokuMatrix = {
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        int sudokuNumber = 9;
        int coordX = 1;
        int coordY = 1;
        boolean result = ExercicioDezanove.isNumberLegalInSquare(baseSudokuMatrix, sudokuNumber, coordX, coordY);
        assertTrue(result);
    }

    @Test
    void numberIsNotLegalInSquare() {
        int[][] baseSudokuMatrix = {
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        int sudokuNumber = 6;
        int coordX = 1;
        int coordY = 1;
        boolean result = ExercicioDezanove.isNumberLegalInSquare(baseSudokuMatrix, sudokuNumber, coordX, coordY);
        assertFalse(result);
    }

    @Test
    void numberIsLegalInSquareOneZero() {
        int[][] baseSudokuMatrix = {
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        int sudokuNumber = 2;
        int coordX = 4;
        int coordY = 1;
        boolean result = ExercicioDezanove.isNumberLegalInSquare(baseSudokuMatrix, sudokuNumber, coordX, coordY);
        assertTrue(result);
    }

    @Test
    void numberIsLegalInSquareTwoOne() {
        int[][] baseSudokuMatrix = {
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        int sudokuNumber = 5;
        int coordX = 8;
        int coordY = 3;
        boolean result = ExercicioDezanove.isNumberLegalInSquare(baseSudokuMatrix, sudokuNumber, coordX, coordY);
        assertTrue(result);
    }

    @Test
    void numberIsLegal() {
        int[][] baseSudokuMatrix = {
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        int sudokuNumber = 4;
        int coordX = 1;
        int coordY = 1;
        boolean result = ExercicioDezanove.isNumberLegal(baseSudokuMatrix, sudokuNumber, coordX, coordY);
        assertTrue(result);
    }

    @Test
    void numberIsNotLegal() {
        int[][] baseSudokuMatrix = {
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        int sudokuNumber = 8;
        int coordX = 2;
        int coordY = 0;
        boolean result = ExercicioDezanove.isNumberLegal(baseSudokuMatrix, sudokuNumber, coordX, coordY);
        assertFalse(result);
    }

    @Test
    void canChangeNumber() {
        int[][] maskSudokuMatrix = {
                {0, 0, 1, 0, 0, 1, 1, 0, 1},
                {1, 0, 0, 0, 0, 1, 0, 1, 1},
                {0, 1, 1, 1, 1, 1, 0, 0, 0},
                {0, 0, 0, 1, 0, 1, 1, 0, 0},
                {1, 0, 1, 0, 0, 0, 1, 0, 1},
                {0, 0, 0, 0, 0, 0, 1, 1, 1},
                {1, 1, 1, 1, 1, 0, 0, 1, 1},
                {1, 0, 1, 1, 1, 0, 0, 0, 0},
                {0, 1, 0, 0, 0, 1, 1, 1, 0}
        };
        int coordX = 0;
        int coordY = 0;
        boolean result = ExercicioDezanove.canAddOrChangeNumber(maskSudokuMatrix, coordX, coordY);
        assertTrue(result);
    }

    @Test
    void isSudokuMatrixVerified() {
        int[][] baseSudokuMatrix = {
                {7, 5, 1, 3, 8, 2, 4, 9, 6},
                {6, 9, 3, 4, 7, 5, 2, 1, 8},
                {4, 8, 2, 9, 1, 6, 7, 5, 3},
                {2, 7, 6, 8, 9, 4, 5, 3, 1},
                {1, 3, 8, 5, 2, 7, 6, 4, 9},
                {9, 4, 5, 1, 6, 3, 8, 2, 7},
                {5, 1, 4, 7, 3, 8, 9, 6, 2},
                {3, 6, 7, 2, 4, 9, 1, 8, 5},
                {8, 2, 9, 6, 5, 1, 3, 7, 4}
        };
        boolean result = ExercicioDezanove.isSudokuMatrixVerified(baseSudokuMatrix);
        assertTrue(result);
    }
}