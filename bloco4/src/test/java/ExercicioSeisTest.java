import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioSeisTest {

    @Test
    void testExercicioSeisMasMetodoNoExercicioQuatro() {
        int[] array = {3, 4, 6, 8, 1, 23, 89, 12};
        int count = 4;
        int[] expected = {3, 4, 6, 8};
        int[] result = ExercicioQuatro.truncateArrayWithGivenLength(array, count);
        assertArrayEquals(expected, result);
    }
}