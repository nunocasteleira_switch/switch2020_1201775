import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioQuatroTest {

    @Test
    void evenDigitsInArray() {
        int[] numbers = {3, 6, 7, 8, 1};
        int[] expected = {6, 8};
        int[] result = ExercicioQuatro.evenDigitsInArray(numbers);
        assertArrayEquals(expected, result);
    }

    @Test
    void oddDigitsInArray() {
        int[] numbers = {3, 6, 7, 8, 1};
        int[] expected = {3, 7, 1};
        int[] result = ExercicioQuatro.oddDigitsInArray(numbers);
        assertArrayEquals(expected, result);
    }

    @Test
    void nullArrayFromNonEvenArray() {
        int[] array = {1};
        int[] expected = {};
        int[] result = ExercicioQuatro.evenDigitsInArray(array);
        assertArrayEquals(expected, result);
    }

    @Test
    void nullArrayFromNonOddArray() {
        int[] array = {2};
        int[] expected = {};
        int[] result = ExercicioQuatro.oddDigitsInArray(array);
        assertArrayEquals(expected, result);
    }

    @Test
    public void getOddIntegersFromArrayTwo() {
        //Não teria apanhado um erro sem este teste.
        //O -7 MOD 2 dá -1 e não 1.
        //Por isso alterei o método para retornar ímpar se
        // number MOD 2 != 0
        //Obrigado Ricardo Mendes.
        int[] array = {-2, -7, 14, 9, 34};
        int[] expected = {-7, 9};
        int[] result = ExercicioQuatro.oddDigitsInArray(array);
        assertArrayEquals(expected, result);
    }
}