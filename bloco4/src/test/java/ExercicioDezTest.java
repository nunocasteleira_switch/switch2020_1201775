import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezTest {

    @Test
    void smallerValueInArray() {
        int[] array = {21, 54, 2, 45, 8, 254, 45};
        int expected = 2;
        int result = ExercicioDez.smallerValueInArray(array);
        assertEquals(expected, result);
    }

    @Test
    void smallerValueInNullArray() {
        //noinspection ConstantConditions
        Integer result = ExercicioDez.smallerValueInArray(null);
        assertNull(result);
    }

    @Test
    void smallerValueInEmptyArray() {
        int[] array = {};
        Integer result = ExercicioDez.smallerValueInArray(array);
        assertNull(result);
    }

    @Test
    void smallerValueInArrayInfLimit() {
        int[] array = {-1, 54, 2, 45, 8, 254, 45};
        int expected = -1;
        int result = ExercicioDez.smallerValueInArray(array);
        assertEquals(expected, result);
    }

    @Test
    void smallerValueInArraySupLimit() {
        int[] array = {-1, 54, 2, 45, 8, 254, -3};
        int expected = -3;
        int result = ExercicioDez.smallerValueInArray(array);
        assertEquals(expected, result);
    }

    @Test
    void smallerValueInArrayWithSingleElement() {
        int[] array = {-1};
        int expected = -1;
        int result = ExercicioDez.smallerValueInArray(array);
        assertEquals(expected, result);
    }

    @Test
    void largestValueInArray() {
        int[] array = {21, 54, 2, 45, 8, 254, 45};
        int expected = 254;
        int result = ExercicioDez.largestValueInArray(array);
        assertEquals(expected, result);
    }

    @Test
    void largestValueInNullArray() {
        int[] array = null;
        //noinspection ConstantConditions
        Integer result = ExercicioDez.largestValueInArray(array);
        assertNull(result);
    }

    @Test
    void largestValueInEmptyArray() {
        int[] array = {};
        Integer result = ExercicioDez.largestValueInArray(array);
        assertNull(result);
    }

    @Test
    void largestValueInArraySupLimit() {
        int[] array = {21, 54, 2, 45, 8, 254, 4589};
        int expected = 4589;
        int result = ExercicioDez.largestValueInArray(array);
        assertEquals(expected, result);
    }

    @Test
    void largestValueInArrayInfLimit() {
        int[] array = {2145, 54, 2, 45, 8, 254, 45};
        int expected = 2145;
        int result = ExercicioDez.largestValueInArray(array);
        assertEquals(expected, result);
    }

    @Test
    void meanValueInArray() {
        int[] array = {21, 54, 2, 45, 8, 254, 45};
        double expected = 61.28;
        double result = ExercicioDez.meanValueInArray(array);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void meanValueInNullArray() {
        int[] array = null;
        //noinspection ConstantConditions
        Double result = ExercicioDez.meanValueInArray(array);
        assertNull(result);
    }

    @Test
    void meanValueInEmptyArray() {
        int[] array = {};
        Double result = ExercicioDez.meanValueInArray(array);
        assertNull(result);
    }

    @Test
    void productOfValuesInArray() {
        int[] array = {21, 54, 2};
        long expected = 2268;
        long result = ExercicioDez.productOfValuesInArray(array);
        assertEquals(expected, result);
    }

    @Test
    void productOfValuesInNullArray() {
        int[] array = null;
        //noinspection ConstantConditions
        Long result = ExercicioDez.productOfValuesInArray(array);
        assertNull(result);
    }

    @Test
    void productOfValuesInEmptyArray() {
        int[] array = {};
        Long result = ExercicioDez.productOfValuesInArray(array);
        assertNull(result);
    }

    @Test
    void uniqueValuesInArray() {
        int[] array = {1, 2, 2, 3, 4, 5};
        int[] expected = {1, 2, 3, 4, 5};
        int[] result = ExercicioDez.uniqueValuesInArray(array);
        assertArrayEquals(expected, result);
    }

    @Test
    void anotherUniqueValuesInArray() {
        int[] array = {1, 1, 2, 3, 4, 5};
        int[] expected = {1, 2, 3, 4, 5};
        int[] result = ExercicioDez.uniqueValuesInArray(array);
        assertArrayEquals(expected, result);
    }

    @Test
    void justAnotherUniqueValuesInArray() {
        int[] array = {1, 1, 2, 2, 3, 4, 5, 1};
        int[] expected = {1, 2, 3, 4, 5};
        int[] result = ExercicioDez.uniqueValuesInArray(array);
        assertArrayEquals(expected, result);
    }

    @Test
    void invertArray() {
        int[] array = {21, 54, 2, 45, 8, 254, 45};
        int[] expected = {45, 254, 8, 45, 2, 54, 21};
        int[] result = ExercicioDez.invertArray(array);
        assertArrayEquals(expected, result);
    }

    @Test
    void primeNumbersInArray() {
        int[] array = {21, 54, 2, 45, 8, 254, 45};
        int[] expected = {2};
        int[] result = ExercicioDez.primeNumbersInArray(array);
        assertArrayEquals(expected, result);
    }

    @Test
    void twentyThreeIsNumberPrime() {
        int number = 23;
        boolean result = ExercicioDez.isNumberPrime(number);
        assertTrue(result);
    }

    @Test
    void eightIsNotNumberPrime() {
        int number = 8;
        boolean result = ExercicioDez.isNumberPrime(number);
        assertFalse(result);
    }
}