import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioTrezeTest {

    @Test
    void matrixIsSquare() {
        int[][] matrix = {{1,2,3}, {2,3,4}, {4,5,6}};
        boolean result = ExercicioTreze.isMatrixSquare(matrix);
        assertTrue(result);
    }

    @Test
    void matrixIsNotSquare() {
        int[][] matrix = {{1,2,3}, {2,3}, {4,5,6,7}};
        boolean result = ExercicioTreze.isMatrixSquare(matrix);
        assertFalse(result);
    }

    @Test
    void matrixIsNotSquareIfMatrixIsNull() {
        int[][] matrix = null;
        //noinspection ConstantConditions
        boolean result = ExercicioTreze.isMatrixSquare(matrix);
        assertFalse(result);
    }

    @Test
    void matrixIsNotSquareIfMatrixIsEmpty() {
        int[][] matrix = {};
        boolean result = ExercicioTreze.isMatrixSquare(matrix);
        assertFalse(result);
    }
}