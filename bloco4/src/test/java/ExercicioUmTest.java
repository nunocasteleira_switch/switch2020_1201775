import static org.junit.jupiter.api.Assertions.assertEquals;

class ExercicioUmTest {

    @org.junit.jupiter.api.Test
    void count5DigitsIn36781() {
        int number = 36781;
        int expected = 5;
        int result = ExercicioUm.countDigits(number);
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void count0DigitsInNeg125() {
        int number = -125;
        int expected = 0;
        int result = ExercicioUm.countDigits(number);
        assertEquals(expected, result);
    }
}