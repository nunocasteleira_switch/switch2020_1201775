import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class ExercicioOitoTest {

    @Test
    void getMultiplesOfTwoDigits() {
        int first = 12;
        int last = 4;
        int[] multiplex = {2, 3};
        int[] expected = {6, 12};
        int[] result = ExercicioOito.getMultiplesOfMultipliersArray(first, last, multiplex);
        assertArrayEquals(expected, result);
    }

    @Test
    void getMultiplesOfThreeDigits() {
        int first = 1;
        int last = 12;
        int[] multiplex = {2, 3, 4};
        int[] expected = {12};
        int[] result = ExercicioOito.getMultiplesOfMultipliersArray(first, last, multiplex);
        assertArrayEquals(expected, result);
    }

    @Test
    void getCommonMultiplesFromArrayInIntervalVersionTwoResultTwentyForty() {
        int[] array = {2,4,5,10};
        int intervalBegin = 2;
        int intervalEnd = 40;
        int[] expected = {20,40};
        int[] result = ExercicioOito.getMultiplesOfMultipliersArray(intervalBegin, intervalEnd, array);
        assertArrayEquals(expected, result);
    }

    @Test
    void getCommonMultiplesFromArrayInIntervalVersionTwoResult420_840() {
        int[] array = {2,5,10,20,21};
        int intervalBegin = 2;
        int intervalEnd = 1000;
        int[] expected = {420,840};
        int[] result = ExercicioOito.getMultiplesOfMultipliersArray(intervalBegin, intervalEnd, array);
        assertArrayEquals(expected, result);
    }

    @Test
    public void getArrayWithMultiplesOfThree() {
        int start = 1;
        int end = 20;
        int[] multiples = {2, 4, 8};
        int[] expected = {8, 16};
        int[] result = ExercicioOito.getMultiplesOfMultipliersArray(start, end, multiples);
        assertArrayEquals(expected, result);
    } // End Test
}