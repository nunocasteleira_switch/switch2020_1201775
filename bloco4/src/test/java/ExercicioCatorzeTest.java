import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioCatorzeTest {

    @Test
    void matrixIsRect() {
        int[][] matrix = {{1,2,3,4}, {2,3,4,5}, {4,5,6,7}};
        boolean result = ExercicioCatorze.isMatrixRect(matrix);
        assertTrue(result);
    }

    @Test
    void matrixIsNotRect() {
        int[][] matrix = {{1,2,3}, {2,3,4}, {4,5,6}};
        boolean result = ExercicioCatorze.isMatrixRect(matrix);
        assertFalse(result);
    }

    @Test
    void matrixIsNotRectWithDifferentElements() {
        int[][] matrix = {{1,2,3}, {2,3}, {4,5,6}};
        boolean result = ExercicioCatorze.isMatrixRect(matrix);
        assertFalse(result);
    }

    @Test
    void matrixIsNotRectIfMatrixIsNull() {
        int[][] matrix = null;
        //noinspection ConstantConditions
        boolean result = ExercicioTreze.isMatrixSquare(matrix);
        assertFalse(result);
    }

    @Test
    void matrixIsNotRectIfMatrixIsEmpty() {
        int[][] matrix = {};
        boolean result = ExercicioTreze.isMatrixSquare(matrix);
        assertFalse(result);
    }
}