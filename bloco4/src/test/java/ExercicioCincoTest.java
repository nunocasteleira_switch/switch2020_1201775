import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ExercicioCincoTest {

    @Test
    void sumEvenDigitsOnNumber() {
        int number = 36781;
        int expected = 14;
        int result = ExercicioCinco.sumEvenDigitsOnNumber(number);
        assertEquals(expected, result);
    }

    @Test
    public void sumEvenDigitsOnNumberIsSixteen() {
        int number = 8672733;
        int expected = 16;
        int result = ExercicioCinco.sumEvenDigitsOnNumber(number);
        assertEquals(expected, result);
    }

    @Test
    void sumOddDigitsOnNumber() {
        int number = 36781;
        int expected = 11;
        int result = ExercicioCinco.sumOddDigitsOnNumber(number);
        assertEquals(expected, result);
    }

    @Test
    public void sumOddDigitsOnNumberIsTwelve() {
        int number = 1673821;
        int expected = 12;
        int result = ExercicioCinco.sumOddDigitsOnNumber(number);
        assertEquals(expected, result);
    } // End Test
}