import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioNoveTest {

    @Test
    void isPalindrome() {
        int number = 1234321;
        boolean result = ExercicioNove.isPalindrome(number);
        assertTrue(result);
    }

    @Test
    void isNotPalindrome() {
        int number = 12434321;
        boolean result = ExercicioNove.isPalindrome(number);
        assertFalse(result);
    }

    @Test
    void isAnotherPalindrome() {
        int number = 12344321;
        boolean result = ExercicioNove.isPalindrome(number);
        assertTrue(result);
    }

    @Test
    void isNotAnotherPalindrome() {
        int number = 12345;
        boolean result = ExercicioNove.isPalindrome(number);
        assertFalse(result);
    }

    @Test
    void testVverificarCapicua() {
        int numero = 101;
        boolean result = ExercicioNove.isPalindrome(numero);
        assertTrue(result);
    }

    @Test
    void isNumberACapicuaTestThree() {
        int number = 122431;
        boolean result;
        result = ExercicioNove.isPalindrome(number);
        assertFalse(result);
    }
}