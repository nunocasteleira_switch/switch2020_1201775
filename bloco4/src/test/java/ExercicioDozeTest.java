import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDozeTest {

    @Test
    void getCommonLengthNumberWrong() {
        int[][] matrix = {{1,2,3}, {2,3}, {4,5,6,7}};
        int expected = -1;
        int result = ExercicioDoze.getCommonLengthNumber(matrix);
        assertEquals(expected, result);
    }

    @Test
    void getCommonLengthNumberRight() {
        int[][] matrix = {{1,2,3}, {2,3,4}, {4,5,6}};
        int expected = 3;
        int result = ExercicioDoze.getCommonLengthNumber(matrix);
        assertEquals(expected, result);
    }

    @Test
    void getCommonLengthNumberWithNullMatrix() {
        int[][] matrix = null;
        //noinspection ConstantConditions
        Integer result = ExercicioDoze.getCommonLengthNumber(matrix);
        assertNull( result);
    }

    @Test
    void getCommonLengthNumberWithEmptyMatrix() {
        int[][] matrix = {};
        Integer result = ExercicioDoze.getCommonLengthNumber(matrix);
        assertNull( result);
    }
}