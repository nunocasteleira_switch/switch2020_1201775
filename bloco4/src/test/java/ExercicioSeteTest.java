import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioSeteTest {

    @Test
    void get2MultiplesOf3OnGivenInterval() {
        int first = 10;
        int last = 4;
        int multiplex = 3;
        int[] expected = {6, 9};
        int[] result = ExercicioSete.getMultiplesOnGivenInterval(first, last, multiplex);
        assertArrayEquals(expected, result);
    }

    @Test
    void get5MultiplesOf5OnGivenInterval() {
        int first = 1;
        int last = 25;
        int multiplex = 5;
        int[] expected = {5, 10, 15, 20, 25};
        int[] result = ExercicioSete.getMultiplesOnGivenInterval(first, last, multiplex);
        assertArrayEquals(expected, result);
    }

    @Test
    void getMultiplesOfNumberInIntervalResultFourSixEightTen() {
        int number = 2;
        int intervalBegin = 4;
        int intervalEnd = 10;
        int[] expected = {4,6,8,10};
        int[] result = ExercicioSete.getMultiplesOnGivenInterval(intervalBegin, intervalEnd, number);
        assertArrayEquals(expected, result);
    }

    @Test
    void getMultiplesOfNumberInIntervalResultTwo() {
        int number = 2;
        int intervalBegin = 1;
        int intervalEnd = 3;
        int[] expected = {2};
        int[] result = ExercicioSete.getMultiplesOnGivenInterval(intervalBegin, intervalEnd, number);
        assertArrayEquals(expected, result);
    }

    @Test
    void getMultiplesOfNumberInIntervalResultOne() {
        //Obrigado Osswald, sem este teste não teria reparado que ao calcular o tamanho da array de destino
        // com dois números iguais (intervalo com apenas um número), ao subtrair 1 - 1 não poderia criar um array_
        //de índice zero.
        int number = 1;
        int intervalBegin = 1;
        int intervalEnd = 1;
        int[] expected = {1};
        int[] result = ExercicioSete.getMultiplesOnGivenInterval(intervalBegin, intervalEnd, number);
        assertArrayEquals(expected, result);
    }
}