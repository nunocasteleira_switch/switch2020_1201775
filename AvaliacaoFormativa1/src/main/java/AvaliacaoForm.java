public class AvaliacaoForm {
    public static void main(String[] args) {

    }

    /**
     * This method returns the Matching Window to use
     * using the formula
     * ( max(word1, word2) / 2) - 1
     * @param firstSize the first word size
     * @param secondSize the second word size
     * @return an int with the matching window
     */
    public static int getMatchingWindow(int firstSize, int secondSize) {
        int window;
        int max = Math.max(firstSize, secondSize);
        window = (max / 2) - 1;
        return window;
    }

    /**
     * This method adds a char to a char array
     * @param letter the char to add
     * @param charArray the array in which we're going to add the letter AT THE END
     * @return an array of chars with the letter added at the end
     */
    public static char[] addCharToWordArray(char letter, char[] charArray) {
        int size = charArray.length;
        char[] newCharArray = new char[size + 1];
        if (charArray.length != 0) {
            for (int i = 0; i < charArray.length; i++) {
                newCharArray[i] = charArray[i];
            }
            newCharArray[size] = letter;
        } else {
            newCharArray[0] = letter;
        }
        return newCharArray;
    }

    /**
     * This method returns an array with the matching chars using a matching window.
     * The char is compared within a window of two chars before and after (including).
     * The same char can't be used for comparison twice.
     * @param word the word of which we want to get the char array.
     * @param reference the word of which we want to compare.
     * @return an array with the matching chars.
     */
    public static char[] getMatchingCharArray(String word, String reference) {
        //normalize case
        word = word.toUpperCase();
        reference = reference.toUpperCase();
        char[] wordChars = word.toCharArray();
        char[] referenceChars = reference.toCharArray();
        char[] matchingChars = new char[0];
        int loopSize = Math.min(wordChars.length, referenceChars.length);
        int matchingWindow = getMatchingWindow(wordChars.length, referenceChars.length);
        for (int i = 0; i < word.length(); i++) {
            boolean matches = false;
            //prevent Out of Bounds
            int j = i;
            j = getLowestIndexForMatchingWindow(matchingWindow, i);
            int ceiling;
            ceiling = getHighestIndexForMatchingWindow(reference.length(), matchingWindow, i);
            for (; j <= ceiling && !matches; j++) {
                if (wordChars[i] == referenceChars[j]) {
                    matches = true;
                    matchingChars = addCharToWordArray(wordChars[i], matchingChars);
                    referenceChars[j] = 0;
                }

            }
        }
        return matchingChars;
    }

    /**
     * This method ensures we don't fall out of max array bounds.
     * @param max the highest value possible (exclusive).
     * @param matchingWindow the max possible value.
     * @param i the iteration.
     * @return the highest possible value.
     */
    public static int getHighestIndexForMatchingWindow(int max, int matchingWindow, int i) {
        int ceiling = i + matchingWindow;
        if (ceiling >= max - 1) {
            ceiling = max - 1;
        }
        return ceiling;
    }

    /**
     * This method ensures we don't fall out of min array bounds.
     * @param matchingWindow the max possible value.
     * @param i the iteration.
     * @return the lowest possible value.
     */
    public static int getLowestIndexForMatchingWindow(int matchingWindow, int i) {
        int j = 0;
        if (i - matchingWindow < 0) {
            j = 0;
        } else {
            j = i - matchingWindow;
        }
        return j;
    }

    /**
     * This method counts how many transpositions we have between two char arrays
     * @param first the first array of chars
     * @param second the second array of chars
     * @return the number of transpositions.
     */
    public static int countTranspositions(char[] first, char[] second) {
        int count = 0;
        for (int i = 0; i < first.length; i++) {
            if (first[i] != second[i]) {
                count++;
            }
        }
        return count;
    }

    /**
     * This method returns the Similarity value using a math formula, used to compare
     * the similarity between two words.
     * the formula used is
     *  |               / 0                                \ m = 0
     *  | sim(p1, p2) = |
     *  |               \ 1/3( m/p1 + m/p2 + (m-t) / m )   \ m != 0
     *
     *  where p1 and p2 are the number of chars of each words
     *        m is the corresponding chars
     *        t is half of the transpositions
     * @param firstWord A string with the first word to compare
     * @param secondWord A string with the second word to compare
     * @return the value of similarity between 0.0 and 1.0.
     */
    public static double getSimilarityValue(String firstWord, String secondWord) {
        double firstWordSize = firstWord.length();
        double secondWordSize = secondWord.length();
        char[] firstWordMatchingArray = getMatchingCharArray(firstWord, secondWord);
        double firstWordMatching = firstWordMatchingArray.length;
        char[] secondWordMatchingArray = getMatchingCharArray(secondWord, firstWord);
        double secondWordMatching = secondWordMatchingArray.length;
        int transpositions = countTranspositions(firstWordMatchingArray, secondWordMatchingArray);
        double similarity;
        //similarity = (
        //        (firstWordMatching / firstWordSize) +
        //        (secondWordMatching / secondWordSize) +
        //        (firstWordMatching - (transpositions / 2) / firstWordMatching))
        //        / 3;
        if (firstWordMatching == 0) {
            similarity = 0;
        } else {
            similarity = ((firstWordMatching / firstWordSize) +
                    (secondWordMatching / secondWordSize) +
                    ((firstWordMatching - (transpositions / 2.0)) / firstWordMatching)) / 3.0;
        }
        return similarity;
    }
}
