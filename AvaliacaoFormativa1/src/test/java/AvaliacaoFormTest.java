import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AvaliacaoFormTest {

    @org.junit.jupiter.api.Test
    void getMatchingWindow() {
        int firstSize = 3;
        int secondSize = 4;
        int expected = 1;
        int result = AvaliacaoForm.getMatchingWindow(firstSize, secondSize);
        assertEquals(expected, result);
    }

    @Test
    void addCharToEmptyWordArray() {
        char letter = 'A';
        char[] charArray = {};
        char[] expected = {'A'};
        char[] result = AvaliacaoForm.addCharToWordArray(letter, charArray);
        assertArrayEquals(expected, result);
    }

    @Test
    void addCharToWordArrayWithElements() {
        char letter = 'A';
        char[] charArray = {'A'};
        char[] expected = {'A', 'A'};
        char[] result = AvaliacaoForm.addCharToWordArray(letter, charArray);
        assertArrayEquals(expected, result);
    }

    @Test
    void getMatchingCharArrayAmarrarOfAbarcar() {
        String word = "ABARCAR";
        String reference = "AMARRAR";
        char[] expected = {'A', 'A', 'R', 'A', 'R'};
        char[] result = AvaliacaoForm.getMatchingCharArray(word, reference);
        assertArrayEquals(expected, result);
    }

    @Test
    void getMatchingCharArrayAbarcarOfAmarrar() {
        String word = "AMARRAR";
        String reference = "ABARCAR";
        char[] expected = {'A', 'A', 'R', 'R', 'A'};
        char[] result = AvaliacaoForm.getMatchingCharArray(word, reference);
        assertArrayEquals(expected, result);
    }

    @Test
    void getMatchingCharArrayAmarrarOfTerra() {
        String word = "AMARRAR";
        String reference = "TERRA";
        char[] expected = {'A', 'R', 'R'};
        char[] result = AvaliacaoForm.getMatchingCharArray(word, reference);
        assertArrayEquals(expected, result);
    }

    @Test
    void countTranspositions() {
        char[] first = {'A', 'A', 'R', 'A', 'R'};
        char[] second = {'A', 'A', 'R', 'R', 'A'};
        int expected = 2;
        int result = AvaliacaoForm.countTranspositions(first, second);
        assertEquals(expected, result);
    }

    @Test
    void getSimilarityValueAmarrarAndAbarcar() {
        String first = "ABARCAR";
        String second = "AMARRAR";
        double expected = 0.7428571428571429;
        double result = AvaliacaoForm.getSimilarityValue(first, second);
        assertEquals(expected, result, 0.000000001);
    }

    @Test
    void getSimilarityValueAmarrarAndTerra() {
        String first = "AMARRAR";
        String second = "Terra";
        double expected = 0.565079365079365;
        double result = AvaliacaoForm.getSimilarityValue(first, second);
        assertEquals(expected, result, 0.000000001);
    }

    @Test
    void getSimilarityValueSolAndBar() {
        String first = "SOL";
        String second = "BAR";
        double expected = 0.0;
        double result = AvaliacaoForm.getSimilarityValue(first, second);
        assertEquals(expected, result, 0.000000001);
    }

    @Test
    void getSimilarityValueBarAndMar() {
        String first = "BAR";
        String second = "MAR";
        double expected = 0.7777777777777777;
        double result = AvaliacaoForm.getSimilarityValue(first, second);
        assertEquals(expected, result, 0.000000001);
    }

    @Test
    void getSimilarityValueMarAndMare() {
        String first = "Mar";
        String second = "MARÉ";
        double expected = 0.9166666666666666;
        double result = AvaliacaoForm.getSimilarityValue(first, second);
        assertEquals(expected, result, 0.000000001);
    }

    @Test
    void getSimilarityValueMareAndMaresia() {
        String first = "MARÉ";
        String second = "MARESIA";
        double expected = 0.7261904761904763;
        double result = AvaliacaoForm.getSimilarityValue(first, second);
        assertEquals(expected, result, 0.000000001);
    }

    @Test
    void getSimilarityValueMaresiaAndAbarcar() {
        String first = "MARESIA";
        String second = "ABARCAR";
        double expected = 0.6190476190476191;
        double result = AvaliacaoForm.getSimilarityValue(first, second);
        assertEquals(expected, result, 0.000000001);
    }

    @Test
    void getSimilarityValueTerraAndTerraLower() {
        String first = "terra";
        String second = "Terra";
        double expected = 1.0;
        double result = AvaliacaoForm.getSimilarityValue(first, second);
        assertEquals(expected, result, 0.000000001);
    }
}