import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class RectangleTest {

    @org.junit.jupiter.api.Test
    void getPerimeter() {
        int width = 5;
        int height = 4;
        Rectangle rect1 = new Rectangle(width, height);
        int expected = 18;
        int result = rect1.getPerimeter();
        assertEquals(expected, result);
    }

    @Test
    void createIllegalRectangle() {
        int width = -1;
        int height = 4;
        assertThrows(IllegalArgumentException.class, () -> new Rectangle(width, height));
    }
}