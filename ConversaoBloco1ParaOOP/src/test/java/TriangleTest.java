import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TriangleTest {

    @Test
    void getHyphotenuse() {
        int cathetus1 = 3;
        int cathetus2 = 4;
        Triangle triangle = new Triangle(cathetus1, cathetus2);
        double expected = 5;
        double result = triangle.getHyphotenuse();
        assertEquals(expected, result);
    }
}