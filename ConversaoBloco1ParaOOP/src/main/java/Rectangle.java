public class Rectangle {

    private int width = -1;
    private int height = -1;

    private Rectangle() {
    }

    public Rectangle(int width, int height) {
        if (!isMeasureValid(width)) {
            throw new IllegalArgumentException("Width can't have negative values.");
        }
        if (!isMeasureValid(height)) {
            throw new IllegalArgumentException("Height can't have negative values.");

        }
        this.width = width;
        this.height = height;

    }

    public int getPerimeter() {
        return 2 * width + 2 * height;
    }

    private boolean isMeasureValid(int measure) {
        return measure > 0;
    }
}
