public class Triangle {
    private int cathetus1;
    private int cathetus2;

    private Triangle() {
    }

    public Triangle(int cathetus1, int cathetus2) {
        this.cathetus1 = cathetus1;
        this.cathetus2 = cathetus2;
    }

    public double getHyphotenuse() {
        double sum = Math.pow(cathetus1, 2) + Math.pow(cathetus2, 2);
        return Math.sqrt(sum);
    }
}
