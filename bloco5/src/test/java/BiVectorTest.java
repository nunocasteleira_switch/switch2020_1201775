import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BiVectorTest {

    @Test
    void append() {
        int[][] originalBiVector = {{123, 234}, {345}};
        int[] vector2 = {543, 432, 321};
        BiVector biVector = new BiVector(originalBiVector);
        int[][] expected = {
                {123, 234},
                {345},
                {543, 432, 321}
        };

        biVector.appendVector(vector2);

        assertArrayEquals(expected, biVector.toArray());
    }

    @Test
    void appendAt() {
        int[][] originalBiVector = {{123, 234}, {345}};
        BiVector biVector = new BiVector(originalBiVector);
        int value = 543;
        int index = 1;
        int[][] expected = {
                {123, 234},
                {345, 543},
        };

        biVector.appendAt(value, index);

        assertArrayEquals(expected, biVector.toArray());
    }

    @Test
    void removeFirst() {
        int[][] originalBiVector = {
                {123, 234},
                {345},
                {543, 432, 321}
        };
        int[][] expected = {
                {123, 234},
                {345},
                {543, 321}
        };
        BiVector biVector = new BiVector(originalBiVector);
        biVector.removeFirst(432);

        assertArrayEquals(expected, biVector.toArray());
    }

    @Test
    void isNotEmpty() {
        int[][] originalBiVector = {
                {123, 234},
                {345},
                {543, 432, 321}
        };
        BiVector biVector = new BiVector(originalBiVector);

        assertFalse(biVector.isEmpty());
    }

    @Test
    void testIsEmpty() {
        int[][] originalBiVector = {};
        BiVector biVector = new BiVector(originalBiVector);

        assertTrue(biVector.isEmpty());
    }

    @Test
    void highest() {
        int[][] originalBiVector = {
                {123, 234},
                {345},
                {543, 432, 321}
        };
        BiVector biVector = new BiVector(originalBiVector);
        int expected;
        int result;

        expected = 543;
        result = biVector.highest();

        assertEquals(expected, result);
    }

    @Test
    void lowest() {
        int[][] originalBiVector = {
                {123, 234},
                {345},
                {543, 432, 321}
        };
        BiVector biVector = new BiVector(originalBiVector);
        int expected;
        int result;

        expected = 123;
        result = biVector.lowest();

        assertEquals(expected, result);
    }

    @Test
    void avg() {
        int[][] originalBiVector = {
                {123, 234},
                {345},
                {543, 432, 321}
        };
        BiVector biVector = new BiVector(originalBiVector);
        double expected;
        double result;

        expected = 333;
        result = biVector.avg();

        assertEquals(expected, result);
    }

    @Test
    void sumOfLines() {
        int[][] originalBiVector = {
                {123, 234},
                {345},
                {543, 432, 321}
        };
        BiVector biVector = new BiVector(originalBiVector);
        int[] expected = {357, 345, 1296};
        int[] result;
        result = biVector.sumOfLines();

        assertArrayEquals(expected, result);
    }

    @Test
    void sumOfColumns() {
        int[][] originalBiVector = {
                {123, 234},
                {345},
                {543, 432, 321}
        };
        BiVector biVector = new BiVector(originalBiVector);
        int[] expected = {1011, 666, 321};
        int[] result;
        result = biVector.sumOfColumns();

        assertArrayEquals(expected, result);
    }

    @Test
    void indexOfHighestSum() {
        int[][] originalBiVector = {
                {123, 234},
                {345},
                {543, 432, 321}
        };
        BiVector biVector = new BiVector(originalBiVector);
        int expected = 2;
        int result;
        result = biVector.indexOfHighestSum();

        assertEquals(expected, result);
    }

    @Test
    void isNotSquare() {
        int[][] originalBiVector = {
                {123, 234},
                {345},
                {543, 432, 321}
        };
        BiVector biVector = new BiVector(originalBiVector);

        boolean result = biVector.isSquare();

        assertFalse(result);
    }

    @Test
    void isSquare() {
        int[][] originalBiVector = {
                {123, 234, 765},
                {345, 234, 654},
                {543, 432, 321}
        };
        BiVector biVector = new BiVector(originalBiVector);

        boolean result = biVector.isSquare();

        assertTrue(result);
    }

    @Test
    void transposeSquare() {
        int[][] matrix = {
                {0, 1, 2},
                {10, 11, 12},
                {20, 21, 22}
        };
        BiVector biVector = new BiVector(matrix);
        int[][] expected = {
                {0, 10, 20},
                {1, 11, 21},
                {2, 12, 22}
        };
        biVector.transpose();
        assertArrayEquals(expected, biVector.toArray());
    }

    @Test
    void transposeRect() {
        int[][] matrix = {
                {0, 1, 2, 3},
                {10, 11, 12, 13},
                {20, 21, 22, 23}
        };
        BiVector biVector = new BiVector(matrix);
        int[][] expected = {
                {0, 10, 20},
                {1, 11, 21},
                {2, 12, 22},
                {3, 13, 23}
        };
        biVector.transpose();
        assertArrayEquals(expected, biVector.toArray());
    }

    @Test
    void isSymmetric() {
        int[][] matrix = {
                {1, 5, 9},
                {5, 3, 8},
                {9, 8, 7}
        };
        BiVector biVector = new BiVector(matrix);

        boolean result = biVector.isSymmetric();

        assertTrue(result);
    }


    @Test
    void isNotSymmetric() {
        int[][] matrix = {
                {1, 5, 9},
                {5, 3, 8},
                {7, 8, 7}
        };
        BiVector biVector = new BiVector(matrix);

        boolean result = biVector.isSymmetric();

        assertFalse(result);
    }

    @Test
    void nonNullValuesInDiag() {
        int[][] matrix = {
                {1, 5, 9},
                {5, 3, 8},
                {9, 8, 7}
        };
        BiVector biVector = new BiVector(matrix);
        int expected;
        int result;

        expected = 3;
        result = biVector.nonNullValuesInDiag();

        assertEquals(expected, result);
    }

    @Test
    void getDiagonal() {
        int[][] matrix = {
                {1, 5, 9},
                {5, 3, 8},
                {9, 8, 7}
        };
        BiVector biVector = new BiVector(matrix);
        int[] expected = {1, 3, 7};
        int[] result;

        result = biVector.getDiagonal();

        assertArrayEquals(expected, result);
    }

    @Test
    void getAntidiagonal() {
        int[][] matrix = {
                {1, 5, 9},
                {5, 3, 8},
                {9, 8, 7}
        };
        BiVector biVector = new BiVector(matrix);
        int[] expected = {9, 3, 9};
        int[] result;

        result = biVector.getAntidiagonal();

        assertArrayEquals(expected, result);
    }

    @Test
    void haveMatchingDiags() {
        int[][] matrix = {
                {1, 5, 1},
                {5, 3, 8},
                {7, 8, 7}
        };
        BiVector biVector = new BiVector(matrix);
        boolean result = biVector.haveMatchingDiags();

        assertTrue(result);
    }

    @Test
    void largerThanAvgDigitCount() {
        int[][] vector = {
                {1, 23443},
                {345},
                {53, 42, 32}
        };
        BiVector biVector = new BiVector(vector);
        int[] expected = {23443, 345};
        int[] result = biVector.largerThanAvgDigitCount();

        assertArrayEquals(expected, result);
    }


    @Test
    void largerThanAvgEvenPercentage() {
        int[][] vector = {
                {1, 23443},
                {345},
                {53, 42, 32}
        };
        BiVector biVector = new BiVector(vector);
        int[] expected = {23443, 42, 32};
        int[] result = biVector.largerThanAvgEvenPercentage();

        assertArrayEquals(expected, result);
    }

    @Test
    void invertLines() {
        int[][] vector = {
                {1, 23443},
                {345},
                {53, 42, 32}
        };
        BiVector biVector = new BiVector(vector);
        int[][] expected = {
                {23443, 1},
                {345},
                {32, 42, 53}
        };

        biVector.invertLines();

        assertArrayEquals(expected, biVector.toArray());
    }

    @Test
    void invertColumns() {
        int[][] vector = {
                {1, 23443},
                {345},
                {53, 42, 32}
        };
        BiVector biVector = new BiVector(vector);
        int[][] expected = {
                {53, 42, 32},
                {345},
                {1, 23443}
        };

        biVector.invertColumns();

        assertArrayEquals(expected, biVector.toArray());
    }

    @Test
    void rotateNinetyTaller() {
        int[][] vector = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9},
                {10, 11, 12}
        };
        BiVector biVector = new BiVector(vector);
        int[][] expected = {
                {10, 7, 4, 1},
                {11, 8, 5, 2},
                {12, 9, 6, 3}
        };

        biVector.rotateNinety();

        assertArrayEquals(expected, biVector.toArray());
    }

    @Test
    void rotateNinetyWider() {
        int[][] vector = {
                {1, 2, 3, 4},
                {4, 5, 6, 7},
                {7, 8, 9, 8}
        };
        BiVector biVector = new BiVector(vector);
        int[][] expected = {
                {7, 4, 1},
                {8, 5, 2},
                {9, 6, 3},
                {8, 7, 4}
        };

        biVector.rotateNinety();

        assertArrayEquals(expected, biVector.toArray());
    }

    @Test
    void rotateNinetySquare() {
        int[][] vector = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9},
        };
        BiVector biVector = new BiVector(vector);
        int[][] expected = {
                {7, 4, 1},
                {8, 5, 2},
                {9, 6, 3}
        };

        biVector.rotateNinety();

        assertArrayEquals(expected, biVector.toArray());
    }

    @Test
    void appendAtNull() {
        BiVector bivector = new BiVector();
        int[][] expected = {
                {1}
        };

        bivector.appendAt(1, 0);

        assertArrayEquals(expected, bivector.toArray());
    }

    @Test
    void appendAtNewLine() {
        int[][] intBivector = {
                {1}
        };
        BiVector bivector = new BiVector(intBivector);
        int[][] expected = {
                {1},
                {2}
        };

        bivector.appendAt(2, 1);

        assertArrayEquals(expected, bivector.toArray());
    }

    @Test
    void rotateHundredEightyWider() {
        int[][] vector = {
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12}
        };
        BiVector biVector = new BiVector(vector);
        int[][] expected = {
                {12, 11, 10, 9},
                {8, 7, 6, 5},
                {4, 3, 2, 1}
        };

        biVector.rotateHundredEighty();

        assertArrayEquals(expected, biVector.toArray());
    }

    @Test
    void rotateHundredEightySquare() {
        int[][] vector = {
                {1, 2, 3},
                {5, 6, 7},
                {9, 10, 11}
        };
        BiVector biVector = new BiVector(vector);
        int[][] expected = {
                {11, 10, 9},
                {7, 6, 5},
                {3, 2, 1}
        };

        biVector.rotateHundredEighty();

        assertArrayEquals(expected, biVector.toArray());
    }

    @Test
    void rotateHundredEightyTaller() {
        int[][] vector = {
                {1, 2, 3},
                {5, 6, 7},
                {9, 10, 11},
                {4, 8, 12}
        };
        BiVector biVector = new BiVector(vector);
        int[][] expected = {
                {12, 8, 4},
                {11, 10, 9},
                {7, 6, 5},
                {3, 2, 1}
        };

        biVector.rotateHundredEighty();

        assertArrayEquals(expected, biVector.toArray());
    }

    @Test
    void rotateNegNinetyWider() {
        int[][] vector = {
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12}
        };
        BiVector biVector = new BiVector(vector);
        int[][] expected = {
                {4, 8, 12},
                {3, 7, 11},
                {2, 6, 10},
                {1, 5, 9}
        };

        biVector.rotateNegNinety();

        assertArrayEquals(expected, biVector.toArray());
    }

    @Test
    void rotateNegNinetySquare() {
        int[][] vector = {
                {1, 2, 3},
                {5, 6, 7},
                {9, 10, 11}
        };
        BiVector biVector = new BiVector(vector);
        int[][] expected = {
                {3, 7, 11},
                {2, 6, 10},
                {1, 5, 9}
        };

        biVector.rotateNegNinety();

        assertArrayEquals(expected, biVector.toArray());
    }

    @Test
    void rotateNegNinetyTaller() {
        int[][] vector = {
                {1, 2, 3},
                {5, 6, 7},
                {9, 10, 11},
                {4, 8, 12}
        };
        BiVector biVector = new BiVector(vector);
        int[][] expected = {
                {3, 7, 11, 12},
                {2, 6, 10, 8},
                {1, 5, 9, 4}
        };

        biVector.rotateNegNinety();

        assertArrayEquals(expected, biVector.toArray());
    }
}