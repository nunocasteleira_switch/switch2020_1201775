package Sudoku;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SudokuGridTest {
    @Test
    void addNumber() {
        int[][] baseSudokuMatrix = new int[][]{
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        SudokuGrid testGrid = new SudokuGrid(baseSudokuMatrix);
        int line = 0;
        int column = 0;
        int number = 7;
        testGrid.addNumber(line, column, number);
        int[][] expected = new int[][]{
                {7, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        int[][] result = testGrid.toArray();
        Assertions.assertArrayEquals(expected, result);
    }

    @Test
    void tryToAddAnInvalidNumber() {
        int[][] baseSudokuMatrix = new int[][]{
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        SudokuGrid testGrid = new SudokuGrid(baseSudokuMatrix);
        int line = 0;
        int column = 0;
        int number = 17;
        testGrid.addNumber(line, column, number);
        int[][] expected = new int[][]{
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        int[][] result = testGrid.toArray();
        Assertions.assertArrayEquals(expected, result);
    }

    @Test
    void changeNumber() {
        int[][] baseSudokuMatrix = new int[][]{
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        SudokuGrid testGrid = new SudokuGrid(baseSudokuMatrix);
        int line = 0;
        int column = 0;
        int number = 7;
        int newNumber = 9;
        testGrid.addNumber(line, column, number);
        testGrid.changeNumber(line,column,newNumber);
        int[][] expected = new int[][]{
                {9, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        int[][] result = testGrid.toArray();
        Assertions.assertArrayEquals(expected, result);
    }

    @Test
    void numberIsLegalInLine() {
        int[][] baseSudokuMatrix = new int[][]{
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        SudokuGrid testGrid = new SudokuGrid(baseSudokuMatrix);
        int line = 3;
        int column = 0;
        int number = 7;
        testGrid.addNumber(line, column, number);
        int[][] expected = new int[][]{
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {7, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        int[][] result = testGrid.toArray();
        Assertions.assertArrayEquals(expected, result);
    }

    @Test
    void numberIsNotLegalInLine() {
        int[][] baseSudokuMatrix = new int[][]{
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        SudokuGrid testGrid = new SudokuGrid(baseSudokuMatrix);
        int line = 0;
        int column = 0;
        int number = 6;
        testGrid.addNumber(line, column, number);
        int[][] expected = new int[][]{
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        int[][] result = testGrid.toArray();
        Assertions.assertArrayEquals(expected, result);
    }

    @Test
    void numberIsLegalInColumn() {
        int[][] baseSudokuMatrix = new int[][]{
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        SudokuGrid testGrid = new SudokuGrid(baseSudokuMatrix);
        int line = 3;
        int column = 2;
        int number = 6;
        testGrid.addNumber(line, column, number);
        int[][] expected = new int[][]{
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 6, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        int[][] result = testGrid.toArray();
        Assertions.assertArrayEquals(expected, result);
    }

    @Test
    void numberIsNotLegalInColumn() {
        int[][] baseSudokuMatrix = new int[][]{
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        SudokuGrid testGrid = new SudokuGrid(baseSudokuMatrix);
        int line = 3;
        int column = 8;
        int number = 6;
        testGrid.addNumber(line, column, number);
        int[][] expected = new int[][]{
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        int[][] result = testGrid.toArray();
        Assertions.assertArrayEquals(expected, result);
    }

    @Test
    void numberIsLegalInSquare() {
        int[][] baseSudokuMatrix = new int[][]{
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        SudokuGrid testGrid = new SudokuGrid(baseSudokuMatrix);
        int line = 1;
        int column = 1;
        int number = 9;
        testGrid.addNumber(line, column, number);
        int[][] expected = new int[][]{
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 9, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        int[][] result = testGrid.toArray();
        Assertions.assertArrayEquals(expected, result);
    }

    @Test
    void numberIsNotLegalInSquare() {
        int[][] baseSudokuMatrix = new int[][]{
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        SudokuGrid testGrid = new SudokuGrid(baseSudokuMatrix);
        int line = 1;
        int column = 1;
        int number = 6;
        testGrid.addNumber(line, column, number);
        int[][] expected = new int[][]{
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        int[][] result = testGrid.toArray();
        Assertions.assertArrayEquals(expected, result);
    }

    @Test
    void numberIsLegalInSquareTwoOne() {
        int[][] baseSudokuMatrix = new int[][]{
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}
        };
        SudokuGrid testGrid = new SudokuGrid(baseSudokuMatrix);
        int line = 8;
        int column = 3;
        int number = 5;
        testGrid.addNumber(line, column, number);
        int[][] expected = new int[][]{
                {0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 5, 0, 1, 3, 7, 0}
        };
        int[][] result = testGrid.toArray();
        Assertions.assertArrayEquals(expected, result);
    }

    @Test
    void isSudokuMatrixVerified() {
        int[][] baseSudokuMatrix = new int[][]{
                {7, 5, 1, 3, 8, 2, 4, 9, 6},
                {6, 9, 3, 4, 7, 5, 2, 1, 8},
                {4, 8, 2, 9, 1, 6, 7, 5, 3},
                {2, 7, 6, 8, 9, 4, 5, 3, 1},
                {1, 3, 8, 5, 2, 7, 6, 4, 9},
                {9, 4, 5, 1, 6, 3, 8, 2, 7},
                {5, 1, 4, 7, 3, 8, 9, 6, 2},
                {3, 6, 7, 2, 4, 9, 1, 8, 5},
                {8, 2, 9, 6, 5, 1, 3, 7, 4}
        };
        SudokuGrid testGrid = new SudokuGrid(baseSudokuMatrix);
        boolean result = testGrid.isSudokuMatrixVerified();
        Assertions.assertTrue(result);
    }
}