import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VectorTest {

    @org.junit.jupiter.api.Test
    void append() {
        int[] intVector = {1, 2, 3};
        Vector vector = new Vector(intVector);
        int value = 4;
        int[] intExpected = {1, 2, 3, 4};
        Vector expected = new Vector(intExpected);

        vector.append(value);

        assertArrayEquals(expected.toArray(), vector.toArray());
    }

    @org.junit.jupiter.api.Test
    void removeFirstInstance() {
        int[] intVector = {1, 3, 4, 2};
        Vector vector = new Vector(intVector);
        int value = 2;
        int[] intExpected = {1, 3, 4};
        Vector expected = new Vector(intExpected);

        vector.removeFirstInstance(value);

        assertArrayEquals(expected.toArray(), vector.toArray());
    }

    @Test
    void valueOfIndex() {
        int[] intVector = {1, 2, 3, 4};
        Vector vector = new Vector(intVector);
        int index = 3;
        int expected = 4;

        int result = vector.valueOfIndex(index);

        assertEquals(expected, result);
    }

    @Test
    void positionOfFirst() {
        int[] intVector = {1, 2, 3, 4};
        Vector vector = new Vector(intVector);
        int value = 2;
        int expected = 1;

        int result = vector.indexOfFirst(value);

        assertEquals(expected, result);
    }

    @Test
    void countElements() {
        int[] intVector = {1, 2, 3, 4};
        Vector vector = new Vector(intVector);
        int expected = 4;

        int result = vector.countElements();

        assertEquals(expected, result);
    }

    @Test
    void highest() {
        int[] intVector = {1, 2, 3, 4};
        Vector vector = new Vector(intVector);
        int expected = 4;

        int result = vector.highest();

        assertEquals(expected, result);
    }

    @Test
    void lowest() {
        int[] intVector = {1, 2, 3, 4};
        Vector vector = new Vector(intVector);
        int expected = 1;

        int result = vector.lowest();

        assertEquals(expected, result);
    }

    @Test
    void avg() {
        int[] intVector = {1, 2, 3, 4};
        Vector vector = new Vector(intVector);
        double expected = 2.5;

        double result = vector.avg();

        assertEquals(expected, result, 0.01);
    }

    @Test
    void avgEven() {
        int[] intVector = {1, 2, 3, 4};
        Vector vector = new Vector(intVector);
        double expected = 3;

        double result = vector.avgEven();

        assertEquals(expected, result, 0.01);
    }

    @Test
    void avgOdd() {
        int[] intVector = {1, 2, 3, 4};
        Vector vector = new Vector(intVector);
        double expected = 2;

        double result = vector.avgOdd();

        assertEquals(expected, result, 0.01);
    }

    @Test
    void avgMultiplex() {
        int[] intVector = {1, 2, 3, 4, 5, 6};
        Vector vector = new Vector(intVector);
        int multiplex = 3;
        double expected = 4.5;

        double result = vector.avgMultiplex(multiplex);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void ascending() {
        int[] intVector = {1, 6, 3, 2, 4, 5};
        Vector vector = new Vector(intVector);
        int[] intExpected = {1, 2, 3, 4, 5, 6};
        Vector expected = new Vector(intExpected);

        vector.ascending();

        assertArrayEquals(expected.toArray(), vector.toArray());
    }

    @Test
    void descending() {
        int[] intVector = {1, 6, 3, 2, 4, 5};
        Vector vector = new Vector(intVector);
        int[] intExpected = {6, 5, 4, 3, 2, 1};
        Vector expected = new Vector(intExpected);

        vector.descending();

        assertArrayEquals(expected.toArray(), vector.toArray());
    }


    @Test
    void isNull() {
        Vector vector = null;
        assertNull(vector);
    }

    @Test
    void hasOnlyOneElement() {
        Vector vector = new Vector();
        vector.append(1);
        boolean result;

        result = vector.hasOnlyOneElement();

        assertTrue(result);
    }

    @Test
    void hasOnlyEvenElements() {
        int[] intVector = {6, 2, 4};
        Vector vector = new Vector(intVector);
        boolean result;

        result = vector.hasOnlyEvenElements();

        assertTrue(result);
    }

    @Test
    void hasOnlyOddElements() {
        int[] intVector = {-1, 3, -7};
        Vector vector = new Vector(intVector);
        boolean result;

        result = vector.hasOnlyOddElements();

        assertTrue(result);
    }

    @Test
    void hasDuplicates() {
        int[] intVector = {1, 2, 3, 1, 5};
        Vector vector = new Vector(intVector);
        boolean result;

        result = vector.hasDuplicates();

        assertTrue(result);
    }

    @Test
    void hasNotDuplicates() {
        int[] intVector = {1, 2, 3, 5};
        Vector vector = new Vector(intVector);
        boolean result;

        result = vector.hasDuplicates();

        assertFalse(result);
    }

    @Test
    void digitsLargerThanAvg() {
        int[] intVector = {123, 21, 3674, 54};
        Vector vector = new Vector(intVector);
        Vector expected = new Vector();
        expected.append(3674);

        Vector result = vector.digitsLargerThanAvg();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getEvenPercentage() {
        int[] intVector = {123, 21, 3674, 54};
        Vector vector = new Vector(intVector);
        double expected;
        double result;

        expected = 0.458333;
        result = vector.getEvenPercentage();

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void getSecondEvenPercentage() {
        int[] intVector = {126, 213, 36746, 542};
        Vector vector = new Vector(intVector);
        double expected;
        double result;

        expected = 0.5666666667;
        result = vector.getEvenPercentage();

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void getFullyEvenElements() {
        int[] intVector = {264, 213, 36746, 542};
        Vector vector = new Vector(intVector);
        int[] intExpected = {264};
        Vector expected = new Vector(intExpected);
        Vector result;

        result = vector.getFullyEvenElements();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void increasingSequences() {
        int[] intVector = {246, 347, 36746, 542};
        Vector vector = new Vector(intVector);
        int[] intExpected = {246, 347};
        Vector expected = new Vector(intExpected);
        Vector result;

        result = vector.increasingSequences();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void palindromes() {
        int[] intVector = {246, 347, 36763, 542};
        Vector vector = new Vector(intVector);
        int[] intExpected = {36763};
        Vector expected = new Vector(intExpected);
        Vector result;

        result = vector.palindromes();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void sameDigits() {
        int[] intVector = {246, 222, 36763, 542};
        Vector vector = new Vector(intVector);
        int[] intExpected = {222};
        Vector expected = new Vector(intExpected);
        Vector result;

        result = vector.sameDigits();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void armstrongNumbers() {
        int[] intVector = {246, 153, 36763, 542};
        Vector vector = new Vector(intVector);
        int[] intExpected = {153};
        Vector expected = new Vector(intExpected);
        Vector result;

        result = vector.armstrongNumbers();

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void increasingSequencesOfSize() {
        int[] intVector = {246, 153, 36763, 542};
        Vector vector = new Vector(intVector);
        int[] intExpected = {246, 36763};
        Vector expected = new Vector(intExpected);
        Vector result;

        result = vector.increasingSequencesOfSize(3);

        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void isMatching() {
        int[] intVector = {246, 153, 36763, 542};
        Vector vector = new Vector(intVector);
        int[] intOther = {246, 153, 36763, 542};
        boolean result;

        result = vector.isMatching(intOther);

        assertTrue(result);
    }

    @Test
    void isNotMatching() {
        int[] intVector = {246, 153, 36763, 542};
        Vector vector = new Vector(intVector);
        int[] intOther = {246, 36763, 542};
        boolean result;

        result = vector.isMatching(intOther);

        assertFalse(result);
    }

    @Test
    void invert() {
        int[] intVector = {246, 153, 36763, 542};
        Vector vector = new Vector(intVector);
        int[] expected = {542, 36763, 153, 246};

        vector.invert();

        assertArrayEquals(expected, vector.toArray());
    }

    @Test
    void sumValueToIndexInExistingIndex() {
        int[] intVector = {246, 153, 36763, 542};
        Vector result = new Vector(intVector);
        int value = 2;
        int index = 3;
        int[] expected = {246, 153, 36763, 544};

        result.sumValueToIndex(value, index);

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void sumValueToIndexInNewIndex() {
        int[] intVector = {246, 153, 36763, 542};
        Vector result = new Vector(intVector);
        int value = 2;
        int index = 4;
        int[] expected = {246, 153, 36763, 542, 2};

        result.sumValueToIndex(value, index);

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void sumValueToIndexInNewFartherIndex() {
        int[] intVector = {246, 153, 36763, 542};
        Vector result = new Vector(intVector);
        int value = 2;
        int index = 5;
        int[] expected = {246, 153, 36763, 542, 0, 2};

        result.sumValueToIndex(value, index);

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void countDigitsOfEveryElement() {
        int[] intVector = {246, 153, 36763, 542};
        Vector vector = new Vector(intVector);
        int expected = 14;
        int result = vector.countDigitsOfEveryElement();

        assertEquals(expected, result);
    }

    @Test
    void join() {
        int[] intVector = {246, 153, 36763, 542};
        Vector result = new Vector(intVector);
        int[] joinVector = {543, 432};
        Vector join = new Vector(joinVector);
        int[] expected = {246, 153, 36763, 542, 543, 432};

        result.join(join);

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void joinIfNull() {
        Vector result = new Vector();
        int[] joinVector = {543, 432};
        Vector join = new Vector(joinVector);
        int[] expected = {543, 432};

        result.join(join);

        assertArrayEquals(expected, result.toArray());
    }
}