import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MatrixTest {

    @Test
    void createMatrixByValue() {
        Vector[] matrixVector = new Vector[1];
        Vector arr = new Vector();
        arr.append(1);
        matrixVector[0] = arr;
        int[][] expected = {{1}};
        Matrix result = new Matrix(matrixVector);

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void createEmptyMatrixAndAppend() {
        Matrix result = new Matrix();
        Vector vector = new Vector();
        vector.append(12);
        int[][] expected = {{12}};

        result.appendVector(vector);

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void createMatrixByArrayOfInts() {
        int[][] arrayOfInts = {
                {1, 2, 3},
                {2, 3, 4},
                {3, 4, 5}
        };
        Matrix result = new Matrix(arrayOfInts);

        assertArrayEquals(arrayOfInts, result.toArray());
    }

    @Test
    void appendVector() {
        int[][] arrayOfInts = {
                {1, 2, 3},
        };
        Matrix result = new Matrix(arrayOfInts);
        int[] arr = {2, 3, 4};
        Vector vector = new Vector(arr);
        result.appendVector(vector);
        int[][] expected = {
                {1, 2, 3},
                {2, 3, 4}
        };

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void appendVectorToLargerMatrix() {
        int[][] arrayOfInts = {
                {1, 2, 3},
                {2, 3, 4}
        };
        Matrix result = new Matrix(arrayOfInts);
        int[] arr = {3, 4, 5};
        Vector vector = new Vector(arr);
        result.appendVector(vector);
        int[][] expected = {
                {1, 2, 3},
                {2, 3, 4},
                {3, 4, 5}
        };

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void appendVectorToEmptyMatrix() {
        int[][] arrayOfInts = {
        };
        Matrix result = new Matrix(arrayOfInts);
        int[] arr = {1, 2, 3};
        Vector vector = new Vector(arr);
        result.appendVector(vector);
        int[][] expected = {
                {1, 2, 3},
        };

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void copy() {
        int[][] arrayOfInts = {
                {1, 2, 3},
                {2, 3, 4},
                {3, 4, 5}
        };
        Matrix expected = new Matrix(arrayOfInts);
        Matrix result = expected.copy();

        assertNotSame(expected, result);
    }

    @Test
    void appendAtIfLineExists() {
        int[][] arrayOfInts = {
                {1, 2, 3},
                {2, 3, 4},
                {3, 4, 5}
        };
        Matrix result = new Matrix(arrayOfInts);
        int value = 6;
        int index = 2;
        int[][] expected = {
                {1, 2, 3},
                {2, 3, 4},
                {3, 4, 5, 6}
        };

        result.appendAt(value, index);

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void appendAtNewLine() {
        int[][] arrayOfInts = {
                {1, 2, 3},
                {2, 3, 4},
                {3, 4, 5}
        };
        Matrix result = new Matrix(arrayOfInts);
        int value = 6;
        int index = 3;
        int[][] expected = {
                {1, 2, 3},
                {2, 3, 4},
                {3, 4, 5},
                {6}
        };

        result.appendAt(value, index);

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void appendAtNewDistantLine() {
        int[][] arrayOfInts = {
                {1, 2, 3},
                {2, 3, 4},
                {3, 4, 5}
        };
        Matrix result = new Matrix(arrayOfInts);
        int value = 6;
        int index = 5;
        int[][] expected = {
                {1, 2, 3},
                {2, 3, 4},
                {3, 4, 5},
                {0},
                {0},
                {6}
        };

        result.appendAt(value, index);

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void appendAtIfNull() {
        Matrix result = new Matrix();
        int value = 6;
        int index = 5;
        int[][] expected = {
                {0},
                {0},
                {0},
                {0},
                {0},
                {6}
        };

        result.appendAt(value, index);

        assertArrayEquals(expected, result.toArray());
    }

    /*@Test
    void firstIndexWithValue() {
        int[][] arrayOfInts = {
                {1, 2, 3},
                {2, 3, 4},
                {3, 4, 5}
        };
        Matrix matrix = new Matrix(arrayOfInts);
        int value = 4;
        int expected = 1;

        int result = matrix.firstIndexWithValue(value);
        assertEquals(expected, result);
    }*/

    @Test
    void removeFirst() {
        int[][] arrayOfInts = {
                {1, 2, 3},
                {2, 3, 4},
                {3, 4, 5}
        };
        Matrix result = new Matrix(arrayOfInts);
        int value = 4;
        int[][] expected = {
                {1, 2, 3},
                {2, 3},
                {3, 4, 5}
        };

        result.removeFirst(value);
        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void highest() {
        int[][] arrayOfInts = {
                {123, 234},
                {345},
                {543, 432, 321}
        };
        Matrix matrix = new Matrix(arrayOfInts);
        int expected;
        int result;

        expected = 543;
        result = matrix.highest();

        assertEquals(expected, result);
    }

    @Test
    void lowest() {
        int[][] arrayOfInts = {
                {123, 234},
                {345},
                {543, 432, 321}
        };
        Matrix matrix = new Matrix(arrayOfInts);
        int expected;
        int result;

        expected = 123;
        result = matrix.lowest();

        assertEquals(expected, result);
    }

    @Test
    void avg() {
        int[][] originalBiVector = {
                {123, 234},
                {345},
                {543, 432, 321}
        };
        Matrix matrix = new Matrix(originalBiVector);
        double expected;
        double result;

        expected = 333;
        result = matrix.avg();

        assertEquals(expected, result);
    }

    @Test
    void sumOfLines() {
        int[][] originalBiVector = {
                {123, 234},
                {345},
                {543, 432, 321}
        };
        Matrix matrix = new Matrix(originalBiVector);
        int[] expected = {357, 345, 1296};
        int[] result;
        result = matrix.sumOfLines().toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void sumOfColumns() {
        int[][] originalBiVector = {
                {123, 234},
                {345},
                {543, 432, 321}
        };
        Matrix matrix = new Matrix(originalBiVector);
        int[] expected = {1011, 666, 321};
        Vector result;
        result = matrix.sumOfColumns();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void indexOfHighestSum() {
        int[][] originalBiVector = {
                {123, 234},
                {345},
                {543, 432, 321}
        };
        Matrix matrix = new Matrix(originalBiVector);
        int expected = 2;
        int result;
        result = matrix.indexOfHighestSum();

        assertEquals(expected, result);
    }

    @Test
    void isNotSquare() {
        int[][] originalBiVector = {
                {123, 234},
                {345},
                {543, 432, 321}
        };
        Matrix matrix = new Matrix(originalBiVector);

        boolean result = matrix.isSquare();

        assertFalse(result);
    }

    @Test
    void isSquare() {
        int[][] originalBiVector = {
                {123, 234, 765},
                {345, 234, 654},
                {543, 432, 321}
        };
        Matrix matrix = new Matrix(originalBiVector);

        boolean result = matrix.isSquare();

        assertTrue(result);
    }

    @Test
    void isNotRect() {
        int[][] originalBiVector = {
                {123, 234, 765},
                {345, 234},
                {543, 432, 321},
        };
        Matrix matrix = new Matrix(originalBiVector);

        boolean result = matrix.isRect();

        assertFalse(result);
    }

    @Test
    void isRectTaller() {
        int[][] originalBiVector = {
                {123, 234, 765},
                {345, 234, 654},
                {543, 432, 321},
                {543, 432, 321}
        };
        Matrix matrix = new Matrix(originalBiVector);

        boolean result = matrix.isRect();

        assertTrue(result);
    }

    @Test
    void isRectWider() {
        int[][] originalBiVector = {
                {123, 234, 765, 2345, 34},
                {345, 234, 654, 2345, 34},
                {543, 432, 321, 2345, 34},
                {543, 432, 321, 2345, 34}
        };
        Matrix matrix = new Matrix(originalBiVector);

        boolean result = matrix.isRect();

        assertTrue(result);
    }

    @Test
    void transposeSquare() {
        int[][] originalBiVector = {
                {0, 1, 2},
                {10, 11, 12},
                {20, 21, 22}
        };
        Matrix matrix = new Matrix(originalBiVector);
        int[][] expected = {
                {0, 10, 20},
                {1, 11, 21},
                {2, 12, 22}
        };
        matrix.transpose();
        assertArrayEquals(expected, matrix.toArray());
    }

    @Test
    void transposeRect() {
        int[][] originalBiVector = {
                {0, 1, 2, 3},
                {10, 11, 12, 13},
                {20, 21, 22, 23}
        };
        Matrix matrix = new Matrix(originalBiVector);
        int[][] expected = {
                {0, 10, 20},
                {1, 11, 21},
                {2, 12, 22},
                {3, 13, 23}
        };
        matrix.transpose();
        assertArrayEquals(expected, matrix.toArray());
    }


    @Test
    void isSymmetric() {
        int[][] originalBiVector = {
                {1, 5, 9},
                {5, 3, 8},
                {9, 8, 7}
        };
        Matrix matrix = new Matrix(originalBiVector);

        boolean result = matrix.isSymmetric();

        assertTrue(result);
    }

    @Test
    void isNotSymmetric() {
        int[][] originalBiVector = {
                {1, 5, 9},
                {5, 3, 8},
                {7, 8, 7}
        };
        Matrix matrix = new Matrix(originalBiVector);

        boolean result = matrix.isSymmetric();

        assertFalse(result);
    }

    @Test
    void getDiagonal() {
        int[][] originalBiVector = {
                {1, 5, 9},
                {5, 3, 8},
                {9, 8, 7}
        };
        Matrix matrix = new Matrix(originalBiVector);
        int[] expected = {1, 3, 7};
        Vector result;

        result = matrix.getDiagonal();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void getAntidiagonal() {
        int[][] originalBiVector = {
                {1, 5, 9},
                {5, 3, 8},
                {9, 8, 7}
        };
        Matrix matrix = new Matrix(originalBiVector);
        int[] expected = {9, 3, 9};
        Vector result;

        result = matrix.getAntidiagonal();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void nonNullValuesInDiag() {
        int[][] originalBiVector = {
                {1, 5, 9},
                {5, 3, 8},
                {9, 8, 7}
        };
        Matrix matrix = new Matrix(originalBiVector);
        int expected;
        int result;

        expected = 3;
        result = matrix.nonNullValuesInDiag();

        assertEquals(expected, result);
    }

    @Test
    void haveMatchingDiags() {
        int[][] originalBiVector = {
                {1, 5, 1},
                {5, 3, 8},
                {7, 8, 7}
        };
        Matrix matrix = new Matrix(originalBiVector);
        boolean result = matrix.haveMatchingDiags();

        assertTrue(result);
    }

    @Test
    void largerThanAvgDigitCount() {
        int[][] vector = {
                {1, 23443},
                {345},
                {53, 42, 32}
        };
        Matrix matrix = new Matrix(vector);
        int[] expected = {23443, 345};
        Vector result = matrix.largerThanAvgDigitCount();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void largerThanAvgEvenPercentage() {
        int[][] vector = {
                {1, 23443},
                {345},
                {53, 42, 32}
        };
        Matrix matrix = new Matrix(vector);
        int[] expected = {23443, 42, 32};
        Vector result = matrix.largerThanAvgEvenPercentage();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void invertLines() {
        int[][] vector = {
                {1, 23443},
                {345},
                {53, 42, 32}
        };
        Matrix matrix = new Matrix(vector);
        int[][] expected = {
                {23443, 1},
                {345},
                {32, 42, 53}
        };

        matrix.invertLines();

        assertArrayEquals(expected, matrix.toArray());
    }

    @Test
    void invertColumns() {
        int[][] vector = {
                {1, 23443},
                {345},
                {53, 42, 32}
        };
        Matrix matrix = new Matrix(vector);
        int[][] expected = {
                {53, 42, 32},
                {345},
                {1, 23443}
        };

        matrix.invertColumns();

        assertArrayEquals(expected, matrix.toArray());
    }

    @Test
    void rotateNinetyTaller() {
        int[][] vector = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9},
                {10, 11, 12}
        };
        Matrix matrix = new Matrix(vector);
        int[][] expected = {
                {10, 7, 4, 1},
                {11, 8, 5, 2},
                {12, 9, 6, 3}
        };

        matrix.rotateNinety();

        assertArrayEquals(expected, matrix.toArray());
    }

    @Test
    void rotateNinetyWider() {
        int[][] vector = {
                {1, 2, 3, 4},
                {4, 5, 6, 7},
                {7, 8, 9, 8}
        };
        Matrix matrix = new Matrix(vector);
        int[][] expected = {
                {7, 4, 1},
                {8, 5, 2},
                {9, 6, 3},
                {8, 7, 4}
        };

        matrix.rotateNinety();

        assertArrayEquals(expected, matrix.toArray());
    }

    @Test
    void rotateNinetySquare() {
        int[][] vector = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9},
        };
        Matrix matrix = new Matrix(vector);
        int[][] expected = {
                {7, 4, 1},
                {8, 5, 2},
                {9, 6, 3}
        };

        matrix.rotateNinety();

        assertArrayEquals(expected, matrix.toArray());
    }

    @Test
    void rotateHundredEightyWider() {
        int[][] vector = {
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12}
        };
        Matrix matrix = new Matrix(vector);
        int[][] expected = {
                {12, 11, 10, 9},
                {8, 7, 6, 5},
                {4, 3, 2, 1}
        };

        matrix.rotateHundredEighty();

        assertArrayEquals(expected, matrix.toArray());
    }

    @Test
    void rotateHundredEightySquare() {
        int[][] vector = {
                {1, 2, 3},
                {5, 6, 7},
                {9, 10, 11}
        };
        Matrix matrix = new Matrix(vector);
        int[][] expected = {
                {11, 10, 9},
                {7, 6, 5},
                {3, 2, 1}
        };

        matrix.rotateHundredEighty();

        assertArrayEquals(expected, matrix.toArray());
    }

    @Test
    void rotateHundredEightyTaller() {
        int[][] vector = {
                {1, 2, 3},
                {5, 6, 7},
                {9, 10, 11},
                {4, 8, 12}
        };
        Matrix matrix = new Matrix(vector);
        int[][] expected = {
                {12, 8, 4},
                {11, 10, 9},
                {7, 6, 5},
                {3, 2, 1}
        };

        matrix.rotateHundredEighty();

        assertArrayEquals(expected, matrix.toArray());
    }

    @Test
    void rotateNegNinetyWider() {
        int[][] vector = {
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12}
        };
        Matrix matrix = new Matrix(vector);
        int[][] expected = {
                {4, 8, 12},
                {3, 7, 11},
                {2, 6, 10},
                {1, 5, 9}
        };

        matrix.rotateNegNinety();

        assertArrayEquals(expected, matrix.toArray());
    }

    @Test
    void rotateNegNinetySquare() {
        int[][] vector = {
                {1, 2, 3},
                {5, 6, 7},
                {9, 10, 11}
        };
        Matrix matrix = new Matrix(vector);
        int[][] expected = {
                {3, 7, 11},
                {2, 6, 10},
                {1, 5, 9}
        };

        matrix.rotateNegNinety();

        assertArrayEquals(expected, matrix.toArray());
    }

    @Test
    void rotateNegNinetyTaller() {
        int[][] vector = {
                {1, 2, 3},
                {5, 6, 7},
                {9, 10, 11},
                {4, 8, 12}
        };
        Matrix matrix = new Matrix(vector);
        int[][] expected = {
                {3, 7, 11, 12},
                {2, 6, 10, 8},
                {1, 5, 9, 4}
        };

        matrix.rotateNegNinety();

        assertArrayEquals(expected, matrix.toArray());
    }
}