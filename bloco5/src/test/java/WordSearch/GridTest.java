package WordSearch;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class GridTest {

    @Test
    void getMaskMatrix() {
        char[][] baseMatrix = {
                {'X', 'S', 'I', 'M', 'A', 'N', 'U', 'S', 'T', 'B'},
                {'Z', 'T', 'E', 'C', 'T', 'O', 'N', 'I', 'C', 'A'},
                {'J', 'J', 'S', 'O', 'M', 'S', 'I', 'S', 'O', 'G'},
                {'O', 'T', 'O', 'M', 'E', 'R', 'A', 'M', 'R', 'E'},
                {'R', 'V', 'I', 'I', 'U', 'C', 'R', 'O', 'T', 'D'},
                {'T', 'A', 'H', 'L', 'A', 'F', 'E', 'G', 'N', 'U'},
                {'N', 'C', 'B', 'L', 'R', 'L', 'T', 'R', 'E', 'T'},
                {'E', 'I', 'P', 'A', 'G', 'J', 'H', 'A', 'C', 'I'},
                {'C', 'L', 'S', 'C', 'L', 'J', 'C', 'F', 'O', 'N'},
                {'I', 'P', 'O', 'R', 'X', 'O', 'I', 'O', 'P', 'G'},
                {'P', 'E', 'L', 'E', 'N', 'E', 'R', 'G', 'I', 'A'},
                {'E', 'R', 'O', 'M', 'E', 'R', 'T', 'H', 'H', 'M'}
        };
        Grid base = new Grid(baseMatrix);
        char searchLetter = 'A';
        int[][] expected = {
                {0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 1, 0, 0, 1, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 1, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
        };

        int[][] result = base.getMaskMatrix(searchLetter);
        assertArrayEquals(expected, result);
    }

    @Test
    void getDirectionZero() {
        char[][] baseMatrix = {
                {'X', 'S', 'I', 'M', 'A', 'N', 'U', 'S', 'T', 'B'},
                {'Z', 'T', 'E', 'C', 'T', 'O', 'N', 'I', 'C', 'A'},
                {'J', 'J', 'S', 'O', 'M', 'S', 'I', 'S', 'O', 'G'},
                {'O', 'T', 'O', 'M', 'E', 'R', 'A', 'M', 'R', 'E'},
                {'R', 'V', 'I', 'I', 'U', 'C', 'R', 'O', 'T', 'D'},
                {'T', 'A', 'H', 'L', 'A', 'F', 'E', 'G', 'N', 'U'},
                {'N', 'C', 'B', 'L', 'R', 'L', 'T', 'R', 'E', 'T'},
                {'E', 'I', 'P', 'A', 'G', 'J', 'H', 'A', 'C', 'I'},
                {'C', 'L', 'S', 'C', 'L', 'J', 'C', 'F', 'O', 'N'},
                {'I', 'P', 'O', 'R', 'X', 'O', 'I', 'O', 'P', 'G'},
                {'P', 'E', 'L', 'E', 'N', 'E', 'R', 'G', 'I', 'A'},
                {'E', 'R', 'O', 'M', 'E', 'R', 'T', 'H', 'H', 'M'}
        };
        //Direction 0
        int[] coord1Zero = {1, 1};
        int[] coord2Zero = {1, 9};
        String expectedWordInDirectionZero = "TECTONICA";
        Grid base = new Grid(baseMatrix);
        String resultWordInDirectionZero = base.getWordFromWordSearch(coord1Zero, coord2Zero);
        assertEquals(expectedWordInDirectionZero, resultWordInDirectionZero);
    }

    @Test
    void getDirectionOne() {
        char[][] baseMatrix = {
                {'X', 'S', 'I', 'M', 'A', 'N', 'U', 'S', 'T', 'B'},
                {'Z', 'T', 'E', 'C', 'T', 'O', 'N', 'I', 'C', 'A'},
                {'J', 'J', 'S', 'O', 'M', 'S', 'I', 'S', 'O', 'G'},
                {'O', 'T', 'O', 'M', 'E', 'R', 'A', 'M', 'R', 'E'},
                {'R', 'V', 'I', 'I', 'U', 'C', 'R', 'O', 'T', 'D'},
                {'T', 'A', 'H', 'L', 'A', 'F', 'E', 'G', 'N', 'U'},
                {'N', 'C', 'B', 'L', 'R', 'L', 'T', 'R', 'E', 'T'},
                {'E', 'I', 'P', 'A', 'G', 'J', 'H', 'A', 'C', 'I'},
                {'C', 'L', 'S', 'C', 'L', 'J', 'C', 'F', 'O', 'N'},
                {'I', 'P', 'O', 'R', 'X', 'O', 'I', 'O', 'P', 'G'},
                {'P', 'E', 'L', 'E', 'N', 'E', 'R', 'G', 'I', 'A'},
                {'E', 'R', 'O', 'M', 'E', 'R', 'T', 'H', 'H', 'M'}
        };
        //Direction 1
        int[] coord1One = {2, 7};
        int[] coord2One = {2, 2};
        String expectedWordInDirectionOne = "SISMOS";
        Grid base = new Grid(baseMatrix);
        String resultWordInDirectionOne = base.getWordFromWordSearch(coord1One, coord2One);
        assertEquals(expectedWordInDirectionOne, resultWordInDirectionOne);
    }

    @Test
    void getDirectionTwo() {
        char[][] baseMatrix = {
                {'X', 'S', 'I', 'M', 'A', 'N', 'U', 'S', 'T', 'B'},
                {'Z', 'T', 'E', 'C', 'T', 'O', 'N', 'I', 'C', 'A'},
                {'J', 'J', 'S', 'O', 'M', 'S', 'I', 'S', 'O', 'G'},
                {'O', 'T', 'O', 'M', 'E', 'R', 'A', 'M', 'R', 'E'},
                {'R', 'V', 'I', 'I', 'U', 'C', 'R', 'O', 'T', 'D'},
                {'T', 'A', 'H', 'L', 'A', 'F', 'E', 'G', 'N', 'U'},
                {'N', 'C', 'B', 'L', 'R', 'L', 'T', 'R', 'E', 'T'},
                {'E', 'I', 'P', 'A', 'G', 'J', 'H', 'A', 'C', 'I'},
                {'C', 'L', 'S', 'C', 'L', 'J', 'C', 'F', 'O', 'N'},
                {'I', 'P', 'O', 'R', 'X', 'O', 'I', 'O', 'P', 'G'},
                {'P', 'E', 'L', 'E', 'N', 'E', 'R', 'G', 'I', 'A'},
                {'E', 'R', 'O', 'M', 'E', 'R', 'T', 'H', 'H', 'M'}
        };
        //Direction 2
        int[] coord1Two = {0, 7};
        int[] coord2Two = {9, 7};
        String expectedWordInDirectionTwo = "SISMOGRAFO";
        Grid base = new Grid(baseMatrix);
        String resultWordInDirectionTwo = base.getWordFromWordSearch(coord1Two, coord2Two);
        assertEquals(expectedWordInDirectionTwo, resultWordInDirectionTwo);
    }

    @Test
    void getDirectionThree() {
        char[][] baseMatrix = {
                {'X', 'S', 'I', 'M', 'A', 'N', 'U', 'S', 'T', 'B'},
                {'Z', 'T', 'E', 'C', 'T', 'O', 'N', 'I', 'C', 'A'},
                {'J', 'J', 'S', 'O', 'M', 'S', 'I', 'S', 'O', 'G'},
                {'O', 'T', 'O', 'M', 'E', 'R', 'A', 'M', 'R', 'E'},
                {'R', 'V', 'I', 'I', 'U', 'C', 'R', 'O', 'T', 'D'},
                {'T', 'A', 'H', 'L', 'A', 'F', 'E', 'G', 'N', 'U'},
                {'N', 'C', 'B', 'L', 'R', 'L', 'T', 'R', 'E', 'T'},
                {'E', 'I', 'P', 'A', 'G', 'J', 'H', 'A', 'C', 'I'},
                {'C', 'L', 'S', 'C', 'L', 'J', 'C', 'F', 'O', 'N'},
                {'I', 'P', 'O', 'R', 'X', 'O', 'I', 'O', 'P', 'G'},
                {'P', 'E', 'L', 'E', 'N', 'E', 'R', 'G', 'I', 'A'},
                {'E', 'R', 'O', 'M', 'E', 'R', 'T', 'H', 'H', 'M'}
        };
        //Direction 3
        int[] coord1Three = {11, 9};
        int[] coord2Three = {3, 9};
        String expectedWordInDirectionThree = "MAGNITUDE";
        Grid base = new Grid(baseMatrix);
        String resultWordInDirectionThree = base.getWordFromWordSearch(coord1Three, coord2Three);
        assertEquals(expectedWordInDirectionThree, resultWordInDirectionThree);
    }

    @Test
    void getDirectionFour() {
        char[][] baseMatrix = {
                {'X', 'S', 'I', 'M', 'A', 'N', 'U', 'S', 'T', 'B'},
                {'Z', 'T', 'E', 'C', 'T', 'O', 'N', 'I', 'C', 'A'},
                {'J', 'J', 'S', 'O', 'M', 'S', 'I', 'S', 'O', 'G'},
                {'O', 'T', 'O', 'M', 'E', 'R', 'A', 'M', 'R', 'E'},
                {'R', 'V', 'I', 'I', 'U', 'C', 'R', 'O', 'T', 'D'},
                {'T', 'A', 'H', 'L', 'A', 'F', 'E', 'G', 'N', 'U'},
                {'N', 'C', 'B', 'L', 'R', 'L', 'T', 'R', 'E', 'T'},
                {'E', 'I', 'P', 'A', 'G', 'J', 'H', 'A', 'C', 'I'},
                {'C', 'L', 'S', 'C', 'L', 'J', 'C', 'F', 'O', 'N'},
                {'I', 'P', 'O', 'R', 'X', 'O', 'I', 'O', 'P', 'G'},
                {'P', 'E', 'L', 'E', 'N', 'E', 'R', 'G', 'I', 'A'},
                {'E', 'R', 'O', 'M', 'E', 'R', 'T', 'H', 'H', 'M'}
        };
        //Direction 4
        int[] coord1Four = {5, 1};
        int[] coord2Four = {9, 5};
        String expectedWordInDirectionFour = "ABALO";
        Grid base = new Grid(baseMatrix);
        String resultWordInDirectionFour = base.getWordFromWordSearch(coord1Four, coord2Four);
        assertEquals(expectedWordInDirectionFour, resultWordInDirectionFour);
    }

    @Test
    void getDirectionFive() {
        char[][] baseMatrix = {
                {'X', 'S', 'I', 'M', 'A', 'N', 'U', 'S', 'T', 'B'},
                {'Z', 'T', 'E', 'C', 'T', 'O', 'N', 'I', 'C', 'A'},
                {'J', 'J', 'S', 'O', 'M', 'S', 'I', 'S', 'O', 'G'},
                {'O', 'T', 'O', 'M', 'E', 'R', 'A', 'M', 'R', 'E'},
                {'R', 'V', 'I', 'I', 'U', 'C', 'R', 'O', 'T', 'D'},
                {'T', 'A', 'H', 'L', 'A', 'F', 'E', 'G', 'N', 'U'},
                {'N', 'C', 'B', 'L', 'R', 'L', 'T', 'R', 'E', 'T'},
                {'E', 'I', 'P', 'A', 'G', 'J', 'H', 'A', 'C', 'I'},
                {'C', 'L', 'S', 'C', 'L', 'J', 'C', 'F', 'O', 'N'},
                {'I', 'P', 'O', 'R', 'X', 'O', 'I', 'O', 'P', 'G'},
                {'P', 'E', 'L', 'E', 'N', 'E', 'R', 'G', 'I', 'A'},
                {'E', 'R', 'O', 'M', 'E', 'R', 'T', 'H', 'H', 'M'}
        };
        //Direction 5
        int[] coord1Five = {2, 2};
        int[] coord2Five = {0, 0};
        String expectedWordInDirectionFive = "STX";
        Grid base = new Grid(baseMatrix);
        String resultWordInDirectionFive = base.getWordFromWordSearch(coord1Five, coord2Five);
        assertEquals(expectedWordInDirectionFive, resultWordInDirectionFive);
    }

    @Test
    void getDirectionSix() {
        char[][] baseMatrix = {
                {'X', 'S', 'I', 'M', 'A', 'N', 'U', 'S', 'T', 'B'},
                {'Z', 'T', 'E', 'C', 'T', 'O', 'N', 'I', 'C', 'A'},
                {'J', 'J', 'S', 'O', 'M', 'S', 'I', 'S', 'O', 'G'},
                {'O', 'T', 'O', 'M', 'E', 'R', 'A', 'M', 'R', 'E'},
                {'R', 'V', 'I', 'I', 'U', 'C', 'R', 'O', 'T', 'D'},
                {'T', 'A', 'H', 'L', 'A', 'F', 'E', 'G', 'N', 'U'},
                {'N', 'C', 'B', 'L', 'R', 'L', 'T', 'R', 'E', 'T'},
                {'E', 'I', 'P', 'A', 'G', 'J', 'H', 'A', 'C', 'I'},
                {'C', 'L', 'S', 'C', 'L', 'J', 'C', 'F', 'O', 'N'},
                {'I', 'P', 'O', 'R', 'X', 'O', 'I', 'O', 'P', 'G'},
                {'P', 'E', 'L', 'E', 'N', 'E', 'R', 'G', 'I', 'A'},
                {'E', 'R', 'O', 'M', 'E', 'R', 'T', 'H', 'H', 'M'}
        };
        //Direction 6
        int[] coord1Six = {0, 9};
        int[] coord2Six = {2, 7};
        String expectedWordInDirectionSix = "BCS";
        Grid base = new Grid(baseMatrix);
        String resultWordInDirectionSix = base.getWordFromWordSearch(coord1Six, coord2Six);
        assertEquals(expectedWordInDirectionSix, resultWordInDirectionSix);
    }

    @Test
    void getDirectionSeven() {
        char[][] baseMatrix = {
                {'X', 'S', 'I', 'M', 'A', 'N', 'U', 'S', 'T', 'B'},
                {'Z', 'T', 'E', 'C', 'T', 'O', 'N', 'I', 'C', 'A'},
                {'J', 'J', 'S', 'O', 'M', 'S', 'I', 'S', 'O', 'G'},
                {'O', 'T', 'O', 'M', 'E', 'R', 'A', 'M', 'R', 'E'},
                {'R', 'V', 'I', 'I', 'U', 'C', 'R', 'O', 'T', 'D'},
                {'T', 'A', 'H', 'L', 'A', 'F', 'E', 'G', 'N', 'U'},
                {'N', 'C', 'B', 'L', 'R', 'L', 'T', 'R', 'E', 'T'},
                {'E', 'I', 'P', 'A', 'G', 'J', 'H', 'A', 'C', 'I'},
                {'C', 'L', 'S', 'C', 'L', 'J', 'C', 'F', 'O', 'N'},
                {'I', 'P', 'O', 'R', 'X', 'O', 'I', 'O', 'P', 'G'},
                {'P', 'E', 'L', 'E', 'N', 'E', 'R', 'G', 'I', 'A'},
                {'E', 'R', 'O', 'M', 'E', 'R', 'T', 'H', 'H', 'M'}
        };
        //Direction 7
        int[] coord1Seven = {7, 2};
        int[] coord2Seven = {2, 7};
        String expectedWordInDirectionSeven = "PLACAS";
        Grid base = new Grid(baseMatrix);
        String resultWordInDirectionSeven = base.getWordFromWordSearch(coord1Seven, coord2Seven);
        assertEquals(expectedWordInDirectionSeven, resultWordInDirectionSeven);
    }
}