package WordSearch;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SolutionsTest {

    @Test
    void validateWord() {
        String[] solutionsArr = {"ABALO", "ENERGIA", "EPICENTRO", "FALHA", "GRAU", "HIPOCENTRO", "MAGNITUDE", "MAREMOTO",
                "MERCALLI", "PLACAS", "REPLICA", "RICHTER", "SISMOGRAFO", "SISMOS", "SOLO", "TECTONICA",
                "TREMOR", "TSUNAMI"};
        Solutions solutions = new Solutions(solutionsArr);
        String candidateWord = "SISMOS";
        boolean result = solutions.validateWord(candidateWord);

        assertTrue(result);
    }

    @Test
    void doNotValidateWord() {
        String[] solutionsArr = {"ABALO", "ENERGIA", "EPICENTRO", "FALHA", "GRAU", "HIPOCENTRO", "MAGNITUDE", "MAREMOTO",
                "MERCALLI", "PLACAS", "REPLICA", "RICHTER", "SISMOGRAFO", "SISMOS", "SOLO", "TECTONICA",
                "TREMOR", "TSUNAMI"};
        Solutions solutions = new Solutions(solutionsArr);
        String candidateWord = "BATATA";
        boolean result = solutions.validateWord(candidateWord);

        assertFalse(result);
    }

    @Test
    void removeValidWord() {
        String[] solutionsArr = {"ABALO", "ENERGIA", "EPICENTRO", "FALHA", "GRAU", "HIPOCENTRO", "MAGNITUDE", "MAREMOTO",
                "MERCALLI", "PLACAS", "REPLICA", "RICHTER", "SISMOGRAFO", "SISMOS", "SOLO", "TECTONICA",
                "TREMOR", "TSUNAMI"};
        Solutions result = new Solutions(solutionsArr);
        String foundWord = "SISMOS";
        String[] expectedArr = {"ABALO", "ENERGIA", "EPICENTRO", "FALHA", "GRAU", "HIPOCENTRO", "MAGNITUDE", "MAREMOTO",
                "MERCALLI", "PLACAS", "REPLICA", "RICHTER", "SISMOGRAFO", "SOLO", "TECTONICA",
                "TREMOR", "TSUNAMI"};
        Solutions expected = new Solutions(expectedArr);
        result.removeWord(foundWord);

        assertEquals(expected, result);
    }

    @Test
    void dontRemoveInalidWord() {
        String[] solutionsArr = {"ABALO", "ENERGIA", "EPICENTRO", "FALHA", "GRAU", "HIPOCENTRO", "MAGNITUDE", "MAREMOTO",
                "MERCALLI", "PLACAS", "REPLICA", "RICHTER", "SISMOGRAFO", "SISMOS", "SOLO", "TECTONICA",
                "TREMOR", "TSUNAMI"};
        Solutions result = new Solutions(solutionsArr);
        String foundWord = "BATATAS";
        String[] expectedArr = {"ABALO", "ENERGIA", "EPICENTRO", "FALHA", "GRAU", "HIPOCENTRO", "MAGNITUDE", "MAREMOTO",
                "MERCALLI", "PLACAS", "REPLICA", "RICHTER", "SISMOGRAFO", "SISMOS", "SOLO", "TECTONICA",
                "TREMOR", "TSUNAMI"};
        Solutions expected = new Solutions(expectedArr);
        result.removeWord(foundWord);

        assertEquals(expected, result);
    }
}