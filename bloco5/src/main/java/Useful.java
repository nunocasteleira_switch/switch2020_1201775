public class Useful {

    public static int countDigits(int value) {
        int count = 0;
        while (value > 0) {
            value /= 10;
            count++;
        }
        return count;
    }

    public static int countEvenDigits(int value) {
        int count = 0;
        while (value > 0) {
            if ((value % 10) % 2 == 0) {
                count++;
            }
            value /= 10;
        }
        return count;
    }
}
