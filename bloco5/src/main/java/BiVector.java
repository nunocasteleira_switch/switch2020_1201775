public class BiVector {
    private int[][] biVector;

    public BiVector() {

    }

    public BiVector(int[][] initialBiVector) {
        if (initialBiVector != null) {
            this.biVector = new int[initialBiVector.length][];
            for (int i = 0; i < initialBiVector.length; i++) {
                this.biVector[i] = new int[initialBiVector[i].length];
                for (int j = 0; j < initialBiVector[i].length; j++) {
                    this.biVector[i][j] = initialBiVector[i][j];
                }
            }
        }
    }

    public int[][] toArray() {
        int[][] newArr = new int[this.biVector.length][];
        for (int i = 0; i < this.biVector.length; i++) {
            newArr[i] = new int[this.biVector[i].length];
            for (int j = 0; j < this.biVector[i].length; j++) {
                newArr[i][j] = this.biVector[i][j];
            }
        }
        return newArr;
    }

    public boolean isNull() {
        return this.biVector == null;
    }

    public void appendVector(int[] vector) {
        if (isNull()) {
            int[][] newBiVector = new int[1][vector.length];
            for (int i = 0; i < vector.length; i++) {
                newBiVector[0][i] = vector[i];
            }
            this.biVector = newBiVector;
        } else {
            int[][] newBiVector = new int[this.biVector.length + 1][];
            for (int i = 0; i < this.biVector.length; i++) {
                newBiVector[i] = new int[this.biVector[i].length];
                for (int j = 0; j < this.biVector[i].length; j++) {
                    newBiVector[i][j] = this.biVector[i][j];
                }
            }
            newBiVector[this.biVector.length] = new int[vector.length];
            for (int i = 0; i < vector.length; i++) {
                newBiVector[this.biVector.length][i] = vector[i];
            }
            this.biVector = newBiVector;
        }
    }

    private void appendAtIfNull(int value) {
        int[][] newBiVector = new int[1][1];
        newBiVector[0][0] = value;
        this.biVector = newBiVector;
    }

    private void appendAtIfLineExists(int value, int index) {
        BiVector newBiVector = new BiVector();
        //int[][] newBiVector = new int[this.biVector.length][];
        for (int i = 0; i < this.biVector.length; i++) {
            Vector arr = new Vector();
            for (int j = 0; j < this.biVector[i].length; j++) {
                arr.append(getValue(i, j));
            }
            if (i == index) {
                arr.append(value);
            }
            newBiVector.appendVector(arr.toArray());
        }
        this.biVector = newBiVector.toArray();
    }

    private void appendAtIfNewLine(int value) {
        BiVector newBiVector = new BiVector();
        for (int i = 0; i < this.biVector.length; i++) {
            Vector arr = new Vector();
            for (int j = 0; j < this.biVector[i].length; j++) {
                arr.append(getValue(i, j));
            }
            newBiVector.appendVector(arr.toArray());
        }
        Vector newline = new Vector();
        newline.append(value);
        newBiVector.appendVector(newline.toArray());
        this.biVector = newBiVector.toArray();
    }

    public void appendAt(int value, int index) {
        if (isNull()) {
            appendAtIfNull(value);
        } else if (index < this.biVector.length) {
            appendAtIfLineExists(value, index);
        } else {
            appendAtIfNewLine(value);
        }
    }

    /*
    public void appendAt(int value, int index) {
        if (isNull()) {
            int[][] newBiVector = new int[1][1];
            newBiVector[0][0] = value;
            this.biVector = newBiVector;
        } else {
            int[][] newBiVector = new int[this.biVector.length][];
            for (int i = 0; i < this.biVector.length; i++) {
                if (i != index) {
                    newBiVector[i] = new int[this.biVector[i].length];
                    for (int j = 0; j < this.biVector[i].length; j++) {
                        newBiVector[i][j] = this.biVector[i][j];
                    }
                } else {
                    newBiVector[i] = new int[this.biVector[i].length + 1];
                    for (int j = 0; j < this.biVector[i].length; j++) {
                        newBiVector[i][j] = this.biVector[i][j];
                    }
                    newBiVector[i][newBiVector[i].length - 1] = value;
                }
            }
            this.biVector = newBiVector;
        }
    }*/

    private int indexWithValue(int value) {
        int i = -1;
        for (i = 0; i < this.biVector.length; i++) {
            for (int j = 0; j < this.biVector[i].length; j++) {
                if (this.biVector[i][j] == value) {
                    return i;
                }
            }
        }
        return i;
    }

    public void removeFirst(int value) {
        int index = this.indexWithValue(value);
        int[][] newBiVector;
        if (index >= 0) {
            newBiVector = new int[this.biVector.length][];
            for (int i = 0; i < this.biVector.length; i++) {
                if (i != index) {
                    newBiVector[i] = new int[this.biVector[i].length];
                    for (int j = 0; j < this.biVector[i].length; j++) {
                        newBiVector[i][j] = this.biVector[i][j];
                    }
                } else {
                    newBiVector[i] = new int[this.biVector[i].length - 1];
                    for (int j = 0, count = 0; j < this.biVector[i].length; j++) {
                        if (this.biVector[i][j] != value) {
                            newBiVector[i][count] = this.biVector[i][j];
                            count++;
                        }
                    }
                }
            }
            this.biVector = newBiVector;
        }
    }

    public boolean isEmpty() {
        boolean isEmpty = true;
        for (int i = 0; i < this.biVector.length && isEmpty; i++) {
            if (this.biVector[i] != null) {
                isEmpty = false;
            }
        }
        return isEmpty;
    }

    public void checkIfValid() {
        if (isNull()) {
            throw new NullPointerException("Vector is null.");
        }
        if (isEmpty()) {
            throw new IllegalStateException("Vector is empty.");
        }
    }

    public int highest() {
        int highest = this.biVector[0][0];
        for (int[] arr :
                this.biVector) {
            for (int value :
                    arr) {
                if (highest < value) {
                    highest = value;
                }
            }
        }
        return highest;
    }

    public int lowest() {
        int lowest = this.biVector[0][0];
        for (int[] arr :
                this.biVector) {
            for (int value :
                    arr) {
                if (lowest > value) {
                    lowest = value;
                }
            }
        }
        return lowest;
    }

    public double avg() {
        int sum = 0;
        int count = 0;
        for (int[] arr :
                this.biVector) {
            for (int value :
                    arr) {
                sum += value;
                count++;
            }
        }
        return (double) sum / count;
    }

    public int[] sumOfLines() {
        int[] sum = new int[this.biVector.length];
        for (int i = 0; i < this.biVector.length; i++) {
            for (int j = 0; j < this.biVector[i].length; j++) {
                sum[i] += this.biVector[i][j];
            }
        }
        return sum;
    }

    public int[] sumOfColumns() {
        int highestColumn = 0;
        for (int i = 0; i < this.biVector.length; i++) {
            if (highestColumn < this.biVector[i].length) {
                highestColumn = this.biVector[i].length;
            }
        }
        int[] sum = new int[highestColumn];
        for (int i = 0; i < this.biVector.length; i++) {
            for (int j = 0; j < this.biVector[i].length; j++) {
                //note the j, as we want sum of columns.
                sum[j] += this.biVector[i][j];
            }
        }
        return sum;
    }

    public int indexOfHighestSum() {
        int[] highestVector = this.sumOfLines();
        int i = 0;
        int highest = highestVector[i];
        for (int j = 0; j < highestVector.length; j++) {
            if (highest < highestVector[j]) {
                highest = highestVector[j];
                i = j;
            }
        }
        return i;
    }

    public boolean isSquare() {
        if (!isValid()) return false;
        boolean isSquare = true;
        for (int i = 0; i < this.biVector.length && isSquare; i++) {
            isSquare = this.biVector[i].length == this.biVector.length;
        }
        return isSquare;
    }

    public Integer getCommonLengthNumber() {
        if (!isValid()) return null;
        int rowLength = 0;
        for (int i = 0; i < this.biVector.length; i++) {
            int rowLengthNew;
            if (i == 0) {
                rowLength = this.biVector[i].length;
            } else {
                rowLengthNew = this.biVector[i].length;
                if (rowLength != rowLengthNew) {
                    rowLength = -1;
                }
            }
        }
        return rowLength;
    }

    public boolean isRect() {
        if (!isValid()) return false;
        boolean isRect = false;
        int rectLength = this.getCommonLengthNumber();
        if (rectLength != -1) {
            for (int[] array : this.biVector) {
                isRect = array.length != this.biVector.length;
            }
        }
        return isRect;
    }

    private int[][] transposeTaller() {
        int height = this.biVector.length;
        int width = this.biVector[0].length;
        int[][] transposedTaller = new int[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                transposedTaller[j][i] = this.biVector[i][j];
            }
        }
        return transposedTaller;
    }

    private int[][] transposeWider() {
        int height = this.biVector.length;
        int width = this.biVector[0].length;
        int[][] transposedWider = new int[width][height];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                transposedWider[j][i] = this.biVector[i][j];
            }
        }
        return transposedWider;
    }

    public void transpose() {
        if (this.isSquare() || this.isRect() && isValid()) {
            int height = this.biVector.length;
            int width = this.biVector[0].length;
            if (height > width) {
                this.biVector = this.transposeTaller();
            } else {
                this.biVector = this.transposeWider();
            }
        }
    }

    public boolean isSymmetric() {
        BiVector transposed = new BiVector(this.toArray());
        transposed.transpose();
        //int[][] matrixTransposed = transposed.toArray();
        boolean isSymmetric = true;
        for (int i = 0; i < this.biVector.length && isSymmetric; i++) {
            for (int j = 0; j < this.biVector[i].length && isSymmetric; j++) {
                isSymmetric = transposed.getValue(i, j) == this.biVector[i][j];
            }
        }
        return isSymmetric;
    }

    private int getValue(int i, int j) {
        if (i < 0 || j < 0 || i > this.biVector.length || j > this.biVector[i].length) {
            throw new ArrayIndexOutOfBoundsException("Array Out of Bounds");
        }
        return this.biVector[i][j];
    }

    private boolean isValid() {
        return this.biVector != null && this.biVector.length != 0;
    }

    public int[] getDiagonal() {
        int[] diagonal = new int[this.biVector.length];
        for (int i = 0; i < diagonal.length; i++) {
            diagonal[i] = this.getValue(i, i);
        }
        return diagonal;
    }

    public int[] getAntidiagonal() {
        int[] antidiagonal = new int[this.biVector.length];
        for (int i = 0, j = antidiagonal.length - 1; i < antidiagonal.length; i++, j--) {
            antidiagonal[i] = this.getValue(i, j);
        }
        return antidiagonal;
    }

    public int nonNullValuesInDiag() {
        if (this.isSquare()) {
            int count = 0;
            int[] diagonal = getDiagonal();
            for (int i = 0; i < diagonal.length; i++) {
                if (diagonal[i] != 0) {
                    count++;
                }
            }
            return count;
        } else {
            return -1;
        }
    }

    public boolean haveMatchingDiags() {
        int[] diagonal = getDiagonal();
        int[] antidiagonal = getAntidiagonal();
        boolean matches = true;
        if (diagonal.length == antidiagonal.length) {
            for (int i = 0; i < diagonal.length && matches; i++) {
                matches = diagonal[i] == antidiagonal[i];
            }
        }
        return matches;
    }

    private int avgDigitCount() {
        int sumDigitCount = 0;
        int valueCount = 0;
        int digitAvg;
        for (int[] arr : this.biVector) {
            for (int value : arr) {
                sumDigitCount += Useful.countDigits(value);
                valueCount++;
            }
        }
        digitAvg = sumDigitCount / valueCount;
        return digitAvg;
    }

    public int[] largerThanAvgDigitCount() {
        int digitAvg = this.avgDigitCount();
        Vector largerThanAvg = new Vector();
        for (int i = 0; i < this.biVector.length; i++) {
            for (int j = 0; j < this.biVector[i].length; j++) {
                if (Useful.countDigits(getValue(i, j)) > digitAvg) {
                    largerThanAvg.append(getValue(i, j));
                }
            }
        }
        return largerThanAvg.toArray();
    }

    private double evenDigitPercentage() {
        int sumEvenDigitCount = 0;
        int totalDigitCount = 0;
        double evenPercentage;
        for (int[] arr : this.biVector) {
            for (int value : arr) {
                sumEvenDigitCount += Useful.countEvenDigits(value);
                totalDigitCount += Useful.countDigits(value);
            }
        }
        evenPercentage = (double) sumEvenDigitCount / totalDigitCount;
        return evenPercentage;
    }

    public int[] largerThanAvgEvenPercentage() {
        double avgPercentage = this.evenDigitPercentage();
        Vector largerThanAvg = new Vector();
        for (int i = 0; i < this.biVector.length; i++) {
            for (int j = 0; j < this.biVector[i].length; j++) {
                double evenPercentage = (double) Useful.countEvenDigits(getValue(i, j)) / Useful.countDigits(getValue(i, j));
                if (evenPercentage > avgPercentage) {
                    largerThanAvg.append(getValue(i, j));
                }
            }
        }
        return largerThanAvg.toArray();
    }

    public void invertLines() {
        BiVector invertedLines = new BiVector();
        for (int i = 0; i < this.biVector.length; i++) {
            Vector arr = new Vector(this.biVector[i]);
            arr.invert();
            invertedLines.appendVector(arr.toArray());
        }
        this.biVector = invertedLines.toArray();
    }

    public void invertColumns() {
        BiVector invertedColumns = new BiVector();
        for (int i = this.biVector.length - 1; i >= 0; i--) {
            Vector arr = new Vector(this.biVector[i]);
            invertedColumns.appendVector(arr.toArray());
        }
        this.biVector = invertedColumns.toArray();
    }

    public void rotateNinety() {
        if (isSquare() || isRect()) {
            BiVector rotatedNinety = new BiVector();
            for (int i = this.biVector.length - 1; i >= 0; i--) {
                for (int j = 0; j < this.biVector[i].length; j++) {
                    rotatedNinety.appendAt(this.biVector[i][j], j);
                }
            }
            this.biVector = rotatedNinety.toArray();
        }
    }

    public void rotateHundredEighty() {
        if (isSquare() || isRect()) {
            BiVector rotatedHundredEighty = new BiVector();
            for (int i = this.biVector.length - 1, k = 0; i >= 0; i--, k++) {
                for (int j = this.biVector[i].length - 1; j >= 0; j--) {
                    rotatedHundredEighty.appendAt(this.biVector[i][j], k);
                }
            }
            this.biVector = rotatedHundredEighty.toArray();
        }
    }

    public void rotateNegNinety() {
        if (isSquare() || isRect()) {
            BiVector rotatedNegNinety = new BiVector();
            for (int i = 0; i < this.biVector.length; i++) {
                for (int j = this.biVector[i].length - 1, k = 0; j >= 0; j--, k++) {
                    rotatedNegNinety.appendAt(this.biVector[i][j], k);
                }
            }
            this.biVector = rotatedNegNinety.toArray();
        }
    }

}
