package WordSearch;

public class Grid {
    private char[][] puzzle;
    private Solutions solutions;
    //private final int LINE_INDEX = 0;
    //private final int COLUMN_INDEX = 1;

    public Grid(char[][] puzzle) {
        if (puzzle != null && puzzle.length > 0 && verifyEqualityOfLinesLength(puzzle)) {
            char[][] copyPuzzle = new char[puzzle.length][puzzle[0].length];
            for (int i = 0; i < puzzle.length; i++) {
                System.arraycopy(puzzle[i], 0, copyPuzzle[i], 0, puzzle[i].length);
            }
            this.puzzle = copyPuzzle;
        }
    }

    private static int getCommonLengthNumber(char[][] puzzle) {
        int rowLength = 0;
        for (int i = 0; i < puzzle.length; i++) {
            int rowLengthNew;
            if (i == 0) {
                rowLength = puzzle[i].length;
            } else {
                rowLengthNew = puzzle[i].length;
                if (rowLength != rowLengthNew) {
                    rowLength = -1;
                }
            }
        }
        return rowLength;
    }

    private static boolean verifyEqualityOfLinesLength(char[][] puzzle) {
        int rectLength = getCommonLengthNumber(puzzle);
        return rectLength != -1;
    }

    public int[][] getMaskMatrix(char searchLetter) {
        int[][] maskMatrix = new int[puzzle.length][puzzle[0].length];
        if (Character.isLowerCase(searchLetter)) {
            searchLetter = Character.toUpperCase(searchLetter);
        }
        for (int i = 0; i < puzzle.length; i++) {
            for (int j = 0; j < puzzle[0].length; j++) {
                if (searchLetter == puzzle[i][j]) {
                    maskMatrix[i][j] = 1;
                }
            }
        }
        return maskMatrix;
    }

    private static int getDirection(int[] firstCoord, int[] secondCoord) {
        /*0. esquerda > direita;
          1. direita > esquerda;
          2. cima > baixo;
          3. baixo > cima;
          4. diagonal > descendente esq-dir;
          5. diagonal > ascendente dir-esq;
          6. diagonal > descendente dir-esq;
          7. diagonal > ascendente esq-dir;*/
        int direction = 0;
        if (firstCoord[1] < secondCoord[1] && firstCoord[0] == secondCoord[0]) {
            //noinspection ConstantConditions
            direction = 0;
        }
        if (firstCoord[1] > secondCoord[1] && firstCoord[0] == secondCoord[0]) {
            direction = 1;
        }
        if (firstCoord[0] < secondCoord[0] && firstCoord[1] == secondCoord[1]) {
            direction = 2;
        }
        if (firstCoord[0] > secondCoord[0] && firstCoord[1] == secondCoord[1]) {
            direction = 3;
        }
        if (firstCoord[0] < secondCoord[0] && firstCoord[1] < secondCoord[1]) {
            direction = 4;
        }
        if (firstCoord[0] > secondCoord[0] && firstCoord[1] > secondCoord[1]) {
            direction = 5;
        }
        if (firstCoord[0] < secondCoord[0] && firstCoord[1] > secondCoord[1]) {
            direction = 6;
        }
        if (firstCoord[0] > secondCoord[0] && firstCoord[1] < secondCoord[1]) {
            direction = 7;
        }
        return direction;
    }

    public String getWordFromWordSearch(int[] firstCoord, int[] secondCoord) {
        int direction = getDirection(firstCoord, secondCoord);

        StringBuilder foundWord = new StringBuilder();
        switch (direction) {
            case 0:
            default:
                for (int i = firstCoord[1]; i <= secondCoord[1]; i++) {
                    foundWord.append(this.puzzle[firstCoord[0]][i]);
                }
                break;
            case 1:
                for (int i = firstCoord[1]; i >= secondCoord[1]; i--) {
                    foundWord.append(this.puzzle[firstCoord[0]][i]);
                }
                break;
            case 2:
                for (int i = firstCoord[0]; i <= secondCoord[0]; i++) {
                    foundWord.append(this.puzzle[i][firstCoord[1]]);
                }
                break;
            case 3:
                for (int i = firstCoord[0]; i >= secondCoord[0]; i--) {
                    foundWord.append(this.puzzle[i][firstCoord[1]]);
                }
                break;
            case 4:
                for (int i = firstCoord[0], j = firstCoord[1]; i <= secondCoord[0]; i++, j++) {
                    foundWord.append(this.puzzle[i][j]);
                }
                break;
            case 5:
                for (int i = firstCoord[0], j = firstCoord[1]; i >= secondCoord[0]; i--, j--) {
                    foundWord.append(this.puzzle[i][j]);
                }
                break;
            case 6:
                for (int i = firstCoord[0], j = firstCoord[1]; i <= secondCoord[0]; i++, j--) {
                    foundWord.append(this.puzzle[i][j]);
                }
                break;
            case 7:
                for (int i = firstCoord[0], j = firstCoord[1]; i >= secondCoord[0]; i--, j++) {
                    foundWord.append(this.puzzle[i][j]);
                }
                break;
        }
        return foundWord.toString();
    }
}
