package WordSearch;

import java.util.Arrays;

public class Solutions {
    private String[] solutions;

    public Solutions(String[] solutions) {
        if (solutions != null && solutions.length > 0) {
            String[] copySolutions = new String[solutions.length];
            System.arraycopy(solutions, 0, copySolutions, 0, solutions.length);
            this.solutions = copySolutions;
        }
    }

    public boolean validateWord(String candidateWord) {
        boolean isWord = false;
        for (int i = 0; i < this.solutions.length && !isWord; i++) {
            isWord = candidateWord.equals(this.solutions[i]);
        }
        return isWord;
    }

    public void removeWord(String foundWord) {
        int counter = 0;
        String[] trimmedSolutions;
        if (this.validateWord(foundWord)) {
            int lengthOfTrimmedSolutions = this.solutions.length - 1;
            trimmedSolutions = new String[lengthOfTrimmedSolutions];
            for (String solution : solutions) {
                if (!solution.equals(foundWord)) {
                    trimmedSolutions[counter] = solution;
                    counter++;
                }
            }
        } else {
            int lengthOfSolutions = this.solutions.length;
            trimmedSolutions = new String[lengthOfSolutions];
            for (String solution : solutions) {
                trimmedSolutions[counter] = solution;
                counter++;
            }
        }
        this.solutions = trimmedSolutions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Solutions other = (Solutions) o;
        return Arrays.equals(solutions, other.solutions);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(solutions);
    }
}
