package Sudoku;

public class SudokuGrid {
    private final int gridSize = 9;
    private final int sectorSize = 3;
    private final int emptyCellNumber = 0;
    private final SudokuCell[][] gameGrid = new SudokuCell[9][9];

    public SudokuGrid(int[][] grid) {
        this.fillGrid(grid);
    }

    private void fillGrid(int[][] grid) {
        for (int i = 0; i < gridSize; ++i) {
            for (int j = 0; j < gridSize; ++j) {
                if (grid[i][j] != emptyCellNumber) {
                    this.gameGrid[i][j] = new SudokuCell(grid[i][j]);
                } else {
                    this.gameGrid[i][j] = new SudokuCell();
                }
            }
        }
    }

    public void addNumber(int line, int column, int value){
        SudokuCell cell = this.gameGrid[line][column];
        if (this.isMoveValid(line, column, value) && !cell.isFixed()) {
            cell.addNumber(value);
        }
    }

    public void changeNumber(int line, int column, int value) {
        SudokuCell cell = this.gameGrid[line][column];
        if (this.isMoveValid(line, column, value) && !cell.isFixed()) {
            cell.changeNumber(value);
        }
    }

    public int[][] toArray() {
        int[][] newArray = new int[gridSize][gridSize];

        for (int i = 0; i < 9; ++i) {
            for (int j = 0; j < 9; ++j) {
                newArray[i][j] = this.gameGrid[i][j].getNumber();
            }
        }
        return newArray;
    }

    private boolean verifyLine(int line, int value) {
        boolean isValid = true;

        for (int i = 0; i < gridSize && isValid; ++i) {
            isValid = this.gameGrid[line][i].getNumber() != value;
        }
        return isValid;
    }

    private boolean verifyColumn(int column, int value) {
        boolean isValid = true;

        for (int i = 0; i < gridSize && isValid; ++i) {
            isValid = this.gameGrid[i][column].getNumber() != value;
        }
        return isValid;
    }

    private int getSectorFirstLine(int line) {
        return line / sectorSize * sectorSize;
    }

    private int getSectorFirstColumn(int column) {
        return column / sectorSize * sectorSize;
    }

    private boolean verifySector(int line, int column, int value) {
        boolean isValid = true;
        int sectorFirstCell = this.getSectorFirstLine(line);
        int sectorFirstColumn = this.getSectorFirstColumn(column);

        for (int i = sectorFirstCell; i < sectorSize; ++i) {
            for (int j = sectorFirstColumn; j < sectorSize; ++j) {
                isValid = this.gameGrid[i][j].getNumber() != value;
            }
        }
        return isValid;
    }

    private boolean isMoveValid(int line, int column, int value) {
        boolean isMoveValid = this.verifyColumn(column, value) && this.verifyLine(line, value) && this.verifySector(line, column, value);
        return isMoveValid;
    }

    private boolean areEmptyCellsOnSudokuMatrix() {
        for (int i = 0; i < gridSize; ++i) {
            for (int j = 0; j < gridSize; ++j) {
                if (this.gameGrid[i][j].getNumber() == emptyCellNumber) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isSudokuMatrixVerified() {
        int FACTORIAL_NINE = 362880;
        boolean isSudokuVerified = true;
        int productOfLine = 1;

        for (int i = 0; i < gridSize && isSudokuVerified; ++i) {
            for (int j = 0; j < gridSize; ++j) {
                productOfLine *= this.gameGrid[i][j].getNumber();
            }

            isSudokuVerified = productOfLine == 362880;
            productOfLine = 1;
        }

        return isSudokuVerified;
    }
}