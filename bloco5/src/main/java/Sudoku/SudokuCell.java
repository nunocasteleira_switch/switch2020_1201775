package Sudoku;

public class SudokuCell {
    private final boolean fixed;
    private int number = 0;

    public SudokuCell(int number) {
        if (isValidNumber(number)) {
            this.number = number;
        }
        this.fixed = true;
    }

    public SudokuCell() {
        this.fixed = false;
    }

    public boolean isFixed() {
        return this.fixed;
    }

    public void addNumber(int number) {
        if (!this.fixed && isValidNumber(number)) {
            this.number = number;
        }
    }

    public void changeNumber(int number) {
        if (!this.fixed && this.isValidNumber(number)) {
            this.number = number;
        }
    }

    public int getNumber() {
        return this.number;
    }

    private boolean isValidNumber(int number) {
        return number > 0 && number <= 9;
    }
}