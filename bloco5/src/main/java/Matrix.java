public class Matrix {
    private Vector[] matrix;

    public Matrix() {

    }

    public Matrix(Vector[] initialMatrix) {
        if (initialMatrix == null) {
            throw new IllegalStateException("Matrix can't be null");
        }
        Vector[] newMatrix = new Vector[initialMatrix.length];
        System.arraycopy(initialMatrix, 0, newMatrix, 0, initialMatrix.length);
        this.matrix = newMatrix;
    }

    public Matrix(int[][] initialArray) {
        Vector[] newVector = new Vector[initialArray.length];
        for (int i = 0; i < initialArray.length; i++) {
            Vector arr = new Vector();
            for (int j = 0; j < initialArray[i].length; j++) {
                arr.append(initialArray[i][j]);
            }
            newVector[i] = arr;
        }
        this.matrix = newVector;
    }

    public Matrix copy() {
        Matrix newMatrix = new Matrix();
        for (Vector vector : this.matrix) {
            Vector arr = new Vector();
            for (int i = 0; i < vector.length(); i++) {
                arr.append(vector.valueOfIndex(i));
            }
            newMatrix.appendVector(vector);
        }
        return newMatrix;
    }

    public int length() {
        int count = 0;
        for (Vector ignored :
                this.matrix) {
            count++;
        }
        return count;
    }

    public Vector atIndex(int index) {
        Vector copy;
        copy = this.matrix[index].copy();
        return copy;
    }

    public int[][] toArray() {
        int[][] arrayCopy = new int[this.length()][];
        for (int i = 0; i < this.length(); i++) {
            Vector newArray;
            newArray = this.atIndex(i);
            arrayCopy[i] = new int[newArray.length()];
            for (int j = 0; j < newArray.length(); j++) {
                arrayCopy[i][j] = newArray.valueOfIndex(j);
            }
        }
        return arrayCopy;
    }

    public Vector[] toVector() {
        Vector[] newVector = new Vector[this.matrix.length];
        for (int i = 0; i < this.matrix.length; i++) {
            newVector[i] = this.atIndex(i);
        }
        return newVector;
    }

    public boolean isNull() {
        return this.matrix == null;
    }

    public void appendVector(Vector vector) {
        if (isNull()) {
            Vector[] newBiVector = new Vector[1];
            newBiVector[0] = vector.copy();
            this.matrix = newBiVector;
        } else {
            Vector[] newBiVector = new Vector[this.matrix.length + 1];
            for (int i = 0; i < this.matrix.length; i++) {
                Vector arr = new Vector();
                for (int j = 0; j < this.matrix[i].length(); j++) {
                    arr.append(this.matrix[i].valueOfIndex(j));
                }
                newBiVector[i] = arr;
            }
            newBiVector[this.matrix.length] = vector;
            this.matrix = newBiVector;
        }
    }

    private void appendAtIfNull(int value, int index) {
        Matrix newMatrix = new Matrix();
        for (int i = 0; i <= index; i++) {
            Vector newline = new Vector();
            if (i == index) {
                newline.append(value);
            } else {
                newline.append(0);
            }
            newMatrix.appendVector(newline);
        }
        this.matrix = newMatrix.toVector();
    }

    private void appendAtIfLineExists(int value, int index) {
        Matrix newMatrix = new Matrix();
        for (int i = 0; i < this.matrix.length; i++) {
            Vector vector;
            vector = this.atIndex(i);
            if (i == index) {
                vector.append(value);
            }
            newMatrix.appendVector(vector);
        }
        this.matrix = newMatrix.toVector();
    }

    private void appendAtIfNewLine(int value, int index) {
        Matrix newMatrix;
        newMatrix = this.copy();
        for (int i = this.matrix.length; i <= index; i++) {
            Vector newline = new Vector();
            if (i == index) {
                newline.append(value);
            } else {
                newline.append(0);
            }
            newMatrix.appendVector(newline);
        }
        this.matrix = newMatrix.toVector();
    }

    public void appendAt(int value, int index) {
        if (isNull()) {
            appendAtIfNull(value, index);
        } else if (index < this.matrix.length) {
            appendAtIfLineExists(value, index);
        } else {
            appendAtIfNewLine(value, index);
        }
    }

    private int firstIndexWithValue(int value) {
        int index = -1;
        for (int i = 0; i < this.matrix.length && index < 0; i++) {
            Vector vector = this.atIndex(i);
            for (int j = 0; j < vector.length(); j++) {
                if (vector.valueOfIndex(j) == value) {
                    index = i;
                }
            }
        }
        return index;
    }

    public void removeFirst(int value) {
        int index = this.firstIndexWithValue(value);
        Matrix newMatrix = new Matrix();
        if (index >= 0) {
            for (int i = 0; i < this.matrix.length; i++) {
                Vector vector;
                vector = this.copy().atIndex(i);
                if (i == index) {
                    vector.removeFirstInstance(value);
                }
                newMatrix.appendVector(vector);
            }
        }
        this.matrix = newMatrix.toVector();
    }

    public int highest() {
        Vector highestOfEachVec = new Vector();
        for (Vector vector : this.matrix) {
            highestOfEachVec.append(vector.highest());
        }
        return highestOfEachVec.highest();
    }

    public int lowest() {
        Vector lowestOfEachVec = new Vector();
        for (Vector vector : this.matrix) {
            lowestOfEachVec.append(vector.lowest());
        }
        return lowestOfEachVec.lowest();
    }

    public double avg() {
        int sum = 0;
        int count = 0;
        for (Vector vector : this.matrix) {
            sum += vector.sum();
            count += vector.countElements();
        }
        return (double) sum / count;
    }

    public Vector sumOfLines() {
        Vector sum = new Vector();
        for (Vector vector : this.matrix) {
            int sumOfVector = vector.sum();
            sum.append(sumOfVector);
        }
        return sum;
    }

    public Vector sumOfColumns() {
        Vector sumOfColumns = new Vector();
        for (Vector value : this.matrix) {
            Vector vector = new Vector(value.toArray());
            for (int j = 0; j < vector.length(); j++) {
                sumOfColumns.sumValueToIndex(vector.valueOfIndex(j), j);
            }
        }
        return sumOfColumns;
    }

    public int indexOfHighestSum() {
        Vector sumOfLines = this.sumOfLines();
        int highest = sumOfLines.highest();
        return sumOfLines.indexOfFirst(highest);
    }

    public boolean isSquare() {
        boolean isSquare = true;
        for (int i = 0; i < this.matrix.length && isSquare; i++) {
            isSquare = this.matrix[i].length() == this.matrix.length;
        }
        return isSquare;
    }

    private int getCommonLengthNumber() {
        int rowLength = this.matrix[0].length();
        //first iteration: save row length
        int rowLengthNew;
        for (int i = 1; i < this.matrix.length; i++) {
            rowLengthNew = this.matrix[i].length();
            if (rowLength != rowLengthNew) {
                rowLength = -1;
            }
        }
        return rowLength;
    }

    public boolean isRect() {
        boolean isRect = false;
        int rectLength = this.getCommonLengthNumber();
        if (rectLength != -1) {
            for (Vector vector : this.matrix) {
                isRect = vector.length() != this.matrix.length;
            }
        }
        return isRect;
    }

    public void transpose() {
        Matrix transposedTaller = new Matrix();
        for (Vector vector : this.matrix) {
            for (int j = 0; j < vector.length(); j++) {
                transposedTaller.appendAt(vector.valueOfIndex(j), j);
            }
        }
        this.matrix = transposedTaller.toVector();
    }

    @Override
    public boolean equals(Object obj) {
        if (this.matrix == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        boolean isEquals = true;
        Matrix other = (Matrix) obj;
        if (other.length() == this.matrix.length) {
            for (int i = 0; i < this.matrix.length && isEquals; i++) {
                isEquals = this.matrix[i].equals(other.matrix[i]);
            }
        } else {
            return false;
        }
        return isEquals;
    }

    public boolean isSymmetric() {
        Matrix transposed = new Matrix(this.toVector());
        transposed.transpose();
        return this.equals(transposed);
    }

    private int getValue(int i, int j) {
        if (i < 0 || j < 0 || i > this.matrix.length || j > this.matrix[i].length()) {
            throw new ArrayIndexOutOfBoundsException("Array Out of Bounds");
        }
        return this.matrix[i].valueOfIndex(j);
    }

    public Vector getDiagonal() {
        Vector diagonal = new Vector();
        for (int i = 0; i < this.matrix.length; i++) {
            diagonal.append(getValue(i, i));
        }
        return diagonal;
    }

    public Vector getAntidiagonal() {
        Vector antidiagonal = new Vector();
        for (int i = 0, j = this.matrix.length - 1; i < this.matrix.length; i++, j--) {
            antidiagonal.append(getValue(i, j));
        }
        return antidiagonal;
    }

    public int nonNullValuesInDiag() {
        if (this.isSquare() || this.isRect()) {
            int count = 0;
            Vector diagonal = this.getDiagonal();
            for (int i = 0; i < diagonal.length(); i++) {
                if (diagonal.valueOfIndex(i) != 0) {
                    count++;
                }
            }
            return count;
        } else {
            return -1;
        }
    }

    public boolean haveMatchingDiags() {
        Vector diagonal = getDiagonal();
        Vector antidiagonal = getAntidiagonal();
        return diagonal.equals(antidiagonal);
    }

    private int avgDigitCount() {
        int sumDigitCount = 0;
        int valueCount = 0;
        int digitAvg;
        for (Vector vector : this.matrix) {
            sumDigitCount = vector.countDigitsOfEveryElement();
            valueCount++;
        }
        digitAvg = sumDigitCount / valueCount;
        return digitAvg;
    }

    public Vector largerThanAvgDigitCount() {
        int digitAvg = this.avgDigitCount();
        Vector largerThanAvg = new Vector();
        for (Vector vector : this.matrix) {
            largerThanAvg.join(vector.overDigitCount(digitAvg));
        }
        return largerThanAvg;
    }

    private double evenDigitPercentage() {
        double sumEvenDigitCount = 0;
        int totalDigitCount = 0;
        double evenPercentage;
        for (Vector vector : this.matrix) {
            sumEvenDigitCount += vector.countEvenDigits();
            totalDigitCount += vector.countDigitsOfEveryElement();
        }
        evenPercentage = sumEvenDigitCount / totalDigitCount;
        return evenPercentage;
    }

    public Vector largerThanAvgEvenPercentage() {
        double avgPercentage = this.evenDigitPercentage();
        Vector largerThanAvg = new Vector();
        for (Vector vector : this.matrix) {
            largerThanAvg.join(vector.largerThanPercentage(avgPercentage));
        }
        return largerThanAvg;
    }

    public void invertLines() {
        Matrix invertedLines = new Matrix();
        for (Vector vector : this.matrix) {
            Vector arr = new Vector(vector.toArray());
            arr.invert();
            invertedLines.appendVector(arr);
        }
        this.matrix = invertedLines.toVector();
    }

    public void invertColumns() {
        Matrix invertedColumns = new Matrix();
        for (int i = this.matrix.length - 1; i >= 0; i--) {
            Vector arr = new Vector(this.matrix[i].toArray());
            invertedColumns.appendVector(arr);
        }
        this.matrix = invertedColumns.toVector();
    }

    public void rotateNinety() {
        if (isSquare() || isRect()) {
            this.transpose();
            this.invertLines();
        }
    }

    public void rotateHundredEighty() {
        if (isSquare() || isRect()) {
            this.invertColumns();
            this.invertLines();
        }
    }

    public void rotateNegNinety() {
        if (isSquare() || isRect()) {
            this.transpose();
            this.invertColumns();
        }
    }
}
