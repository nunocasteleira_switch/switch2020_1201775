public class Vector {
    private int[] vector;

    public Vector() {

    }

    public Vector(int[] initialVector) {
        if (initialVector != null) {
            this.vector = new int[initialVector.length];
            System.arraycopy(initialVector, 0, this.vector, 0, initialVector.length);
        }
    }

    private static double getEvenPercentage(int value) {
        int countTotal = Useful.countDigits(value);
        int countEven = Useful.countEvenDigits(value);
        return (double) countEven / countTotal;
    }

    public int[] toArray() {
        int[] returnArray = new int[this.vector.length];
        System.arraycopy(this.vector, 0, returnArray, 0, this.vector.length);
        return returnArray;
    }

    public void append(int value) {
        int[] newVector;
        if (isNull()) {
            newVector = new int[1];
            newVector[0] = value;
        } else {
            newVector = new int[this.vector.length + 1];
            System.arraycopy(this.vector, 0, newVector, 0, this.vector.length);
            newVector[newVector.length - 1] = value;
        }
        this.vector = newVector;
    }

    public void removeFirstInstance(int value) {
        int[] newVector = new int[this.vector.length - 1];
        boolean hasChanged = false;
        for (int i = 0, j = 0; i < newVector.length; i++) {
            if (this.vector[i] == value && !hasChanged) {
                i++;
                hasChanged = true;
            }
            newVector[j] = this.vector[i];
            j++;
        }
        this.vector = newVector;
    }

    public int valueOfIndex(int index) {
        int value;
        if (index > this.vector.length) {
            throw new ArrayIndexOutOfBoundsException("Array out of bounds.");
        }
        value = this.vector[index];
        return value;
    }

    public int indexOfFirst(int value) {
        int position = -1;
        for (int i = 0; i < this.vector.length && position == -1; i++) {
            if (this.vector[i] == value) {
                position = i;
            }
        }
        return position;
    }

    public int countElements() {
        int count = 0;
        for (int ignored :
                this.vector) {
            count++;
        }
        return count;
    }

    public int highest() {
        if (isEmpty()) {
            throw new ArrayIndexOutOfBoundsException("Array out of bounds.");
        }
        int highest = this.vector[0];
        for (int value :
                this.vector) {
            if (value > highest) {
                highest = value;
            }
        }
        return highest;
    }

    public int lowest() {
        checkIfValid();
        int lowest = this.vector[0];
        for (int value :
                this.vector) {
            if (value < lowest) {
                lowest = value;
            }
        }
        return lowest;
    }

    public double avg() {
        if (isEmpty() || isNull()) {
            return 0;
        } else {
            int sum = 0;
            for (int value :
                    this.vector) {
                sum += value;
            }
            return (double) sum / this.vector.length;
        }
    }

    public double avgEven() {
        checkIfValid();
        int count = 0;
        int sum = 0;
        for (int value :
                this.vector) {
            if (value % 2 == 0) {
                sum += value;
                count++;
            }
        }
        if (count > 0) {
            return (double) sum / count;
        } else {
            return 0;
        }
    }

    public double avgOdd() {
        checkIfValid();
        int count = 0;
        int sum = 0;
        for (int value :
                this.vector) {
            if (value % 2 != 0) {
                sum += value;
                count++;
            }
        }
        if (count > 0) {
            return (double) sum / count;
        } else {
            return 0;
        }
    }

    public double avgMultiplex(int multiplex) {
        checkIfValid();
        int count = 0;
        int sum = 0;
        for (int value :
                this.vector) {
            if (value % multiplex == 0) {
                sum += value;
                count++;
            }
        }
        if (count > 0) {
            return (double) sum / count;
        } else {
            return 0;
        }
    }

    public void ascending() {
        checkIfValid();
        for (int i = 0; i < this.vector.length; i++) {
            for (int j = i + 1; j < this.vector.length; j++) {
                if (this.vector[i] > this.vector[j]) {
                    int temp = this.vector[i];
                    this.vector[i] = this.vector[j];
                    this.vector[j] = temp;
                }
            }
        }
    }

    public void descending() {
        checkIfValid();
        for (int i = 0; i < this.vector.length; i++) {
            for (int j = i + 1; j < this.vector.length; j++) {
                if (this.vector[i] < this.vector[j]) {
                    int temp = this.vector[i];
                    this.vector[i] = this.vector[j];
                    this.vector[j] = temp;
                }
            }
        }
    }

    public boolean isNull() {
        return this.vector == null;
    }

    public void checkIfValid() {
        if (isNull()) {
            throw new NullPointerException("Vector is null.");
        }
        if (isEmpty()) {
            throw new IllegalStateException("Vector is empty.");
        }
    }

    public boolean isEmpty() {
        return this.vector.length == 0;
    }

    public boolean hasOnlyOneElement() {
        return this.vector.length == 1;
    }

    public boolean hasOnlyEvenElements() {
        checkIfValid();
        boolean onlyEven = true;
        for (int i = 0; i < this.vector.length && onlyEven; i++) {
            if (this.vector[i] % 2 != 0) {
                onlyEven = false;
            }
        }
        return onlyEven;
    }

    public boolean hasOnlyOddElements() {
        checkIfValid();
        boolean onlyOdd = true;
        for (int i = 0; i < this.vector.length && onlyOdd; i++) {
            if (this.vector[i] % 2 == 0) {
                onlyOdd = false;
            }
        }
        return onlyOdd;
    }

    public boolean hasDuplicates() {
        checkIfValid();
        boolean hasDuplicates = false;
        for (int i = 0; i < this.vector.length && !hasDuplicates; i++) {
            for (int j = i + 1; j < this.vector.length && !hasDuplicates; j++) {
                if (this.vector[i] == this.vector[j]) {
                    hasDuplicates = true;
                }
            }
        }
        return hasDuplicates;
    }

    private int countAvgDigits() {
        int digitsCount = 0;
        int digitsSum = 0;
        for (int value :
                this.vector) {
            int count = Useful.countDigits(value);
            digitsSum += count;
            digitsCount++;
        }
        return (int) Math.ceil((double) digitsSum / digitsCount);
    }

    public Vector digitsLargerThanAvg() {
        Vector digitsLargerThanAvg = new Vector();
        int avgDigits = countAvgDigits();

        for (int value :
                this.vector) {
            int count = Useful.countDigits(value);
            if (count > avgDigits) {
                digitsLargerThanAvg.append(value);
            }
        }
        return digitsLargerThanAvg;
    }

    public double getEvenPercentage() {
        double sum = 0;
        for (int value :
                this.vector) {
            sum += getEvenPercentage(value);
        }
        return sum / this.vector.length;
    }

    public Vector getFullyEvenElements() {
        Vector fullyEven = new Vector();
        for (int value :
                this.vector) {
            if (getEvenPercentage(value) == 1) {
                fullyEven.append(value);
            }
        }
        return fullyEven;
    }

    private static int[] digitsToArray(int value) {
        int size = Useful.countDigits(value);
        int[] arr = new int[size];
        for (int i = size - 1; i >= 0; i--) {
            arr[i] = value % 10;
            value /= 10;
        }
        return arr;
    }

    private static boolean isIncreasingSequence(int value) {
        boolean isIncrSequence = true;
        int[] arr = digitsToArray(value);
        for (int i = 0, j = i + 1; i < arr.length - 1 && isIncrSequence; i++, j++) {
            if (arr[i] >= arr[j]) {
                isIncrSequence = false;
            }
        }
        return isIncrSequence;
    }

    public Vector increasingSequences() {
        Vector increasingSequences = new Vector();
        for (int value :
                this.vector) {
            if (isIncreasingSequence(value)) {
                increasingSequences.append(value);
            }
        }
        return increasingSequences;
    }

    private static boolean isPalindrome(int value) {
        int[] arr = digitsToArray(value);
        boolean isPalindrome = false;
        for (int i = 0, j = arr.length - 1; i < j; i++, j--) {
            isPalindrome = arr[i] == arr[j];
        }
        return isPalindrome;
    }

    public Vector palindromes() {
        Vector palindromes = new Vector();
        for (int value :
                this.vector) {
            if (isPalindrome(value)) {
                palindromes.append(value);
            }
        }
        return palindromes;
    }

    private static boolean isSameDigit(int value) {
        int[] arr = digitsToArray(value);
        boolean isSame = true;
        for (int i = 0, j = i + 1; i < arr.length - 1 && isSame; i++, j++) {
            isSame = arr[i] == arr[j];
        }
        return isSame;
    }

    public Vector sameDigits() {
        Vector sameDigits = new Vector();
        for (int value :
                this.vector) {
            if (isSameDigit(value)) {
                sameDigits.append(value);
            }
        }
        return sameDigits;
    }

    public static boolean isNumberAnArmstrong(int value) {
        //153 = 1^3 + 5^3+ 3^3;
        int size = Useful.countDigits(value);
        int saveNumber = value;
        int sum = 0;
        int temp;
        while (value != 0) {
            temp = value % 10;
            sum += Math.pow(temp, size);
            value /= 10;
        }
        return sum == saveNumber;
    }

    public Vector armstrongNumbers() {
        Vector armstrongs = new Vector();
        for (int value :
                this.vector) {
            if (isNumberAnArmstrong(value)) {
                armstrongs.append(value);
            }
        }
        return armstrongs;
    }

    private static boolean isIncreasingSequenceOfSize(int value, int size) {
        boolean isIncrSequence = true;
        int[] arr = digitsToArray(value);
        int count = Useful.countDigits(value);
        //i < size for general condition
        //i <= size - 1 for it to only compare the numbers between the window
        //imagine 34756 and size = 3. We only want 3 to compare with 4 and 4 to compare with 7.
        //j <= count - 1 for ArrayOutOfBoundsException
        for (int i = 0, j = i + 1; i < size - 1 && j <= count - 1 && isIncrSequence; i++, j++) {
            if (arr[i] >= arr[j]) {
                isIncrSequence = false;
            }
        }
        return isIncrSequence;
    }

    public Vector increasingSequencesOfSize(int size) {
        Vector increasingSequencesOfSize = new Vector();
        for (int value :
                this.vector) {
            if (isIncreasingSequenceOfSize(value, size)) {
                increasingSequencesOfSize.append(value);
            }
        }
        return increasingSequencesOfSize;
    }

    public boolean isMatching(int[] other) {
        boolean isMatching = true;
        for (int i = 0; i < this.vector.length && isMatching; i++) {
            isMatching = this.vector[i] == other[i];
        }
        return isMatching;
    }

    public void invert() {
        int[] inverted = new int[this.vector.length];
        for (int i = 0, j = inverted.length - 1; j >= 0; i++, j--) {
            inverted[i] = this.vector[j];
        }
        this.vector = inverted;
    }

    public int length() {
        int count = 0;
        for (int ignored :
                this.vector) {
            count++;
        }
        return count;
    }

    public Vector copy() {
        int[] newArr = new int[this.vector.length];
        for (int i = 0; i < this.vector.length; i++) {
            newArr[i] = this.valueOfIndex(i);
        }
        Vector newVec = new Vector(newArr);
        return newVec;
    }

    public int sum() {
        int sum = 0;
        for (int value : this.vector) {
            sum += value;
        }
        return sum;
    }

    public void sumValueToIndex(int value, int index) {
        if (isNull()) {
            int[] newArr = new int[1];
            newArr[0] = value;
            this.vector = newArr;
        } else if (index < this.vector.length) {
            this.vector[index] += value;
        } else {
            int remainingIndexes = index - (this.vector.length - 1);
            int newSize = this.vector.length + remainingIndexes;
            int[] newArr = new int[newSize];
            System.arraycopy(this.vector, 0, newArr, 0, this.vector.length);
            newArr[index] = value;
            this.vector = newArr;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this.vector == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        boolean isEquals = true;
        Vector other = (Vector) obj;
        if (other.length() == this.vector.length) {
            for (int i = 0; i < this.vector.length && isEquals; i++) {
                isEquals = this.vector[i] == other.vector[i];
            }
        } else {
            return false;
        }
        return isEquals;
    }

    public int countDigitsOfEveryElement() {
        int count = 0;
        for (int value :
                this.vector) {
            count += Useful.countDigits(value);
        }
        return count;
    }

    public Vector overDigitCount(int count) {
        Vector overDigitCount = new Vector();
        for (int value :
                this.vector) {
            if (Useful.countDigits(value) > count) {
                overDigitCount.append(value);
            }
        }
        return overDigitCount;
    }

    public void join(Vector other) {
        Vector larger = new Vector();
        if (!isNull()) {
            for (int value : this.vector) {
                larger.append(value);
            }
        }
        if (!other.isNull()) {
            for (int value :
                    other.vector) {
                larger.append(value);
            }
            this.vector = larger.toArray();
        }
    }

    public Vector largerThanPercentage(double percentage) {
        Vector largerThan = new Vector();
        for (int value : this.vector) {
            if (getEvenPercentage(value) > percentage) {
                largerThan.append(value);
            }
        }
        return largerThan;
    }

    public int countEvenDigits() {
        int countEven = 0;
        for (int value : this.vector) {
            countEven += Useful.countEvenDigits(value);
        }
        return countEven;
    }


}
