package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class exercicioSeteTest {

    @Test
    public void testeUmObterDistanciaMaratona() {
        double expected = 1158.14;
        double result = exercicioSete.obterDistanciaMaratona(10930000, 42195, 300000);
        assertEquals(expected, result, 0.01);
    }
}