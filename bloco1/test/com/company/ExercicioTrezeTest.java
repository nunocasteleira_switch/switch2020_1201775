package com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioTrezeTest {

    @Test
    public void testGetMinutes() {
        int hours = 2;
        int minutes = 34;
        int expected = 154;
        int result = ExercicioTreze.getMinutes(hours, minutes);
        assertEquals(expected,result);
    }
}