package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class exercicioCincoTest {

    @Test
    public void testeUmObterDistanciaPredio() {
        double expected = 19.6;
        double result = exercicioCinco.obterDistanciaPredio(2);
        assertEquals(expected, result, 0.1);
    }

    @Test
    public void testeDoisObterDistanciaPredio() {
        double expected = 78.4;
        double result = exercicioCinco.obterDistanciaPredio(4);
        assertEquals(expected, result, 0.1);
    }
}