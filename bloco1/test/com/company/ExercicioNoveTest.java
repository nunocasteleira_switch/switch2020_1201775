package com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioNoveTest {

    @Test
    public void testOneGetRectPerimeter() {
        //Arrange
        double base = 4;
        double height = 2;
        double expected = 12;

        //Act
        double result = ExercicioNove.getRectPerimeter(base, height);

        //Assert
        assertEquals(expected, result, 0.01);
    }
}