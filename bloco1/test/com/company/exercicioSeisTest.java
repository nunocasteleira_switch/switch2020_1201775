package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class exercicioSeisTest {

    @Test
    public void testeUmObterAlturaTales() {
        double expected = 20;
        double result = exercicioSeis.obterAlturaTales(40, 4, 2);
        assertEquals(expected, result, 0.1);
    }
}