package com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class exercicioOitoTest {

    @Test
    public void testeUmObterDistanciaFuncionarios() {
        double expected = 98.85;
        double result = exercicioOito.obterDistanciaFuncionarios(60, 40, 60);
        assertEquals(expected, result, 0.01);
    }
}