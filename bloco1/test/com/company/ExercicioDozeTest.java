package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExercicioDozeTest {

    @Test
    public void testGetFahrenheit() {
        double celsius = 20;
        double expected = 68;
        double result = ExercicioDoze.getFahrenheit(celsius);
        assertEquals(expected, result, 0.0001);
    }
}