package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class exercicioQuatroTest {

    @Test
    public void testeUmObterDistanciaTrovoada() {
        double expected = 3.4;
        double result = exercicioQuatro.obterDistanciaTrovoada(10);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeDoisObterDistanciaTrovoada() {
        double expected = 5;
        double result = exercicioQuatro.obterDistanciaTrovoada(14);
        assertEquals(expected, result, 1);
    }
}