package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExercicioDezTest {

    @Test
    public void getHypotenuse() {
        double cathet1 = 3;
        double cathet2 = 4;
        double expected = 5;

        double result = ExercicioDez.getHypotenuse(cathet1, cathet2);

        assertEquals(expected, result, 0.01);
    }
}