package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class exercicioTresTest {

    @Test
    public void testeUmObterVolumeCilindro() {
        double expected = 50.26;
        double result = exercicioTres.obterVolumeCilindro(2, 4);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeDoisObterVolumeCilindro() {
        double expected = 565486.68;
        double result = exercicioTres.obterVolumeCilindro(60, 50);
        assertEquals(expected, result, 0.01);
    }
}