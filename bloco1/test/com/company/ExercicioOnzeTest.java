package com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioOnzeTest {

    @Test
    public void testExercicio11() {
        double expected = 0;
        double result = ExercicioOnze.getDiscriminant(1, 2, 1);
        assertEquals(expected, result, 0.00001);
    }

    @Test
    public void testSolveWithQuadraticFormulaPositive() {
        double a = 1;
        double b = 2;
        double c = -3;
        double expected = 1;
        double result = ExercicioOnze.solveWithQuadraticFormulaPositive(a, b, c);
        assertEquals(expected, result, 0.0001);
    }

    @Test
    public void testSolveWithQuadraticFormulaNegative() {
        double a = 1;
        double b = 2;
        double c = -3;
        double expected = -3;
        double result = ExercicioOnze.solveWithQuadraticFormulaNegative(a, b, c);
        assertEquals(expected, result, 0.0001);
    }
}