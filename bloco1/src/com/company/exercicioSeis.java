package com.company;

import java.util.Scanner;

public class exercicioSeis {

    public static void main(String[] args) {
        // write your code here
        exercicio6();
    }

    public static void exercicio6() {
        //Este programa pretende calcular a altura de um prédio
        //usando o teorema de Tales

        //Dados
        double sombraPredio, sombraPessoa, alturaPredio, alturaPessoa;

        //Leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o comprimento da sombra do prédio.");
        sombraPredio = ler.nextInt();
        System.out.println("Introduza o comprimento da tua sombra.");
        sombraPessoa = ler.nextInt();
        System.out.println("Introduza a tua altura.");
        alturaPessoa = ler.nextInt();

        //Processamento
        alturaPredio = obterAlturaTales(sombraPredio, sombraPessoa, alturaPessoa);

        //Saída de dados
        System.out.println("O prédio tem " + String.format("%.2f", alturaPredio) + " m de altura.");
    }

    public static double obterAlturaTales(double sombraPredio, double sombraPessoa, double alturaPessoa) {
        //O cálculo é obtido pela seguinte fórmula
        //H/h = S/s.
        //H - Altura Prédio
        //h - Altura pessoa
        //S - Sombra Prédio
        //s - Sombra pessoa

        return (alturaPessoa * sombraPredio) / sombraPessoa;
    }
}