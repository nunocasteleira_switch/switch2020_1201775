package com.company;

import java.util.Scanner;

public class ExercicioNove {
    public static void main(String[] args) {
        // write your code here
        exercicio9();
    }

    public static void exercicio9() {
        //Pretende-se calcular o perímetro de um retângulo
        //perimetro = 2*A + 2*B

        double base;
        double height;
        double perimeter;

        //Leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o comprimento da base.");
        base = ler.nextInt();
        System.out.println("Introduza o comprimento da altura.");
        height = ler.nextInt();

        perimeter = getRectPerimeter(base, height);

        System.out.println("O retângulo tem " + String.format("%.2f", perimeter) + " de perímetro.");
    }

    public static double getRectPerimeter(double base, double height) {
        return 2 * base + 2 * height;
    }
}
