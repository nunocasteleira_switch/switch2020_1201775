package com.company;

import java.util.Scanner;

public class ExercicioDez {
    public static void main(String[] args) {
        // write your code here
        exercicio10();
    }

    public static void exercicio10() {
        //Pretende-se calcular a hipotenusa do triângulo retângulo.
        //h^2 = c1^2 + c2^2

        double cathet1;
        double cathet2;
        double hypotenuse;

        //Leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o comprimento do primeiro cateto.");
        cathet1 = ler.nextDouble();
        System.out.println("Introduza o comprimento do segundo cateto.");
        cathet2 = ler.nextDouble();

        hypotenuse = getHypotenuse(cathet1, cathet2);

        System.out.println("O triângulo tem " + String.format("%.2f", hypotenuse) + " de hipotenusa.");
    }

    public static double getHypotenuse(double cathet1, double cathet2) {
        return Math.hypot(cathet1, cathet2);
    }
}
