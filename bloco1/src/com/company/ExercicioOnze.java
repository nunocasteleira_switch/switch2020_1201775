package com.company;

import java.util.Scanner;

public class ExercicioOnze {
    public static void main(String[] args) {
        exercicio11();
    }

    public static void exercicio11() {
        /*
         *     x^2 - 3x + 1 = 0 <=> /////// x = (-b ± sqrt(b^2 - 4*a*c)) / 2a
         * <=> x = (-3 ± sqrt(3^2 - 4*1*1)) / 2*-1 <=>
         * <=> x = (-3 ± sqrt(9 - 4)) / -6 <=>
         * <=> x = (3+sqrt(5))/2 V (3-sqrt(5))/2
         *
         */
        double a, b, c;
        double discriminant;

        //Leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o valor do coeficiente quadrado.");
        a = ler.nextDouble();
        System.out.println("Introduza o valor do coeficiente linear.");
        b = ler.nextDouble();
        System.out.println("Introduza o valor do termo constante.");
        c = ler.nextDouble();

        //Binómio Discriminante
        discriminant = getDiscriminant(a, b, c);
        if (discriminant >= 0) {
            System.out.println("A equação quadrática tem a solução real:");
            System.out.println(solveWithQuadraticFormulaPositive(a, b, c));
        }
        if (discriminant > 0) {
            System.out.println("e também a solução real:");
            System.out.println(solveWithQuadraticFormulaNegative(a, b, c));
        }
    }

    public static double getDiscriminant(double a, double b, double c) {
        return (Math.pow(b, 2) - 4 * a * c);
    }
    public static double solveWithQuadraticFormulaPositive(double a, double b, double c) {
        double positiveSolution;
        positiveSolution = (-b + Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / 2 * a;
        return positiveSolution;
    }
    public static double solveWithQuadraticFormulaNegative(double a, double b, double c) {
        double negativeSolution;
        negativeSolution = (-b - Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / 2 * a;
        return negativeSolution;
    }
}
