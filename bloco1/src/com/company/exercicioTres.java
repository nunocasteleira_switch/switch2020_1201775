package com.company;

import java.util.Scanner;

public class exercicioTres {

    public static void main(String[] args) {
        // write your code here
        exercicio3();
    }

    public static void exercicio3() {
        //Dados
        double raio, altura, volume;
        //Conhece-se o raio da base e a altura

        //Leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o valor do raio da base em metros");
        raio = ler.nextDouble();
        System.out.println("Introduza o valor da altura em metros");
        altura = ler.nextDouble();

        //Processamento recorrendo ao método volumeCilindro
        volume = obterVolumeCilindro(raio, altura) * 1000;
        //Transformar em litros = multiplicar por 1000.

        //Saída de dados
        System.out.println("O volume em litros deste sólido é de " + String.format("%.2f", volume) + " litros.");
        //String.format() vai formatar a apresentação do resultado.
        //Neste caso, apresenta-nos duas casas decimais.
    }

    public static double obterVolumeCilindro(double raio, double altura) {
        double volume;
        //Volume cilindro = área da base (pi*r^2) * altura
        volume = Math.PI * raio * raio * altura;

        //Poder-se-ia usar uma constante que retornasse um valor mais preciso de pi
        //Poder-se-ia usar uma função que elevasse raio a dois
        //java.lang.Math, constante PI e método pow()
        return volume;
    }
}