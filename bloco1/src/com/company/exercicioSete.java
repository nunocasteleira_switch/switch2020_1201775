package com.company;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class exercicioSete {

    public static void main(String[] args) {
        // write your code here
        exercicio7();
    }

    public static void exercicio7() {
        //Este programa pretende calcular a distância percorrida pelo Zé
        //tendo em conta a distância percorrida pelo Manel e atendendo à sua velocidade média

        //De acordo com https://stackoverflow.com/questions/3206473/number-of-seconds-in-hhmmss

        //inicia-se um formato de data
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

        //Inicia-se uma string para receber a leitura de dados
        String stringRef, stringDesignated;
        double distanciaRef;

        //Leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o tempo que o corredor de referência demorou na maratona em formato HH:mm:ss.");
        stringRef = ler.next();
        System.out.println("Introduza a distância que o corredor de referência percorreu em metros.");
        distanciaRef = ler.nextDouble();
        System.out.println("Introduza o tempo que o corredor designado demorou na maratona em formato HH:mm:ss.");
        stringDesignated = ler.next();

        //inicia-se uma variável de tipo Date que seja formatado no formato HH:mm:ss
        Date dateRef = null;
        try {
            dateRef = dateFormat.parse(stringRef);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //converte-se para segundos
        assert dateRef != null;
        long segundosRef = dateRef.getTime();
        //System.out.println(segundosRef);

        //Por forma a calcularmos a distância percorrida pelo Zé vamos calcular recorrendo à velocidade média já calculada.
        //E com acesso ao tempo que ele se manteve em prova, no mesmo formato que o anterior vamos proceder aos cálculos.
        Date dateDesignated = null;
        try {
            dateDesignated = dateFormat.parse(stringDesignated);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assert dateDesignated != null;
        long segundosDesignated = dateDesignated.getTime();
        //System.out.println(segundosDesignated);

        double resultado = obterDistanciaMaratona(segundosRef, distanciaRef, segundosDesignated);
        System.out.println("O corredor designado percorreu " + String.format("%.2f", resultado) + "m na maratona.");
    }

    public static double obterDistanciaMaratona(long segundosRef, double distanciaRef, long segundosDesignated) {
        //De acordo com a fórmula VelocidadeMédia = deltaDistância / deltaTempo calcula-se a velocidade média do Manel.
        double velocidadeMediaRef = distanciaRef / segundosRef;
        System.out.println("A velocidade média dos corredores foi de " + velocidadeMediaRef + "m/s.");

        return velocidadeMediaRef * segundosDesignated;
    }
}