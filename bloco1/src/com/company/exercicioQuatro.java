package com.company;

import java.util.Scanner;

public class exercicioQuatro {

    public static void main(String[] args) {
        // write your code here
        exercicio4();
    }

    public static void exercicio4() {
        //Obter a distância da trovoada introduzindo a distância em segundos
        //Dados
        double segundos;
        double distancia;

        //Leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza quantos segundos contou desde o relâmpago até ao trovão.");
        segundos = ler.nextDouble();

        //Processamento
        distancia = obterDistanciaTrovoada(segundos);

        //Saída de dados
        System.out.println("A trovoada encontra-se a " + String.format("%.1f", distancia) + " km de distância.");
    }

    public static double obterDistanciaTrovoada(double segundos) {
        //O cálculo da distância é dado pela multiplicação da
        // a velocidade do som pelo intervalo de tempo, em segundos,
        // entre o relâmpago e o trovão.
        return segundos * 0.34;
    }
}