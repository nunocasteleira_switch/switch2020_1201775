package com.company;

import java.util.Scanner;

public class exercicioCinco {

    public static void main(String[] args) {
        // write your code here
        exercicio5();
    }

    public static void exercicio5() {
        //Este programa pretende calcular a altura de um prédio recorrendo
        //ao tempo que uma pedra demora a cair do seu ponto mais alto.

        //Dados
        double altura, tempo;

        //Leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza quantos segundos demorou a pedra a cair.");
        tempo = ler.nextDouble();

        //Processamento
        altura = obterDistanciaPredio(tempo);

        //Saída de dados
        System.out.println("O prédio tem " + String.format("%.2f", altura) + " m de altura.");
    }

    public static double obterDistanciaPredio(double segundos) {
        //O cálculo é obtido pela seguinte fórmula
        //D=Vo*t +(a*t2)/2, em que Vo = 0 (aceleração inicial da pedra).
        //D - é a distância que a pedra vai percorrer (a altura do prédio).
        //V0 - é a velocidade inicial da pedra, que, como é largada, vai ser igual a zero.
        //a - é a aceleração da pedra, que neste caso é a gravidade da terra e vale 9,8 m/s2.
        //t - é o tempo cronometrado no relógio (em segundos).

        return (9.8 * (segundos * segundos)) / 2;
    }

}