package com.company;

import java.util.Scanner;

public class ExercicioDoze {
    public static void main(String[] args) {
        exercicio12();
    }

    public static void exercicio12() {
        //Converter Fahrenheit em Celsius
        //Fahrenheit = 32 + (9 / 5) * Celsius
        double celsius;
        double fahrenheit;

        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza a temperatura em Celsius.");
        celsius = ler.nextDouble();

        fahrenheit = getFahrenheit(celsius);
        System.out.println("A temperatura em Fahrenheit é de " + fahrenheit + "º.");
    }

    public static double getFahrenheit(double celsius) {
        return 32 + (9.0 / 5.0) * celsius;
    }
}
