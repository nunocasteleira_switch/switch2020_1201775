package com.company;

import java.util.Scanner;

public class ExercicioTreze {
    public static void main(String[] args) {
        exercicio13();
    }

    public static void exercicio13() {
        //Converter Horas e minutos em total de minutos
        //minutos = horas * 60 + minutos;
        int horas;
        int minutosInput;
        int minutosTotais;

        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza quantas horas.");
        horas = ler.nextInt();
        System.out.println("Introduza quantos minutos.");
        minutosInput = ler.nextInt();

        minutosTotais = getMinutes(horas, minutosInput);
        System.out.println("Decorreram " + minutosTotais + "desde as 00h");
    }

    public static int getMinutes(int hours, int minutes) {
        return hours * 60 + minutes;
    }
}
