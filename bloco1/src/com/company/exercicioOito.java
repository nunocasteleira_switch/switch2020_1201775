package com.company;

import java.util.Scanner;

public class exercicioOito {

    public static void main(String[] args) {
        // write your code here
        exercicio8();
    }

    public static void exercicio8() {
        //Pretende-se calcular a distância entre dois funcionários através da fórmula dada por
        //c^2 = a^2 + b^2 - 2ab cos(ângulo), onde
        //a = 60, b = 40, ângulo = 60º

        int a;
        int b;
        double c;
        double angle;
        int angleIntermediate;

        //Leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o primeiro lado.");
        a = ler.nextInt();
        System.out.println("Introduza o segundo lado.");
        b = ler.nextInt();
        System.out.println("Introduza o ângulo.");
        angleIntermediate = ler.nextInt();
        angle = Math.toRadians(angleIntermediate);

        //A conversão dos catetos e dos ângulos para double é feita para permitir a operação.
        c =  obterDistanciaFuncionarios( a, b, angle);
        System.out.println("Os dois operários encontram-se a " + String.format("%.2f", c) + "m de distância.");
    }

    public static double obterDistanciaFuncionarios(double a, double b, double angle){
        double cSquared;
        cSquared = a * a + b * b - 2 * a * b * Math.cos(angle);
        return Math.sqrt(cSquared);
    }
}

